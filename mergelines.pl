#!/usr/bin/perl -w
# Transform 4 line fastq to 1 line tabular representation.
# File name: mergelines.pl

use strict;

my $count = 0;
while(my $line = <STDIN>){
    chomp($line);
    print $line;
    $count = ($count + 1) % 4;
    
    if($count == 0){
        print "\n"; 
    }else{
        print "\t";
    }
}