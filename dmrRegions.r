#
### R: dmrRegions.r
#


# Two functions:
# 1. getYfgPromoterCounts
# 2. findDEGenes
#
# 




library("gtools")  # for 'combinations'
library(biomaRt)
library(edgeR)


## Global Variables
# Load TTS from Biomart, and
# load the genome wide annotation package for the appropriate organism:
ensembl <- useMart("ensembl")

if ( ORGANISM == ORG$hs ) {
	#martDataset <- "hsapiens_gene_ensembl"
	ensembl <- useDataset("hsapiens_gene_ensembl",mart=ensembl)
} else if ( ORGANISM == ORG$mm ) {
	#martDataset <- ""
} else if ( ORGANISM == ORG$rn ) {	
	#martDataset <- ""
} else {
	cat("Genome wide annotation package does not implemented for organism", ORGANISM, "\n\n")
}
	
	
# set a high scientific notation penalty to avoid scientific notation in the files we write to
options(scipen=100)
options(digits=2)



#
# DESCRIPTION
# Given a list of unique gene names,
#
getYfgPromoterCounts <- function( yfg.file.name, extend.up=1000, extend.down=500 ) {	
# yfg.file.name <- "paperGenes.fig1c.yfg"
	genes <- read.table(yfg.file.name,sep="\t",stringsAsFactors=FALSE,header=TRUE)[ ,1]  # read in vector of genes	
	genes <- genes[ !is.na(genes) ]

	# get gene info from BioMart
	cat("Getting Ensembl gene info from BioMart\n")
	ens.gene.desc <- getBM(
		filters= "hgnc_symbol", 
		attributes= c("chromosome_name","start_position","end_position","strand","ensembl_gene_id", "hgnc_symbol", "description"),
		values= genes,
		mart= ensembl)

	print(head(ens.gene.desc))
	print(dim(ens.gene.desc))

	# if there are no genes then quit
	if (nrow(ens.gene.desc)==0) return(NULL)
	
	# BedTools needs chr names to start with 'chr'
	ens.gene.desc$chromosome_name <- paste0("chr", ens.gene.desc$chromosome_name)
	ens.gene.desc$strand <- ifelse( ens.gene.desc$strand == 1, "+", "-" )
	
	
	# reorder columns to fit with the BED spec
	ens.gene.bed <- ens.gene.desc[ ,c("chromosome_name","start_position","end_position","hgnc_symbol","ensembl_gene_id","strand")]
	
	# Take the 'extend' nts upstream promoter region of each gene.
	# NB: this does not take care of 3' chr ends... perhaps I should use BedTools... TBD
	extend.fct <- function(x) {
		y <- x
		if ( x[6] == "+" ) {
			tmp <- as.numeric(x[2]) - extend.up  # new start_position
			y[2] <- ifelse ( tmp < 0, 0, tmp )
			y[3] <- as.numeric(x[2]) + extend.down  # new end_position
		} else {
			y[3] <- as.numeric(x[3]) + extend.up  # new end_position (which is really the start position on the '-' strand)
			tmp <- as.numeric(x[3]) - extend.down
			y[2] <- ifelse ( tmp < 0, 0, tmp )
		}
		return(y)
	}
	cat("Calculating promoter regions.\n")
	ens.gene.bed <- as.data.frame(t(apply(ens.gene.bed, 1, extend.fct)),stringsAsFactors=FALSE)
	cat("getYfgPromoterCounts:00")
	#ens.gene.bed[ ,c(2:3)] <- lapply(ens.gene.bed[ ,c(2:3,6)], as.numeric)
	ens.gene.bed[ ,c(2:3)] <- lapply(ens.gene.bed[ ,c(2:3)], as.numeric)
		
	#	if ( ens.gene.bed$strand == 1 ) {
	#		ens.gene.bed$start_position <- ens.gene.desc$start_position - extend.l
	#		ens.gene.bed$end_position <- ens.gene.desc$start_position
	#	} else {
	#		ens.gene.bed$end_position <- ens.gene.desc$end_position + extend.l
	#		ens.gene.bed$start_position <- ens.gene.desc$end_position
	#	}
	
	# write bed file of gene promoters
	bed.file.name <- paste0(yfg.file.name,".UP",extend.up,".DOWN",extend.down,".promoter.bed" )
	cat("Writting file: ", bed.file.name, "\n")
	write.table( ens.gene.bed, file = file.path(OUT.DIR,bed.file.name), sep = "\t", quote = FALSE, col.names = F, row.names = F )
	
	#ens.gene.p1 <- ens.gene.bed
	#ens.gene.p1$p.val <- 1
	#ens.gene.p1 <- ens.gene.p1[ ,c(1:3,7,4:6)]
	#write.table( ens.gene.p1, file = "yfgPromoters.fig1c.p1", sep = "\t", quote = FALSE, col.names = F, row.names = F )


	# count hw many rds map to the promoters
	cat("Constructing promoter expression matrix.\n")
	exp.mx.names <- c()
	exp.mx.cond <- c()
	exp.mx.repl <- c()
	exp.mx <- c()  # will hold the expression matrix: gene promoters x samples
	exp.mx.lib.sizes <- c()
	for ( cond in unique( exp.spec$condition ) ) {
		for (repl in unique( exp.spec[exp.spec$condition == cond, ]$replicate )) {  # unique: if its PE then each replicate appears twice
			base.name <- paste0(cond,"-",repl)
			#f.aln <- paste0(base.name,"_aligned.bed")
			f.aln <- paste0(base.name,"_aligned_ext.bed")
			f.aln <- file.path("STATS",f.aln)
			
			exp.mx.cond <- c(exp.mx.cond, cond)
			exp.mx.repl <- c(exp.mx.repl, repl)
			exp.mx.names <- c(exp.mx.names, base.name)
			exp.mx.lib.sizes <- c(exp.mx.lib.sizes, as.numeric(system(paste0("wc -l < ",f.aln), intern=TRUE)))
			
			#idx <- which(exp.spec$condition == cond & exp.spec$replicate == repl)
			#if (is.pe) idx <- idx[1]
	
			# count hw many rds from the current sample overlap each gene promoter
			cat("\tProcessing sample file", f.aln, "\n")
			exp.mx <- cbind(
				exp.mx,
				as.numeric(system(paste0("intersectBed -c -a ", file.path(OUT.DIR,bed.file.name), " -b ", f.aln, " | cut -f7"), intern=TRUE)) )
		}
	}
	
	colnames( exp.mx ) <- exp.mx.names
	rownames( exp.mx ) <- ens.gene.bed$hgnc_symbol
	exp.mx.cond <- factor( exp.mx.cond )
	
	exp.mx.cpm <- sweep(exp.mx, 2, exp.mx.lib.sizes, "/") * 1e6  # normalize to CPM
	
	#pdf("boxplot.pdf")
		#boxplot(exp.mx ~ exp.mx.cond)
		#dotchart(x$mpg,...
	#dev.off()
	
	
	## Raw:	
	# calculate mean and sd
	cat("Calculating raw means and sd's.\n")
	cond.means <- t(apply(exp.mx,1,function(x) tapply(x,exp.mx.cond,mean)))
	cond.sds <- t(apply(exp.mx,1,function(x) tapply(x,exp.mx.cond,sd)))
	exp.means.sds <- as.data.frame(cbind(exp.mx, cond.means, cond.sds))
	#colnames( exp.means.sds ) <- c(exp.mx.names, paste0("mean",":",levels(exp.mx.factor)), paste0("sd",":",levels(exp.mx.factor)))
	print(head(exp.means.sds))
	
	# write resulting table to file
	exp.mens.sds.file.name <- paste0(yfg.file.name,".UP",extend.up,".DOWN",extend.down,".promoter.exp" )
	cat("Writting file: ", exp.mens.sds.file.name, "\n")
	write.table( exp.means.sds, file=file.path(OUT.DIR,exp.mens.sds.file.name), sep="\t", quote=FALSE, col.names=TRUE, row.names=TRUE )
	
	## Same for CPM mx:
	# calculate mean and sd
	cat("Calculating CPM means and sd's.\n")
	cond.cpm.means <- t(apply(exp.mx.cpm,1,function(x) tapply(x,exp.mx.cond,mean)))
	cond.cpm.sds <- t(apply(exp.mx.cpm,1,function(x) tapply(x,exp.mx.cond,sd)))
	exp.cpm.means.sds <- as.data.frame(cbind(exp.mx.cpm, cond.cpm.means, cond.cpm.sds))
	#colnames( exp.means.sds ) <- c(exp.mx.names, paste0("mean",":",levels(exp.mx.factor)), paste0("sd",":",levels(exp.mx.factor)))
	print(head(exp.cpm.means.sds))
	
	# write resulting table to file
	exp.cpm.mens.sds.file.name <- paste0(yfg.file.name,".UP",extend.up,".DOWN",extend.down,".promoter.CPM.exp" )
	cat("Writting file: ", exp.cpm.mens.sds.file.name, "\n")
	write.table( exp.cpm.means.sds, file=file.path(OUT.DIR,exp.cpm.mens.sds.file.name), sep="\t", quote=FALSE, col.names=TRUE, row.names=TRUE )



	###
	### two-sample t-test
	p.signif <- .05
	
	# t-test for raw counts
	p.raw.count <- apply(exp.mx,1, function(x) t.test(x ~ exp.mx.cond)$p.value )
	p.raw.count <- p.raw.count[!is.na(p.raw.count)]
	p.raw.de <- p.raw.count[ p.raw.count < p.signif ]
	print(p.raw.de)
	# TBD TBD: mult. testing correction
	
	# t-test for CPM counts
	p.cpm.count <- apply(exp.mx.cpm,1, function(x) t.test(x ~ exp.mx.cond)$p.value )
	p.cpm.count <- p.cpm.count[!is.na(p.cpm.count)]
	p.cpm.de <- p.cpm.count[ p.cpm.count < p.signif ]
	print(p.cpm.de)
	# TBD TBD: mult. testing correction
	
	# dotplots of the raw and CPM t-test DMR expressions
	cat("Plotting expression dotcharts of the raw and CPM DMRs.\n")
	pdf(file.path(OUT.DIR,"DMRs.t.raw.CPM.pdf"))
		for ( n in names(p.raw.de) ) {
			cat( n, "\n" )
			dotchart(
				exp.mx[n, ],
				labels(colnames(exp.mx)),
				cex=.7,
				groups=exp.mx.cond,
				#color=seq_along(levels(exp.mx.cond)),
				gcolor=2,
				pch=19,
				main="Raw Expression Level for each sample\ngrouped by treatment",
				sub=n,
				xlab="Raw Expression Level"
				)
		}
		for ( n in names(p.cpm.de) ) {
			cat( n, "\n" )
			dotchart(
				exp.mx.cpm[n, ],
				labels(colnames(exp.mx.cpm)),
				cex=.7,
				groups=exp.mx.cond,
				#color=seq_along(levels(exp.mx.cond)),
				gcolor=2,
				pch=19,
				main="CPM Expression Level for each sample\ngrouped by treatment",
				sub=n,
				xlab="CPM Expression Level"
				)
		}
	dev.off()
		
		

	###
	### look for DMRs using edgeR
	if ( DMR.REGIONS.EDGER ) {
		pdf(file.path(OUT.DIR,"EDGER.PLOTS.pdf"))
			cat("***** Raw counts:\n")					
			findDEGenes(counts=exp.mx, group=exp.mx.cond, repl=factor(exp.mx.repl), filter.minExpr=DE.EDGER.MIN.EXPR, pFDR=DE.EDGER.FDR, 
				tagwiseDisp=DE.EDGER.TAGWISE.DISPERSION, rem.samples=c(11), outFile=file.path(OUT.DIR, paste0(yfg.file.name,".DE")), caption=yfg.file.name, ID=yfg.file.name )
			cat("***** CPM counts:\n")
			findDEGenes(counts=exp.mx.cpm, group=exp.mx.cond, repl=factor(exp.mx.repl), filter.minExpr=DE.EDGER.MIN.EXPR, pFDR=DE.EDGER.FDR, 
				tagwiseDisp=DE.EDGER.TAGWISE.DISPERSION, rem.samples=c(11), outFile=file.path(OUT.DIR, paste0(yfg.file.name,".DE")), caption=yfg.file.name, ID=yfg.file.name )
		dev.off()
	}
	
	return(NULL)
}





#
# DESCRIPTION
# Given a bed file (annot.bed), build an expression mx by counting # rds in each range.
# minCount = 1 (default) ; discard rows where every sample has < minCount tags
# win.size: split regions into windows of at most win.size nts. If = 0 then take the entire region.
#
# if both extend r 0 then just used the supplied regions
# if at least one of the extends r != 0 then take the "promoter" regions upstream of each entry
customCounts <- function( annot.bed, minCount=1, extend.up=0, extend.down=0, win.size=0 ) {
	cat( annot.bed, "\n" )
		
	if ( DMR.REGIONS.DIFFBIND ) {
		# if this is TRUE then create DiffBind consensus peaks BED file and use that with edgeR
		
		
	}	
		
	if ( (extend.up != 0) || (extend.down != 0) ) {
		if ( !file.exists( paste0(annot.bed,"TSS.bed") ) ) {
			# create TSS bed file: for each range, get the first nt of the range
			cat("Creating TSS file..\n")
			system(paste0("awk 'BEGIN{OFS=\"\\t\"}{if ($NF==\"+\") {print $1,$2,$2+1,$4,$5,$6} else {print $1,$3-1,$3,$4,$5,$6}}' ", annot.bed, " > ", annot.bed, "TSS.bed"))
		}

		# create bed file with the regions of interest
		promoters.bed <- paste0(annot.bed,".UP",extend.up,".DOWN",extend.down,".promoter.bed" )
		promoters.bed <- file.path(OUT.DIR,promoters.bed)
		cat(promoters.bed, "\n")
		# transform each range into a promoter region by extending it up and down by the specified amount
		system(paste("slopBed -l", extend.up, "-r", extend.down, "-s -i", paste0(annot.bed,"TSS.bed"), "-g chrLengths | sort -k1,1 -k2,2n >", promoters.bed, sep=" "))
	} else {
		# if both extend's = 0, annotate using the regions specified in the annot.bed file
		#system( paste("cp", annot.bed, file.path(OUT.DIR,promoters.bed), sep= " ") )
		promoters.bed <- annot.bed
	}
	
	# split regions into windows of size win.size; last window can be up to 25% larger
	if ( win.size > 0 ) {
		promoters.win.bed <- paste0(annot.bed,".UP",extend.up,".DOWN",extend.down,".WIN",win.size,".promoter.bed" )
		promoters.win.bed <- file.path(OUT.DIR,promoters.win.bed)

		if ( !file.exists(promoters.win.bed)) {
			system(paste0(
				"awk 'BEGIN { wn = ", win.size, "; OFS=\"\\t\"} { rng=$3-$2; rng=(rng<0?-rng:rng); ln=int(rng/wn); tmp=(rng%wn==0?0:1); for (i=0;i<ln;i++) { print $1,$2+i*wn,$2+(i+1)*wn,$4,$5,$6	} if ( tmp == 1 ) print $1,$2+ln*wn,$3,$4,$5,$6}' ", promoters.bed, " > ", promoters.win.bed ))
			
		}
		
		promoters.bed <- promoters.win.bed
	}
	
	# data file storing the counts so that we do not hv 2 recount everything if we re-run it with different minCount or get an error somewhere after counting
	out.data.file <- paste0("expr.data.",annot.bed,".UP",extend.up,".DOWN",extend.down,".WIN",win.size,".RData")	
	if ( !file.exists(file.path(OUT.DIR,out.data.file)) ) {
		# count hw many rds map to the promoters
		cat("Constructing promoter expression matrix.\n")
		exp.mx.names <- c()
		exp.mx.cond <- c()
		exp.mx.repl <- c()
		exp.mx <- c()  # will hold the expression matrix: gene promoters x samples
		exp.mx.lib.sizes <- c()
		for ( cond in unique( exp.spec$condition ) ) {
			for (repl in unique( exp.spec[exp.spec$condition == cond, ]$replicate )) {  # unique: if its PE then each replicate appears twice
				base.name <- paste0(cond,"-",repl)
				f.aln <- paste0(base.name,"_aligned_ext.bed")
				f.aln <- file.path("STATS",f.aln)
				
				exp.mx.cond <- c(exp.mx.cond, cond)
				exp.mx.repl <- c(exp.mx.repl, repl)
				exp.mx.names <- c(exp.mx.names, base.name)
				exp.mx.lib.sizes <- c(exp.mx.lib.sizes, as.numeric(system(paste0("wc -l < ",f.aln), intern=TRUE)))
				
				# count hw many rds from the current sample overlap each gene promoter
				cat("\tProcessing sample file", f.aln, "\n")
				exp.mx <- cbind(
					exp.mx,
					as.numeric(system(paste0("intersectBed -c -a ", promoters.bed, " -b ", f.aln, " | awk '{print $NF}'"), intern=TRUE)) )
			}
		}
		
		colnames( exp.mx ) <- exp.mx.names
		rownames( exp.mx ) <- system(paste0("cut -f4 ", promoters.bed ), intern=TRUE )
		exp.mx.cond <- factor( exp.mx.cond )
		
		
		expr.data <- list(
			exp.mx = exp.mx,
			exp.mx.names = exp.mx.names,
			exp.mx.cond = exp.mx.cond,
			exp.mx.repl = exp.mx.repl,
			exp.mx.lib.sizes = exp.mx.lib.sizes
		)
		
		save(expr.data, file = file.path(OUT.DIR,out.data.file))
	}
	
	cat("Loading expr.data..\n")
	load(file.path(OUT.DIR,out.data.file))
	exp.mx.names <- expr.data$exp.mx.names
	exp.mx.cond <- expr.data$exp.mx.cond
	exp.mx.repl <- expr.data$exp.mx.repl
	exp.mx <- expr.data$exp.mx
	exp.mx.lib.sizes <- expr.data$exp.mx.lib.sizes

	
	cat("Dim b4 filtering: ")
	print( dim( exp.mx ) )
	print( head( exp.mx ) )		
	
	# filter matrix
	min.samples <- 4
	exp.mx <- exp.mx[
		apply( exp.mx, 1, function(x)
			any(tapply(x,exp.mx.cond, function(y) sum(y >= minCount) >= min.samples)) ), ]  # keep only those genes that have more than minCount reads in at least min.sample samples of a group
	#exp.mx <- exp.mx[apply(exp.mx,1, function(x) sum(x >= minCount) >= min.samples), ]  # keep only those genes that have more than minCount reads in at least min.sample samples
	#exp.mx <- exp.mx[apply(exp.mx,1, function(x) any(x >= minCount)), ]  # keep only those genes that have more than minCount reads in at least one sample
	#exp.mx <- exp.mx[apply(exp.mx,1, function(x) all(x >= minCount)), ]  # keep only those genes that have more than minCount reads in all samples
	
	
	cat("Dim after filtering: ")
	print( dim( exp.mx ) )
	print( head( exp.mx ) )
	
#	expr.data.filtered <- list(
#			exp.mx = exp.mx,
#			exp.mx.names = exp.mx.names,
#			exp.mx.cond = exp.mx.cond,
#			exp.mx.repl = exp.mx.repl,
#			exp.mx.lib.sizes = exp.mx.lib.sizes
#		)
		
#	save(expr.data.filtered, file = file.path(OUT.DIR,paste0(out.data.file,".filt")))
	
	# normalize to CPM
#	exp.mx.cpm <- sweep(exp.mx, 2, exp.mx.lib.sizes, "/") * 1e6
	#exp.mx.cpm <- counts.filtered[apply(exp.mx.cpm,1, function(x) all(x >= filter.minCPM.Expr)), ]  # keep only those genes that have more than filter.minCPM.Expr reads in all samples
	
	#pdf("boxplot.pdf")
		#boxplot(exp.mx ~ exp.mx.cond)
		#dotchart(x$mpg,...
	#dev.off()
	
	if ( FALSE ) {
		## Raw:
		# calculate mean and sd
		cat("Calculating raw means and sd's.\n")
		cond.means <- t(apply(exp.mx,1,function(x) tapply(x,exp.mx.cond,mean)))
		colnames(cond.means) <- paste0("mean.",colnames(cond.means))
		cond.sds <- t(apply(exp.mx,1,function(x) tapply(x,exp.mx.cond,sd)))
		colnames(cond.sds) <- paste0("sd.",colnames(cond.sds))
		exp.means.sds <- as.data.frame(cbind(exp.mx, cond.means, cond.sds))
		#colnames( exp.means.sds ) <- c(exp.mx.names, paste0("mean",":",levels(exp.mx.factor)), paste0("sd",":",levels(exp.mx.factor)))
		print(head(exp.means.sds))
		
		# write resulting table to file
		exp.mens.sds.file.name <- paste0(annot.bed,".UP",extend.up,".DOWN",extend.down,".promoter.exp" )
		cat("Writting file: ", exp.mens.sds.file.name, "\n")
		write.table( exp.means.sds, file=file.path(OUT.DIR,exp.mens.sds.file.name), sep="\t", quote=FALSE, col.names=TRUE, row.names=TRUE )
		
		## Same for CPM mx:
		# calculate mean and sd
		cat("Calculating CPM means and sd's.\n")
		cond.cpm.means <- t(apply(exp.mx.cpm,1,function(x) tapply(x,exp.mx.cond,mean)))
		cond.cpm.sds <- t(apply(exp.mx.cpm,1,function(x) tapply(x,exp.mx.cond,sd)))
		exp.cpm.means.sds <- as.data.frame(cbind(exp.mx.cpm, cond.cpm.means, cond.cpm.sds))
		#colnames( exp.means.sds ) <- c(exp.mx.names, paste0("mean",":",levels(exp.mx.factor)), paste0("sd",":",levels(exp.mx.factor)))
		print(head(exp.cpm.means.sds))
		
		# write resulting table to file
		exp.cpm.mens.sds.file.name <- paste0(annot.bed,".UP",extend.up,".DOWN",extend.down,".promoter.CPM.exp" )
		cat("Writting file: ", exp.cpm.mens.sds.file.name, "\n")
		write.table( exp.cpm.means.sds, file=file.path(OUT.DIR,exp.cpm.mens.sds.file.name), sep="\t", quote=FALSE, col.names=TRUE, row.names=TRUE )
	}


	if ( DMR.REGIONS.TTEST ) {
		###
		### two-sample t-test
		p.signif <- .05
		
		# t-test for raw counts
		p.raw.count <- apply(exp.mx,1, function(x) t.test(x ~ exp.mx.cond)$p.value )
		p.raw.count <- p.raw.count[!is.na(p.raw.count)]
		p.raw.de <- p.raw.count[ p.raw.count < p.signif ]
		print(p.raw.de)
		# TBD TBD: mult. testing correction
		
		# t-test for CPM counts
		p.cpm.count <- apply(exp.mx.cpm,1, function(x) t.test(x ~ exp.mx.cond)$p.value )
		p.cpm.count <- p.cpm.count[!is.na(p.cpm.count)]
		p.cpm.de <- p.cpm.count[ p.cpm.count < p.signif ]
		print(p.cpm.de)
		# TBD TBD: mult. testing correction
		
		# dotplots of the raw and CPM t-test DMR expressions
		cat("Plotting expression dotcharts of the raw and CPM DMRs.\n")
		pdf("DMRs.t.raw.CPM.pdf")
			for ( n in names(p.raw.de) ) {
				dotchart(
					exp.mx[n, ],
					labels(colnames(exp.mx)),
					cex=.7,
					groups=exp.mx.cond,
					color=seq_along(levels(exp.mx.cond)),
					pch=19,
					main="Raw Expression Level for each sample\ngrouped by treatment",
					xlab="Raw Expression Level"
					)
			}
			for ( n in names(p.cpm.de) ) {
				dotchart(
					exp.mx.cpm[n, ],
					labels(colnames(exp.mx.cpm)),
					cex=.7,
					groups=exp.mx.cond,
					color=seq_along(levels(exp.mx.cond)),
					pch=19,
					main="CPM Expression Level for each sample\ngrouped by treatment",
					xlab="CPM Expression Level"
					)
			}
		dev.off()
	}		
	
	if ( DMR.REGIONS.ANOVA ) {
		###
		### One-way ANOVA
		
	}		
			

	###
	### look for DMRs using edgeR
	if ( DMR.REGIONS.EDGER ) {
		pdf(file.path(OUT.DIR,"EDGER.PLOTS.pdf"))
			cat("***** Raw counts:\n")					
			findDEGenes(counts=list(exp.mx=exp.mx), group=exp.mx.cond, repl=factor(exp.mx.repl), filter.minExpr=DE.EDGER.MIN.EXPR, pFDR=DE.EDGER.FDR, 
				tagwiseDisp=DE.EDGER.TAGWISE.DISPERSION, rem.samples=NULL, outFile=file.path(OUT.DIR, paste0(annot.bed,".DE")), caption=annot.bed, ID=annot.bed )
		dev.off()
	}
	
	return(NULL)
}





			
printResultsToFile <- function( dge, decide.tests, top.genes.pass, outFile, gene.description ) {
		
	#cat("\ncpm(dge, normalized.lib.sizes=TRUE)[rownames(top.genes.pass),] \n")
	#print(cpm(dge, normalized.lib.sizes=TRUE)[rownames(top.genes.pass),])
		
	# TBD: dge$counts  vs. cpm(dge)  vs.  cpm(dge, normalized.lib.sizes=T)  vs  dge$pseudo.alt
	if (debug) cat("printResultsToFile:00\n")	
	cat( "\n\n*\n", file=outFile, append=TRUE )
	cat( "* ", gene.description, "\n", file=outFile, append=TRUE )
	cat( "*\n", file=outFile, append=TRUE )
	
	if ( !is.null(decide.tests) ) {
		print( summary( decide.tests ) )
		if (debug) cat("printResultsToFile:01\n")	
		write.table( summary( decide.tests ), file=outFile, append=TRUE, sep="\t", quote=FALSE )
	}
	
	cat("Lib sizes:\n", file=outFile, append=TRUE )
	write.table( dge$samples$lib.size, file=outFile, append=TRUE, sep="\t", quote=FALSE )
	cat("\nTop DEGs:\n", file=outFile, append=TRUE )
	write.table( top.genes.pass, file=outFile, append=TRUE, sep="\t", quote=FALSE )
	cat("\nCPM values of the DE genes (counts per mill using the TMM normalized lib sizes):\n", file=outFile, append=TRUE )
	write.table( cpm(dge, normalized.lib.sizes=TRUE)[rownames(top.genes.pass),],  file=outFile, append=TRUE, sep="\t", quote=FALSE )
	cat( "\n\n", file=outFile, append=TRUE )
	cat("\nRaw counts for the DE genes:\n", file=outFile, append=TRUE )
	write.table( dge$counts[rownames(top.genes.pass),],  file=outFile, append=TRUE, sep="\t", quote=FALSE )
	cat( "\n", file=outFile, append=TRUE )

	return( NULL )
}			
			
			
			
			
			

findDEGenes <- function( counts, group, repl, filter.minExpr = 5, pFDR=0.05, tagwiseDisp=FALSE, rem.samples=NULL, outFile="out_DE.txt", caption="DE Genes", ID="", append=FALSE ) {
	## #################
	## Description:
	##   Performs DE analysis: for each expr. matrix in the list of expr. matrices 'counts'
	##     filter the matrix
	##     create DGEList object, plot ..., TMM normalization, estimate common , plots ...
	##
	## Parameters:
	##   counts = list of expression matrices
	##   group = factor indicating which of the two groups (conditions) each sample comes from
	##   filter.minExpr = minimum # of reads mapping to a gene accross all libraries for it to be considerer in the DE analysis
	##   pFDR = BH multiple testing correction FDR cutoff for DE genes
	##   tawiseDisp = estimate tagwise dispersion?
	##   outFile = file containing DE genes output
	##   rem.samples = which samples to excluded from the analysis
	##
	## Returns:
	##   ...
	##############
	
	cat("\n\n#######################\n")
	cat("###", caption, "\n")
	cat("#######################\n\n")
	
	cat("pFDR =", pFDR, "\n\n")
	
	if ( debug ) print( rem.samples )
	if ( debug ) print( colnames(counts[[1]]) )
		
	if ( !is.null( rem.samples ) ) {
		group <- factor(group[-rem.samples])
		repl <- factor(repl[-rem.samples])
	}

	cat("group variable:\n")
	cat( group )  # conditions factor (numbered alphabetically)
	cat("\n")
	cat("replicate variable:\n")
	cat( repl )  # replicate factor (numbered alphabetically)
	cat("\n")


	# Initialize output file:
	cat( "###\n", file=outFile, append=append )
	cat( "###", caption, "###\n", file=outFile, append=TRUE )
	cat( "###\n", file=outFile, append=TRUE )
		
	for ( i in 1:length(names(counts)) ) {
		cat( paste("### Finding DE ", names(counts)[i], " genes:", sep=""), "\n" )
		counts.filtered <- as.data.frame( counts[[i]] )  # convert to data.frame in case there's only one line after filtering
		colnames( counts.filtered ) <- colnames(counts[[i]])

		if ( !is.null( rem.samples ) ) {
			counts.filtered <- counts.filtered[, -rem.samples]
		}

		cat( "Dim b4 filtering: " )
		print( dim(counts.filtered))

		# TBD which way is better?
		#counts.filtered <- counts.filtered[apply(counts.filtered,1,sum) > filter.minExpr, ]  # keep only those genes that have more than filter.minExpr pooled reads
		counts.filtered <- counts.filtered[apply(counts.filtered,1, function(x) any(x >= filter.minExpr)), ]  # keep only those genes that have more than filter.minExpr reads in at least one sample
		#keep <- rowSums(cpm(counts.filtered) > 100) >= 2
		#counts.filtered <- counts.filtered[keep, ]

		cat( "Dim after filtering: " )
		print( dim(counts.filtered))
		
		if( dim( counts.filtered )[1] == 0 ) {
			cat("No genes left after filtering. Cannot perform DE analysis.\n\n")
			next
		}
		if( dim( counts.filtered )[1] == 1 ) {
			cat("Only one gene left after filtering. Cannot perform DE analysis:")
			print(counts.filtered)
			cat("\n\n")
			next
		}
		
		#print( colnames( as.matrix(counts.filtered) ))
		#print( colnames( counts.filtered ))
		#print( colnames(counts[[i]]))

		## plot heatmap of filtered genes
#		if (debug) cat("Drawing heatmap\n")
#		png(paste("heatmap_", names(counts)[i], ".png", sep=""))
		#	heatmap.2(as.matrix(counts.filtered), col=hmcol, scale = "row", ColSideColors = palette()[1:ncol(counts.filtered)], cexRow = .5, key = TRUE, density.info = "none", trace = "none")
#		dev.off()
		
		
		##
		## DE analysis
		##
		if (debug) cat("Bulding DGEList object\n")
		# DGEList takes a matrix or data.frame of counts (expression matrix)
		dge <- DGEList( counts.filtered, lib.size=colSums(counts.filtered), group=group, remove.zeros=TRUE )
#		dge <- DGEList( counts.filtered[,c(1:8,10)], lib.size=colSums(counts.filtered[,c(1:8,10)]), group=group[1:9] )
		
		
	#	print(dge)
		if(debug) cat("01\n")
		dge <- tryCatch( {
			cat("Calculating normalization factors\n")
		#	print(dge)
			dge <- calcNormFactors(dge,method="TMM")  # TMM (default) normalization => effective lib size = norm.factor * lib.size
	      print(dge$samples)
			dge
		},error=function(e) {
			message("!!Calculating norm factors failed.")
	      message("!!Here's the original error message:")
	      message(e)
	      message("\n!!Continuing with the next RNA species.")
	      # Choose a return value in case of error
	      return(NA)
	   },
	   	finally={
		      # NOTE:
		      # Here goes everything that should be executed at the end,
		      # regardless of success or error.
		      # If you want more than one expression to be executed, then you 
		      # need to wrap them in curly brackets ({...}); otherwise you could
		      # just have written 'finally=<expression>' 
		      message("...")
	      }
	   )
	    
	    
	  #  if(debug) cat("-02\n")
	  # print(dge)
	  #  if(debug) cat("-03\n")
		if ( is.na(dge) ) next
		cat("\n\n")
	#	if(debug) cat("-04\n") 
	    # multi-dimensional scaling plot:
		# "distances correspond to BCV between each pair of the samples for the most heterogeneous genes"
		# "a type of unsupervised clustering"

		cat( "MDS plot..\n" )
	#	plotMDS(dge, title=names(counts)[i])
			    
	    if ( DE.EDGER.GLM ) {  ### GLM edgeR
	    	# create the design matrix
	    	if ( DE.EDGER.PAIRED ) {
	    		subject <- repl
				treatment <- group
				design <- model.matrix(~subject+treatment)  # = ~replicate+condition; models the expression as the sum of a group and a treatment effect
				rownames(design) <- colnames(dge)
	    	} else {  # samples are not paired
	    		design <- model.matrix(~group, data=dge$samples)  # model group effect only
	    	}
	    	
	    	cat("design:\n")
	    	print(design)
	    	cat("\n")
	    	
	    	dge <- tryCatch( {
		        cat("Estimating commond dispersion\n")
		    #    print(dge)
				dge <- estimateGLMCommonDisp(dge,design, verbose=TRUE)
				dge <- estimateGLMTrendedDisp(dge,design)
			#	print(dge)
				# TBD: If I estimate the tagwise dispersion I loose some DE genes bc. the FDR increases. Y?
				
				if ( tagwiseDisp ) {
					cat("Estimating tagwise dispersion\n")
					dge <- estimateGLMTagwiseDisp(dge,design)  # estimate these from the trended dispersions
				}
				dge
		    },error=function(e) {
		            message("!!Calculating dispersion failed.")
		            message("!!Here's the original error message:")
		            message(e)
		            message("\n!!Continuing with the next RNA species.")
		            # Choose a return value in case of error
		            return(NA)
		        },
		        finally={
		        # NOTE:
		        # Here goes everything that should be executed at the end,
		        # regardless of success or error.
		        # If you want more than one expression to be executed, then you 
		        # need to wrap them in curly brackets ({...}); otherwise you could
		        # just have written 'finally=<expression>' 
		            message("...")
		        }
	    	)
	    	
	    	#    if(debug) cat("02\n")
		   # print(dge)
		#    if(debug) cat("03\n")
			if ( is.na(dge) ) next
			cat("\n\n")
		#	if(debug) cat("04\n")
	
	    	cat("Fitting model.\n")
	    	fit <- glmFit(dge,design)  # cmp baseline (#1) to each of the others
	    	

				if ( DE.EDGER.PAIRED ) {
					cat("Paired DGE.\n")
					
					# non reference treatment levels (the ones having columns in the model matrix):
					non.ref.treats <- gsub("treatment(.*)", "\\1", colnames(fit$design)[(length(levels(subject))+1):ncol(fit$design)])
					# reference treatment:
					ref.treat <- setdiff( levels(treatment), non.ref.treats )
					
					# test if any treatment had an effect: length(levels(subject)) = # replicates: the treatment types are specified in the columns following these.
					cat("Testing for differences in treatments: ", non.ref.treats, "\n")
					lrt <- glmLRT(dge, fit, coef=(length(levels(subject))+1):ncol(fit$design))  # Digital Gene Expression Likelihood Ratio Test data: 'coef' indicates which coeficients of the lm r to b tested = 0; default coef = last column (ncol(fit$design)), which is the (last) treatment effect
					
					top.genes <- topTags(lrt, n = sum(lrt$table$PValue < pFDR), adjust.method="BH", sort.by="p.value")  # PValue is always smaller than the corresp. adj. FDR, so we at least get all those that will have an FDR < pFDR.
					top.genes.pass <- subset( top.genes$table, FDR < pFDR )
								
					cat(nrow(top.genes.pass), " DE genes due to differences in any of the following treatments (wrt. ", ref.treat, "): ", non.ref.treats, "\n")
			#		print( top.genes.pass )
					
					if (debug) cat(":00\n")				
					
					n.treats <- length(levels(treatment))
					if ( n.treats >= 3 ) {
						decide.tests <- NULL
					} else {
						decide.tests <- decideTestsDGE( lrt )
					}
					
					# write results to file
					printResultsToFile( dge, decide.tests, top.genes.pass, outFile, gene.description=paste0(names( counts )[i], ": ", paste0(non.ref.treats,collapse=", "), " vs. ", ref.treat ))
					
					if (debug) cat(":01\n")					
				
				if (DE.EDGER.CONTRASTS.ALL) {
					# if > 0 DE genes and there are >= 3 treatments (>= 2 non-basal treatments), check all possible pairwise contrasts
					if (nrow(top.genes.pass) > 0 && n.treats >= 3) {
						
						# the effect of each treatment vs basal
						for ( treat in (length(levels(subject))+1):ncol(fit$design) ) {  # the columns in the design matrix after the subjects columns indicate the various treatments
							lrt <- glmLRT(dge, fit, coef=treat)  # likelihood ratio test of treatment against basal
							top.genes <- topTags(lrt, n = sum(lrt$table$PValue < pFDR), adjust.method="BH", sort.by="p.value")
							top.genes.pass <- subset( top.genes$table, FDR < pFDR )
							
							current.treat <- gsub("treatment(.*)", "\\1", colnames(fit$design)[treat])
							cat(nrow(top.genes.pass), " DE genes due to contrast: ", current.treat, " - ", ref.treat, "\n")
				#			print( top.genes.pass )
							
							if (debug) cat(":02\n")				
							
							# write results to file
							printResultsToFile( dge, decide.tests=decideTestsDGE( lrt ), top.genes.pass, outFile,
								gene.description=paste0( names( counts )[i], ": ", current.treat, " - ", ref.treat ) )
						}
						
						# the effect of one non-basal treatment vs another non-basal treatment
						# combinations(size of source vect, size of target vect, source vect)
						treat.pairs <- combinations( n.treats-1, 2, (length(levels(subject))+1):ncol(fit$design) )  # all non-basal treatments
						for ( tp in seq_along(treat.pairs[ ,1]) ) {  # for each treatment pair
							contrast.mx <- rep(0,ncol(design))
							contrast.mx[ treat.pairs[tp, ][1] ] <- 1
							contrast.mx[ treat.pairs[tp, ][2] ] <- -1
							lrt <- glmLRT(dge, fit, contrast=contrast.mx)
							top.genes <- topTags(lrt, n = sum(lrt$table$PValue < pFDR), adjust.method="BH", sort.by="p.value")
				#			top.genes.pass <- subset( top.genes$table, FDR < pFDR )
							
							current.treat.1 <- gsub("treatment(.*)", "\\1", colnames(fit$design)[treat.pairs[tp, ][1]])
							current.treat.2 <- gsub("treatment(.*)", "\\1", colnames(fit$design)[treat.pairs[tp, ][2]])
														
							cat(nrow(top.genes.pass), " DE genes due to contrast ", current.treat.1, "-",  current.treat.2, "\n")
							print( top.genes.pass )
										
							# write results to file
							printResultsToFile( dge, decide.tests=decideTestsDGE( lrt ), top.genes.pass, outFile,
								gene.description=paste0(
									names( counts )[i], ": ",
									current.treat.1, "-",  current.treat.2 ) )
						}
					}
				} else if (DE.EDGER.CONTRASTS.CUSTOM) {
					contrast.mx <- makeContrasts( DE.EDGER.CONTRASTS, levels=design )
					
					for (j in 1:ncol(contrast.mx)) {
						lrt <- glmLRT(dge, fit, contrast = contrast.mx[,j])
						top.genes <- topTags(lrt, n = sum(lrt$table$PValue < pFDR), adjust.method="BH", sort.by="p.value")
						top.genes.pass <- subset( top.genes$table, FDR < pFDR )
						cat(nrow(top.genes.pass), " DE genes due to contrast ", DE.EDGER.CONTRASTS[j], "\n")
			#			print( top.genes.pass )
		
						# write results to file
						printResultsToFile( dge, decide.tests=decideTestsDGE( lrt ), top.genes.pass, outFile, gene.description=paste0( names( counts )[i], ": ", DE.EDGER.CONTRASTS[j] ) )
		
						#tempLR <- lrt$table[2]
						#if(exists("LogCPM")){  LogCPM<  <- cbind(LogCPM, tempLR)   }
						#else{ LogCPM  <- tempLR }
					}
				}
				cat("\n\n")
				
			} else {
		    	# ANOVA (for >=3 conditions)
		    	# Digital Gene Expression Likelihood Ratio Test data: 'coef' indicates which coeficients of the lm r to b tested = 0; default coef = last column (ncol(fit$design))
		    	cat("DGE ANOVA.\n")
		    	lrt <- glmLRT(dge,fit,coef=2:length(levels(group)))  # find genes different between any of the n=length(levels(group)) groups
   				top.genes <- topTags(lrt, n = sum(lrt$table$PValue < pFDR), adjust.method="BH", sort.by="p.value")  # PValue is always smaller than the corresp. adj. FDR, so we at least get all those that will have an FDR < pFDR.
				top.genes.pass <- subset( top.genes$table, FDR < pFDR )
				
				# non reference treatment levels (the ones having columns in the model matrix):
				non.ref.treats <- gsub("group(.*)", "\\1", colnames(fit$design)[2:length(levels(group))])
				# reference treatment:
				ref.treat <- setdiff( levels(group), non.ref.treats )
							
				cat(nrow(top.genes.pass), " DE genes due to differences in any of the treatments (wrt. ", ref.treat, ") ", non.ref.treats, "\n")
	#			print( top.genes.pass )
				
				n.treats <- length(levels(group))
				if ( n.treats >= 3 ) {
					decide.tests <- NULL
				} else {
					decide.tests <- decideTestsDGE( lrt )
				}

				# write results to file
				printResultsToFile( dge, decide.tests, top.genes.pass, outFile, gene.description=paste0(names( counts )[i], ": ", paste0(non.ref.treats,collapse=", "), " vs. ", ref.treat ) )
				
				if (DE.EDGER.CONTRASTS.ALL) {
					# if > 0 DE genes and there are >= 3 treatments (>= 2 non-basal treatments), check all possible pairwise contrasts
					n.treats <- length(levels(group))
					if (nrow(top.genes.pass) > 0 && n.treats >= 3) {
						
						for ( treat in 2:ncol(fit$design) ) {  # the columns in the design matrix after the subjects columns indicate the various treatments
							lrt <- glmLRT(dge, fit, coef=treat)  # likelihood ratio test of treatment against basal
							top.genes <- topTags(lrt, n = sum(lrt$table$PValue < pFDR), adjust.method="BH", sort.by="p.value")
							top.genes.pass <- subset( top.genes$table, FDR < pFDR )
							
							current.treat <- gsub("group(.*)", "\\1", colnames(fit$design)[treat])
							cat(nrow(top.genes.pass), " DE genes due to contrast: ", current.treat, " - ", ref.treat, "\n")
				#			print( top.genes.pass )
							
							if (debug) cat(":02\n")
							
							# write results to file
							printResultsToFile( dge, decide.tests=decideTestsDGE( lrt ), top.genes.pass, outFile,
								gene.description=paste0( names( counts )[i], ": ", current.treat, " - ", ref.treat ) )
						}
						
						# combinations(size of source vect, size of target vect, source vect)
						treat.pairs <- combinations( n.treats-1, 2, 2:ncol(fit$design) )
	
						for ( tp in seq_along(treat.pairs[ ,1]) ) {  # for each treatment pair
							contrast.mx <- rep(0,ncol(design))
							contrast.mx[ treat.pairs[tp, ][1] ] <- 1
							contrast.mx[ treat.pairs[tp, ][2] ] <- -1
							cat("Testing contrast:\n")
							print( contrast.mx )
							lrt <- glmLRT(dge, fit, contrast=contrast.mx)
							top.genes <- topTags(lrt, n = sum(lrt$table$PValue < pFDR), adjust.method="BH", sort.by="p.value")
							top.genes.pass <- subset( top.genes$table, FDR < pFDR )
							
							current.treat.1 <- gsub("group(.*)", "\\1", colnames(fit$design)[treat.pairs[tp, ][1]])
							current.treat.2 <- gsub("group(.*)", "\\1", colnames(fit$design)[treat.pairs[tp, ][2]])
							
							cat(nrow(top.genes.pass), " DE genes due to contrast ", current.treat.1, "-",  current.treat.2, "\n")
			#				print( top.genes.pass )
								
							# write results to file
							printResultsToFile( dge, decide.tests=decideTestsDGE( lrt ), top.genes.pass, outFile,
								gene.description=paste0(
									names( counts )[i], ": ",
									current.treat.1, "-",  current.treat.2 ) )
						}
					}
				} else if (DE.EDGER.CONTRASTS.CUSTOM) {
					contrast.mx <- makeContrasts( DE.EDGER.CONTRASTS, levels=design )
					
					for (j in 1:ncol(contrast.mx)) {
						lrt <- glmLRT(dge, fit, contrast = contrast.mx[,j])
						top.genes <- topTags(lrt, n = sum(lrt$table$PValue < pFDR), adjust.method="BH", sort.by="p.value")
						top.genes.pass <- subset( top.genes$table, FDR < pFDR )
						cat(nrow(top.genes.pass), " DE genes due to contrast ", DE.EDGER.CONTRASTS[j], "\n")
			#			print( top.genes.pass )
						
						# write results to file
						printResultsToFile( dge, decide.tests=decideTestsDGE( lrt ), top.genes.pass, outFile, gene.description=paste0( names( counts )[i], ": ", DE.EDGER.CONTRASTS[j] ) )
		
						#tempLR <- lrt$table[2]
						#if(exists("LogCPM")){  LogCPM<  <- cbind(LogCPM, tempLR)   }
						#else{ LogCPM  <- tempLR }
					}
				}
			}
			cat("\n\n")
	  			
	    	
	    } else { ### classic edgeR
	    	dge <- tryCatch( {
		        cat("Estimating commond dispersion\n")
		    #    print(dge)
				dge <- estimateCommonDisp(dge, verbose=TRUE)
			#	print(dge)
				# TBD: If I estimate the tagwise dispersion I loose some DE genes bc. the FDR increases. Y?
				
				if ( tagwiseDisp ) {
					cat("Estimating tagwise dispersion\n")
					dge <- estimateTagwiseDisp(dge)  # estimate these from the common dispersions
					#dge <- estimateTagwiseDisp(dge,prior.n=getPriorN(dge))
				}
				dge
		    },error=function(e) {
		            message("!!Calculating dispersion failed.")
		            message("!!Here's the original error message:")
		            message(e)
		            message("\n!!Continuing with the next RNA species.")
		            # Choose a return value in case of error
		            return(NA)
		        },
		        finally={
		        # NOTE:
		        # Here goes everything that should be executed at the end,
		        # regardless of success or error.
		        # If you want more than one expression to be executed, then you 
		        # need to wrap them in curly brackets ({...}); otherwise you could
		        # just have written 'finally=<expression>' 
		            message("...")
		        }
	    	)
	    	
	    	#    if(debug) cat("02\n")
		   # print(dge)
		#    if(debug) cat("03\n")
			if ( is.na(dge) ) next
			cat("\n\n")
		#	if(debug) cat("04\n")
	
	    	
	    	# perform the actual DE analysis based on NB distribution
			if (debug) cat("Performing DE test\n")
			group1 <- levels(group)[1]
			group2 <- levels(group)[2]
			cat("\ngroup1: "); cat( group1 )
			cat("\ngroup2: "); cat( group2 )
		
			et <- exactTest(dge, pair=c(group1,group2))	 # comparison: group2 - group1 => pos logFC = upreg in group2 cmp w group1
			de <- decideTestsDGE(et, adjust.method="BH", p.value=pFDR)  # check how many up and down regulated genes there are (only works for two group comparisons)
	#		de <- decideTestsDGE(et, p = pFDR, adjust="BH")  # how many DE genes after mult. test. correction with FDR <= pFDR?
	#		de <- decideTestsDGE(et, p = pFDR, adjust="BY")  # how many DE genes after mult. test. correction with FDR <= pFDR?
			print(summary(de))
			
			
				
			#getPriorN( dge )
			#dge <- estimateTagwiseDisp(dge, prop.used=.95)
			#dge <- estimateTagwiseDisp(dge, prior.n=4, prop.used=.95)
			#dge <- estimateTagwiseDisp(dge, prior.n=2, prop.used=.95)
			#dge <- estimateTagwiseDisp(dge, prior.n=1, prop.used=.95)
			#dge <- estimateTagwiseDisp(dge, prior.n=2, prop.used=.5)
			
			
			# plot of tag-wise BCV
			if (tagwiseDisp) {
				if (debug) cat("Plotting tag-wise BCV\n")
	#		png(paste("bcv_", ID, "_", names(counts)[i], ".png", sep=""))
				plotBCV(dge, cex=0.4)
	#		dev.off()
			}
			
		
			## Plot and store various info.
			if ( any(summary(de)[c("-1","1"),1] != 0) ) {
				detags <- rownames(dge)[as.logical(de)]
				
				# smear plot... TBD this should show that normalization works TBD
	#			png(paste("smear_", ID, "_", names(counts)[i], ".png", sep=""))
					plotSmear(et, de.tags=detags, main=names(counts)[i])
					abline(h = c(-2,2), col = "blue")
	#			dev.off()
				
				# find top BH adjusted de genes and print to screen and to file 'outFile'
				top.genes <- topTags(et, n = sum(et$table$PValue < pFDR), adjust.method="BH", sort.by="p.value")  # PValue is always smaller than the corresp. adj. FDR, so we at least get all those that will have an FDR < pFDR.
				top.genes.pass <- subset( top.genes$table, FDR < pFDR )
				
				cat(paste(group2, "-", group1, "\n", sep=" "))
				print( top.genes.pass )
				

				# write results to file
				printResultsToFile( dge, top.genes.pass, outFile, gene.description=paste0(names( counts )[i], ": ", group2, " - ", group1) )
			
							
				# get differentially expressed RNAs; if there's only one, then it will be a vector; I do the following to make sure it is a matrix so I can use nrow()
				de.rna <- as.matrix( as.data.frame( dge$pseudo.alt )[as.logical(de), ])  # $pseudo.alt = 
	
				# (Biased) plot heatmap of the library size adjusted counts of the DE genes, if there are more than one:
				#if ( !is.null(dim(de.rna)) && dim(de.rna)[1] > 1 ) {  # heatmap.2 expects a matrix with >1 rows
				#	png(paste("heatmap_", ID, "_", names( counts )[i], ".png", sep=""))
				#	heatmap.2(de.rna,col=hmcol, scale = "row", ColSideColors = palette()[1:ncol(de.rna)], cexRow = .5, key = TRUE, density.info = "none", trace = "none")
				#	dev.off()
				#}
				
				# plot boxplots and barplots of the library size adjusted counts of the DE genes
				for (j in 1:nrow(de.rna)) {
				#	png(paste("barBox_", ID, "_", names( counts )[i], "_", rownames(de.rna)[j], ".png", sep=""))
					par(mfrow=c(1,2))
								
					boxplot(log2(de.rna[j, ])~group,col=c("blue","red"), main=rownames(de.rna)[j])
					legend("topright",c(group1,group2),col=c("blue","red"), fill=c("blue","red"))
					
					barplot(de.rna[j, ],col=c(rep("blue",sum(group==group1)),rep("red",sum(group==group2))), main=rownames(de.rna)[j])
					legend("topright",c(group1,group2),col=c("blue","red"),fill=c("blue","red"))
				
					par(mfrow=c(1,1))
				#	dev.off()
				}
				
				#tab <- xtable(top.genes, digits = c(0, 2, 1, 0), caption = "Important results")
				#tab <- xtable(top.genes$table, caption = names(counts)[i])
				#print(tab, file = "foo.html", type = "html", append = FALSE)
	
				
			} else {
				cat( "No DE ", names(counts)[i], " genes.\n" )
			}
			cat("\n\n\n")
		
	    	
	    }  # end classic edgeR
	    	
					
				
		#test.all[[ names(counts)[i] ]] <- et
	}
	cat( "\n", file=outFile, append=TRUE )
	
	return( NULL ) #test.all )	
}



