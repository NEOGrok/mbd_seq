#!/bin/bash
# script name: ./alignPairedEnd.bash file1.fastq file2.fastq cond_name genome_build
# run the script for each pair of filtered, properly matched fastq files

# get root name
cond_name="$1"

# sam to bam
samtools view -Sb ${cond_name}.sam > ${cond_name}_tmp.bam

# keep only properly mapped pairs
samtools view -bf 0x2 ${cond_name}_tmp.bam > ${cond_name}_pair.bam

# sort by leftmost coordinate, index, and remove duplicates (keep the one w highest quality)
samtools sort ${cond_name}_pair.bam ${cond_name}
samtools index ${cond_name}.bam
samtools rmdup -s ${cond_name}.bam ${cond_name}_rmdup.bam

# Create BED file.
#	sort properly paired reads BY NAME (-n):
samtools sort -n ${cond_name}_rmdup.bam ${cond_name}_sortByName

# two lines at a time
bamToBed -i ${cond_name}_sortByName.bam | awk '
{
	OFS="\t";
	chr=$1; st=$2; en=$3; name=$4; str=$6;
	getline;
	
	if ( $4 ~ /^.*\/1$/ ) {
		str_ = $6;
		name_ = $4;

		if (str_ == "+") {
			st_ = $2;
			en_ = en;
		} else {
			st_ = st;
			en_ = $3;
		}
	} else {
		str_ = str;
		name_ = name;

		if (str_ == "+") {
			st_ = st;
			en_ = $3;
		} else {
			st_ = $2;
			en_ = en;
		}
	}
	
	print chr, st_, en_, name_, 0, str_;
}' | grep -v -i "_random" | grep -v -i "chrUn" | sort -k1,1 -k2,2n -k6,6 > ${cond_name}_aligned.bed

# clean up:
rm ${cond_name}.sam
#rm ${cond_name}.bam  # keep this for stats
rm ${cond_name}_pair.bam
rm ${cond_name}_sortByName.bam