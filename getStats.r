##########
### STATS of Contig Analysis
###########
### filename 'getStats.r'
###

#
# DESCRIPTION
#
# There's a data gathering section, and a plotting section.
# Once the data has been collected, it is saved in "exp.stats.obj.RData", and
# can subsequently be loaded without having to re-run the data gathering part
# of the code, which makes experimenting with the plots easier.
#
# Requirements:
# ensGeneTSS.bed
# ensGene.2kb.u.bed
# ensGene.2kb.d.bed


debug <- TRUE

# TBD
# 0 - lib specific reads
# 0 - condition specific reads
# 0 - reads in common btw two or more conditions



##############
## Functions
##############

countFastqReads <- function( fastq ) {
	#
	# DESCRIPTION
	# Counts how many entries there r in a given fastq file.
	#
	# INPUT
	# fastq file with 1. No empty lines, 2. reads and quality strings span ONE LINE only!
	#
	# RETURN
	# Number of reads in the fastq file.
	#

	countCommand <- paste("echo \"$(wc -l <", fastq, ") / 4\" | bc", sep=" ")
	rd.count <- as.numeric(system(countCommand, intern=TRUE))

	return(rd.count)
}


getDistToEnsTSS <- function( bed.file.path ) {
	# DESCRIPTION
	# Given a BED file name this function returns a vector: for each BED entry
	# the distance to the nearest Ensembl gene TSS.

	# get distance from each read to the nearest ensambl gene
	ensGene <- "ensGeneTSS.bed"
	if ( !file.exists(ensGene) ) {
		cat(ensGene,"missing\n")
		q()
	}
	cat("Getting distance to Ensembl TSSs.\n")
	return(
		as.numeric(
			system(paste("closestBed -t first -a", bed.file.path, "-b ensGeneTSS.bed -D \"b\" | awk '{print $NF}'", sep=" "), intern=TRUE)))
}

#closestBed -t first -a hiSeqDepth.bed -b ensGene.bed -D "b" | less
################
## Main
################

if ( file.exists("exp.spec.RData") ) {
	cat("Loading data\n")
	load("exp.spec.RData")  # this should always exist here as it was saved during preprocessin
} else {
	exp.spec$raw <- 0
	exp.spec$clip.adapter.only <- 0
	exp.spec$clip.too.short <- 0
	exp.spec$clip.N <- 0
}



if ( STATS.GATHER.DATA ) {
###########################
### Data gathering
###########################

## List object with all the gathered data
exp.stats.obj <- list(
	exp.spec = NULL,

	rds.per.chr = NULL,
	rd.len.table = NULL,
	rds.per.contig.table = NULL,

	ens.gene.length.table = NULL,
	ens.exon.length.table = NULL,
	dist.btw.ens.gene.length.table = NULL,

	dist.to.tss = NULL,
	rds.per.ensGene.table = NULL,

	ens.gene.length.table = NULL,
	ens.exon.length.table = NULL,
	rds.per.ens.contig.table = NULL,
	dist.btw.ens.contig.table = NULL,

	# pooled data
	pool.aln = NULL,
	pool.cov = NULL,
	pool.dist.to.tss = NULL,
	pool.cgi = NULL,
	pool.cgi.shore = NULL,
	pool.cgi.non.tss = NULL,
	pool.exon = NULL,
	pool.ens = NULL
)



## Print to exp.stats file
# Initialize output stat file where to write stats along the way:
exp.stats <- file.path( OUT.DIR, "exp.stats" )
unlink( exp.stats )  # remove stats file if it already exists


cat("\t#############\n", file=exp.stats)
cat("\t### Stats ###\n", file=exp.stats, append=TRUE)
cat("\t#############\n\n", file=exp.stats, append=TRUE)
system(paste("date >>", exp.stats, sep=" " ))  # print date
cat("\n\n", file=exp.stats, append=TRUE)


for ( cond in unique( exp.spec$condition ) ) {
	for (repl in unique( exp.spec[exp.spec$condition == cond, ]$replicate )) {  # unique: if its PE then each replicate appears twice
		base.name <- paste(cond, repl, sep="-" )
		cat("\n\n### Stats for", base.name, "\n")
		cat("### Stats for", base.name, "\n", file=exp.stats, append=TRUE)

		# the index of the file in the exp.spec data.frame
		idx <- which(exp.spec$condition == cond & exp.spec$replicate == repl)
		idx <- ifelse( is.pe, idx[1], idx )  ## If PE then idx is a vector of length 2. In that case, pick the index of the first pair. (b sure that the pairs r specified in order)
		f.bam <- paste(base.name, ".bam", sep="")

		###
		### VARIOUS PREALIGNMENT AND ALIGNMENT COUNTS
#		# number raw reads; add them as a new column to the exp.spec data.frame
#		cat("Counting raw reads.\n")
#		exp.spec$raw[idx] <- countFastqReads( file.path(fastqDir, exp.spec$filename[idx]) )  # number of reads b4 clipping and trimming
#		if (is.pe) exp.spec$raw[idx+1] <- countFastqReads( file.path(fastqDir, exp.spec$filename[idx+1]) )  # as above for the mate
#  		cat( exp.spec$raw[idx], exp.spec$filename[idx], "\n", file=exp.stats, append=TRUE )  # print to stats file
#
#		# number rds b4 alignment
		cat("Counting clean reads b4 alignment.\n")
#		#clean.file <- paste(base.name, ifelse( is.pe, "_P1_sync.fastq", "_clean.fastq" ), sep="")  # name of pre-alignment file
		exp.spec$clean[idx] <- as.numeric(system(paste("samtools view", f.bam, "| wc -l", sep=" "),intern=TRUE))
		if (is.pe) exp.spec$clean[idx+1] <- exp.spec$clean[idx]
	  	cat( exp.spec$clean[idx], "clean reads b4 alignment\n", file=exp.stats, append=TRUE )

		# number of aligned reads
		cat("Counting aligned reads.\n")
		exp.spec$aln[idx] <- as.numeric(system(paste("samtools view -F 4", f.bam, "| wc -l", sep=" "),intern=TRUE))
		#exp.spec$aln[idx] <- as.numeric(system(paste("wc -l <", aln.file, sep=" "), intern=TRUE))
		if (is.pe) exp.spec$aln[idx+1] <- exp.spec$aln[idx]  # same number in the mate position (there's only one alignment file per mate-pairs)
		cat( exp.spec$aln[idx], "aligned reads\n", file=exp.stats, append=TRUE )

		# hw many failed 2 align
		cat("Counting reads that failed to align.\n")
		failed.2.align.count <- as.numeric(system(paste("samtools view", f.bam, "| awk '$3==4{print $0}' | grep \"XM:i:0\" | wc -l", sep=" "),intern=TRUE))
		exp.spec$failed.2.align[idx] <- failed.2.align.count
		if (is.pe) exp.spec$failed.2.align[idx+1] <- exp.spec$failed.2.align[idx]  # same number in the mate position (there's only one alignment file per mate-pairs)
		cat( exp.spec$failed.2.align[idx], "failed 2 align\n", file=exp.stats, append=TRUE )

		# hw many were suppressed due to the -m setting (usu. -m 1)
		cat("Counting reads suppressed due to -m.\n")
		suppressed.m.count <- as.numeric(system(paste("samtools view", f.bam, "| awk '$3==4{print $0}' | grep \"XM:i:1\" | wc -l", sep=" "),intern=TRUE))
		exp.spec$suppressed.m[idx] <- suppressed.m.count
		if (is.pe) exp.spec$suppressed.m[idx+1] <- exp.spec$suppressed.m[idx]  # same number in the mate position (there's only one alignment file per mate-pairs)
		cat( exp.spec$suppressed.m[idx], "suppressed due to -m\n", file=exp.stats, append=TRUE )

		# Aligned pr chromosome:
		cat("Counting reads pr chromosome.\n")
		idxstats <- system(paste("samtools idxstats", f.bam, sep=" "), intern=TRUE)
		idxstats <- as.data.frame(do.call(rbind, strsplit(idxstats,'\t')), stringsAsFactors = FALSE)
		idxstats <- idxstats[-nrow(idxstats), -4]  # remove last row which contains something else, and last column (# rds that didn't map?)
		idxstats[ ,c(2,3)] <- apply( idxstats[ ,c(2,3)], 2, as.numeric )
		colnames( idxstats ) <- c("chr","chr.len","rds.mapped")
		exp.stats.obj$rds.per.chr[[base.name]] <- idxstats

		# read length distribution
		aln.file <- paste(base.name, "_aligned.bed", sep="" )  # name of aligned file
		rd.len.table <- table(system(paste("awk '{print $3-$2}'", aln.file, sep=" "), intern=TRUE))
		exp.stats.obj$rd.len.table[[base.name]] <- rd.len.table



		###
		### CONTIG STATISTICS
		# Get chromosome lengths file
		if ( !file.exists("chrLengths") ) {
			#system(paste("samtools view -H", aln.file.bam, "| grep '^@SQ' | sed 's/.*SN:\\(chr.*\\).LN:\\(.*\\)$/\\1\\t\\2/' > chrLengths", sep=" "))
			system(paste(
				"mysql --user=genome --host=genome-mysql.cse.ucsc.edu -A -D",
				GENOME.BUILD,
				"-e \"select chrom, size from chromInfo\" | grep -w \"chr[0-9]*[XYM]*\" > chrLengths",
				sep=" "))
		}


		# count duplicates (same chr, start and strand)
		# Since the bam file is from b4 running 'samtools rmdup' and the bed file is from after, the difference
		# is the amount of duplicates that were removed.
		# I say that a read is a duplicate if it has the same start position as another.
		cat("Counting duplicate reads.\n")
		exp.spec$duplicates[idx] <- exp.spec$aln[idx] - as.numeric(system(paste("cut -f1,2,6", aln.file, "| uniq | wc -l", sep=" "), intern=TRUE))
		if (is.pe) exp.spec$duplicates[idx+1] <- exp.spec$duplicates[idx]
		if(debug) print(exp.spec$duplicates[idx])


		# remove duplicates? I skip this for now.
		#if ( STATS.CONTIGS.REMOVE.DUPLICATES ) {
		#	cat("Removing duplicates.\n")
		#	aln.file.no.dup <- file.path( OUT.DIR, paste(base.name, "_aligned_nodup.bed", sep="" ))
		#	system(paste("cut -f1,2,3,6", aln.file, "| uniq | awk '{OFS=\"\t\"; print $1,$2,$3,n++,0,$4}' >", aln.file.no.dup, sep=" "))  # aln.file is already sorted; I could remove more reads by removing all those that have the same start position as some reads have been trimmed.

		#	exp.spec$duplicates[idx] <-
		#		system(paste("wc -l < ", aln.file, sep=""), intern=TRUE) -
		#		system(paste("wc -l < ", aln.file.no.dup, sep=""), intern=TRUE)
		#	if (is.pe) exp.spec$duplicates[idx+1] <- exp.spec$duplicates[idx]

		#	aln.file <- aln.file.no.dup
		#}


		# if SE, then we extend each aligned sequence in the 3' direction to match the size selected fragment length,
		# so from now on, aln.file will proint to the extended sequences.
		if ( !is.pe ) {
			cat("Extending each read", EXTEND.LENGTH, "nts\n")
			ext.aln.file <- file.path( OUT.DIR, paste(base.name, "_aligned_ext.bed", sep="" ))
			system(paste("slopBed -s -l 0 -r", EXTEND.LENGTH, "-i", aln.file, "-g chrLengths | sort -k1,1 -k2,2n >", ext.aln.file, sep=" "))
			aln.file <- ext.aln.file
		}


		cat("Creating contigs.\n")
		# make contigs of alignment bed file and sort it accoring to rds per contig per nt
		# Create strandless contigs (since it's MBD-seq) contig files
		contig.file <- file.path( OUT.DIR, paste(base.name, "_contigs.bed", sep=""))
		# file contains: chr, start, end, rd pr contig pr nt, rd pr contig, contig width
		system(paste(
			"mergeBed -n -i", aln.file,
			"| awk '{OFS=\"\t\"; cwidth=$3-$2; print $1,$2,$3,$4/cwidth *", FRAGMENT.LENGTH, ",$4,cwidth;}' | sort -k4,4nr >", contig.file, sep=" "), intern=TRUE)
		cat("You can now manualy inspect the contig file sorted according 2 rds per contig. E.g. check where the top contigs map 2 at UCSC.\n")

		# number of contigs
		cat("Counting contigs.\n")
		exp.spec$contigs[idx] <- system(paste("wc -l < ", contig.file, sep=""), intern=TRUE)
		if (is.pe) exp.spec$contigs[idx+1] <- exp.spec$contigs[idx]
		cat( exp.spec$contigs[idx], "contigs\n\n", file=exp.stats, append=TRUE )

		# hw many genomic nts do our reads cover at lest 1x
		cat("Counting nt coverage.\n")
		exp.spec$rd.cov[idx] <- as.numeric(system(paste("awk '{sum+=$6} END{print sum}'", contig.file, sep=" "), intern=TRUE))
		if (is.pe) exp.spec$rd.cov[idx+1] <- exp.spec$rd.cov[idx]

		# Reads per contig distribution
		cat("Counting reads per contig.\n")
		rds.per.contig.table <- table(as.numeric(system(paste("cut -f5", contig.file, sep=" "), intern=TRUE)))  # get rds pr contig (col 4 of the contig file
		exp.stats.obj$rds.per.contig.table[[base.name]] <- rds.per.contig.table

		# Normalized reads per contig distribution
		cat("Normalized reads per contig.\n")
		norm.rds.per.contig.table <- table(as.numeric(system(paste("cut -f4", contig.file, sep=" "), intern=TRUE)))  # get rds pr contig (col 4 of the contig file
		exp.stats.obj$norm.rds.per.contig.table[[base.name]] <- norm.rds.per.contig.table

		# Contig size distribution
		cat("Contig size distribution.\n")
		contig.len.table <- table(as.numeric(system(paste("awk '{print $6}'", contig.file, sep=" "), intern=TRUE)))
		exp.stats.obj$contig.len.table[[base.name]] <- contig.len.table


		###
		### How many reads overlap the HiSeqDepth track?
		# This only works for Human right now.
		exp.spec$hiseq[idx] <- 0
		if (is.pe) exp.spec$hiseq[idx+1] <- 0

		if ( ORGANISM == ORG$hs && GENOME.BUILD == "hg19" ) {
			cat("Counting reads that overlap the HiSeqDepth track.\n")

			##
			## Annotate DMRs with Hi-Seq track
			hiseq.tracks <- c(
				"hiSeqDepthTopPt1Pct",
				"hiSeqDepthTopPt5Pct",
				"hiSeqDepthTop1Pct",
				"hiSeqDepthTop5Pct",
				"hiSeqDepthTop10Pct"
			)
			hiseq.tracks <- rev(hiseq.tracks)

			# for each hiSeq track, starting with the HIGHEST pct:
			for ( t in hiseq.tracks ) {
				t.bed <- paste(t,".bed",sep="")

				if ( !file.exists(t.bed) ) {
					cat("Getting track", t, "from UCSC\n")
					command <- paste(
						"mysql -N -B -h genome-mysql.cse.ucsc.edu -A -u genome -D",
						GENOME.BUILD,
						"-e \'select chrom, chromStart, chromEnd from", t, "\' | grep -w \'chr[0-9]*[XYM]*\' >", t.bed, sep=" ")
					system( command )
				}
			}
			system("cat hiSeqDepthTop*.bed | mergeBed -i stdin > hiSeqDepth.bed")
			exp.spec$hiseq[idx] <- as.numeric(system(paste("intersectBed -u -f", STATS.ANNOT.OVERLAP.PCT, "-a", aln.file, "-b hiSeqDepth.bed | wc -l", sep=" "), intern=TRUE))
			if (is.pe) exp.spec$hiseq[idx+1] <- exp.spec$hiseq[idx]

			# mark off which contigs intersect the hiSeqDepth track
			cat("Annotating contigs with HiSeqDepth track.\n")
			contig.hiseq.file <- file.path( OUT.DIR, paste(base.name, "_contigs.hiseq.bed", sep=""))
			# same contig can appear several times after -loj bc. there could be several nonoverlapping HiSeqDepth regions that all intersect the same contig
			system(paste("intersectBed -loj -f", STATS.ANNOT.OVERLAP.PCT, "-a", contig.file, "-b hiSeqDepth.bed >", contig.hiseq.file, sep=" "))
		}

		#intersectBed -loj -f 0.03 -a ctrl-1_contigs.bed -b hiSeqDepth.bed
		###
		### Create BedGraph for viewing at UCSC
		if ( STATS.BEDGRAPH ) {
			cat("\n* Creating BedGraph files.\n")
			bg.file <- file.path( OUT.DIR, paste( base.name, ".bedGraph", sep="" ))  # NB: This is the file extension expected by IGV!
			#cat("track type=bedGraph name=\"", exp.spec$sampleid[idx], "\" description=\"", exp.spec$sampleid[idx], " bed graph\" visibility=full color=200,100,0 altColor=0,100,200 priority=20\n", file=bg.name, sep="" )

			system(paste0("genomeCoverageBed -bg -trackline -trackopts \'name=\"", exp.spec$sampleid[idx], "\" visibility=2 color=255,30,30\' -i ", aln.file, " -g chrLengths > ", bg.file))

			# -trackline -trackopts 'name="basal.pooled" visibility=2 color=255,30,30'
			# genomeCoverageBed -bg -ibam basal.mapped.bam -g chrLengths > basal.pooled.bg
			# genomeCoverageBed -bg -ibam min10.mapped.bam -g chrLengths > min10.pooled.bg
			# genomeCoverageBed -bg -ibam min30.mapped.bam -g chrLengths > min30.pooled.bg
			# genomeCoverageBed -bg -ibam min60.mapped.bam -g chrLengths > min60.pooled.bg

#			bg.plus.file <- file.path( OUT.DIR, paste( base.name, ".plus.bg", sep="" ))
#			bg.minus.file <- file.path( OUT.DIR, paste( base.name, ".minus.bg", sep="" ))
#			bg.both.file <- file.path( OUT.DIR, paste( base.name, ".both.bg", sep="" ))
#			system(paste("awk '$6==\"+\"'", aln.file, " | genomeCoverageBed -bg -trackline -trackopts \'name=\"My Track\" visibility=2 color=255,30,30\' -i stdin -g chrLengths | awk '{print $0\"\t+\"}' >", bg.plus.file, sep=" "))
#			system(paste("awk '$6==\"-\"'", aln.file, " | genomeCoverageBed -bg -trackline -i stdin -g chrLengths | awk '{print $0\"\t-\"}' >", bg.minus.file, sep=" "))
#			system(paste("cat", bg.plus.file, bg.minus.file, "| sort -k1,1 -k2,2n | awk '{OFS=\"\t\"; print $1,$2,$3,NR,$4,$5}' >", bg.both.file, sep=" "))

			# links to the top N bedGraph entries, i.e., the ones w the highest count density
			top.N <- 250
			bg.top.N.file <- file.path( OUT.DIR, paste( base.name, ".bg.top", top.N, ".html", sep="" ))
			system(paste("sort -k4,4nr", bg.file, "| head -n", top.N, "| linksBed -i stdin -org", ORGANISM, "-db", GENOME.BUILD ,">", bg.top.N.file, sep=" "))
			cat( "\n\tYou can now upload the BedGraph files as custom tracks in the  UCSC Genome Browser,\n\t and look at the top",
				top.N,
				"BedGraph entries in the generated HTML file.\n\n" )
		}


		###
		### CGI coverage
		#if ( STATS.UCSC.TRACKS ) {
			cat("Counting CGI and CGI shore statistics.\n")
			# get GCI bed file from UCSC
			if ( !file.exists("cpgIslandExt.bed") ) {
				cat("Getting CGIs from UCSC\n")
				system(paste(
					"mysql -N -B -h genome-mysql.cse.ucsc.edu -A -u genome -D",
					GENOME.BUILD,
					"-e 'select chrom, chromStart, chromEnd, name, cpgNum from cpgIslandExt' | grep -w 'chr[0-9]*[XYM]*' | sort -k1,1 -k2,2n > cpgIslandExt.bed", paste=" "))
			}
			exp.spec$cgi[idx] <- as.numeric(system(paste("intersectBed -u -f", STATS.ANNOT.OVERLAP.PCT, "-a", aln.file, "-b cpgIslandExt.bed| wc -l", sep=" "), intern=TRUE))
			if (is.pe) exp.spec$cgi[idx+1] <- exp.spec$cgi[idx]

			cgi.and.shores <- "cpgIslandAndShores.2kb.bed"
			system(paste("slopBed -b 2000 -i cpgIslandExt.bed -g chrLengths >", cgi.and.shores, sep=" "))

			cgi.shores <- "cpgIslandShores.2kb.bed"
			system(paste("subtractBed -a", cgi.and.shores, "-b cpgIslandExt.bed | sort -k1,1 -k2,2n | mergeBed -i stdin >", cgi.shores, sep=" "))
			exp.spec$cgi.shores[idx] <- as.numeric(system(paste("intersectBed -u -f", STATS.ANNOT.OVERLAP.PCT, "-a", aln.file, "-b", cgi.shores, "| wc -l", sep=" "), intern=TRUE))
			if (is.pe) exp.spec$cgi.shores[idx+1] <- exp.spec$cgi.shores[idx]


			###
			### Repeat masker track annotation
			# get rmsk bed file from UCSC
			if ( !file.exists("rmsk.bed") ) {  # works for human... but not for rat... do it manually 4 now
				cat("Getting rmsk track from UCSC\n")
				system(paste(
					"mysql -N -B -h genome-mysql.cse.ucsc.edu -A -u genome -D",
					GENOME.BUILD,
					"-e 'select genoName, genoStart, genoEnd, repName, repClass, strand from rmsk' | grep -w 'chr[0-9]*[XYM]*' | sort -k1,1 -k2,2n > rmsk.bed", paste=" "))
			}
			cat("Counting reads mapping to the rmsk track.\n")
			exp.spec$rmsk[idx] <- as.numeric(system(paste("intersectBed -u -f", STATS.ANNOT.OVERLAP.PCT, "-a", aln.file, "-b rmsk.bed| wc -l", sep=" "), intern=TRUE))
			if (is.pe) exp.spec$rmsk[idx+1] <- exp.spec$rmsk[idx]
			# TBD: split up into LINE, SINE, LTR, satellite, ...



			###
			### Ensembl genes
			if ( !file.exists("ensGene.bed") ) {
				cat("Getting Ensembl genes from UCSC\n")
				system(paste(
					"mysql -N -B -h genome-mysql.cse.ucsc.edu -A -u genome -D",
					GENOME.BUILD,
					"-e 'select chrom, txStart, txEnd, exonCount, exonStarts, exonEnds, name, name2, strand from ensGene' | grep -w 'chr[0-9]*[XYM]*' > ensGenes", paste=" "))

				# create ensembl bed file
				system("awk 'BEGIN{OFS=\"\\t\"}{print $1,$2,$3,$7,$8,$9}' ensGenes | sort -k1,1 -k2,2n > ensGene.bed")

				# create ensembl TSS bed file
				# Same start and end coordinates => 1nt range since start is 0-based and end is 1-based. But it gives an error sometimes, so I make it 2 nt long.
				system("awk 'BEGIN{OFS=\"\\t\"}{if ($NF==\"+\") {print $1,$2,$2+1,$4,$5,$6} else {print $1,$3,$3+1,$4,$5,$6}}' ensGene.bed > ensGeneTSS.bed")
			#	system("awk 'BEGIN{OFS=\"\\t\"}{if ($NF==\"+\") {print $1,$2,$2,$4,$5,$6} else {print $1,$3,$3,$4,$5,$6}}' ensGene.bed | head")

				# create ensembl exons bed file
				system("awk 'BEGIN{OFS=\"\\t\"} {split($5,exonStarts,\",\"); split($6,exonEnds,\",\"); for (i=1; i<=$4; i++) { print $1,exonStarts[i],exonEnds[i],$7\":exon_\"i,$8,$9}}' ensGenes | sort -k1,1 -k2,2n > ensGene.exon.bed")



				#	awk 'BEGIN{OFS="\t"}
				#		{
				#			split($5,exonStarts,",");
				#			split($6,exonEnds,",");
	#
	#						for (i=1; i<=$4; i++) {
	#							print $1,exonStarts[i],exonEnds[i],$7":exon_"i,$8,$9
	#						}
	#					}' ensGenes > ensGene.exon.bed

			}
			cat("Counting reads mapping to Ensembl genes.\n")
			exp.spec$ensGene[idx] <- as.numeric(system(paste("intersectBed -u -f", STATS.ANNOT.OVERLAP.PCT, "-a", aln.file, "-b ensGene.bed| wc -l", sep=" "), intern=TRUE))
			if (is.pe) exp.spec$ensGene[idx+1] <- exp.spec$ensGene[idx]


			# get distance from each read to the nearest ensambl gene
			exp.stats.obj$dist.to.tss[[base.name]] <- getDistToEnsTSS( aln.file )

			# one nt overlap is enough here
			cat("Counting reads mapping to Ensembl genes TSSs.\n")
			exp.spec$ensGene.tss[idx] <- as.numeric(system(paste("intersectBed -u -a", aln.file, "-b ensGeneTSS.bed | wc -l", sep=" "), intern=TRUE))
			if (is.pe) exp.spec$ensGene.tss[idx+1] <- exp.spec$ensGene.tss[idx]

			cat("Reads per Ensembl gene.\n")
			rds.per.ensGene.table <- table(as.numeric(system(paste("intersectBed -c -f", STATS.ANNOT.OVERLAP.PCT, "-b", aln.file, "-a ensGene.bed| cut -f7", sep=" "), intern=TRUE)))
			exp.stats.obj$rds.per.ensGene.table[[base.name]] <- rds.per.ensGene.table


			cat("Counting reads mapping 2kb b4 and after all Ensembl genes.\n")
			ensGene.2kb.d <- "ensGene.2kb.d.bed"
			ensGene.2kb.u <- "ensGene.2kb.u.bed"
			if ( !file.exists(ensGene.2kb.d) || !file.exists(ensGene.2kb.u) ) {
				cat(ensGene.2kb.d, "and/or", ensGene.2kb.u, "missing\n")
				cat("Creating them now...\n")
				system(paste("slopBed -l 2000 -r 0 -s -i ensGene.bed -g chrLengths | subtractBed -a stdin -b ensGene.bed | sort -k1,1 -k2,2n | mergeBed -s -i stdin | awk 'BEGIN{OFS=\"\\t\"}{print $1,$2,$3,\"ensGene.2kb.u:\"n++,0,$4}' | sort -k1,1 -k2,2n >", ensGene.2kb.u, sep=" "))
				system(paste("slopBed -l 0 -r 2000 -s -i ensGene.bed -g chrLengths | subtractBed -a stdin -b ensGene.bed | sort -k1,1 -k2,2n | mergeBed -s -i stdin | awk 'BEGIN{OFS=\"\\t\"}{print $1,$2,$3,\"ensGene.2kb.d:\"n++,0,$4}' | sort -k1,1 -k2,2n >", ensGene.2kb.d, sep=" "))
			}

			exp.spec$ensGene.2kb.d[idx] <- as.numeric(system(paste("intersectBed -u -f", STATS.ANNOT.OVERLAP.PCT, "-a", aln.file, "-b", ensGene.2kb.d, "| wc -l", sep=" "), intern=TRUE))
			exp.spec$ensGene.2kb.u[idx] <- as.numeric(system(paste("intersectBed -u -f", STATS.ANNOT.OVERLAP.PCT, "-a", aln.file, "-b", ensGene.2kb.u, "| wc -l", sep=" "), intern=TRUE))
			if (is.pe) exp.spec$ensGene.2kb.d[idx+1] <- exp.spec$ensGene.2kb.d[idx]
			if (is.pe) exp.spec$ensGene.2kb.u[idx+1] <- exp.spec$ensGene.2kb.u[idx]


			# non TSS CGIs have been shown to b more meth than TSS CGIs... let's test this
			cgi.nonTSS <- "cgi.nonTSS.bed"
			system(paste("subtractBed -a cpgIslandExt.bed -b ensGene.2kb.u.bed | sort -k1,1 -k2,2n >", cgi.nonTSS, sep=" "))
			exp.spec$cgi.nonTSS[idx] <- as.numeric(system(paste("intersectBed -u -f", STATS.ANNOT.OVERLAP.PCT, "-a", aln.file, "-b", cgi.nonTSS, "| wc -l", sep=" "), intern=TRUE))
			if (is.pe) exp.spec$cgi.nonTSS[idx+1] < exp.spec$cgi.nonTSS[idx]

			cgi.TSS <- "cgi.TSS.bed"
			system(paste("intersectBed -a cpgIslandExt.bed -b ensGene.2kb.u.bed | sort -k1,1 -k2,2n >", cgi.TSS, sep=" "))
			exp.spec$cgi.TSS[idx] <- as.numeric(system(paste("intersectBed -u -f", STATS.ANNOT.OVERLAP.PCT, "-a", aln.file, "-b", cgi.TSS, "| wc -l", sep=" "), intern=TRUE))
			if (is.pe) exp.spec$cgi.TSS[idx+1] < exp.spec$cgi.TSS[idx]


			# ensGene exons (includes UTR's)
			cat("Counting reads mapping to Ensembl exons.\n")
			exp.spec$ensGene.exon[idx] <- as.numeric(system(paste("intersectBed -u -f", STATS.ANNOT.OVERLAP.PCT, "-a", aln.file, "-b ensGene.exon.bed | wc -l", sep=" "), intern=TRUE))
			if (is.pe) exp.spec$ensGene.exon[idx+1] <- exp.spec$ensGene.exon[idx]
		#}
	}
}


#if ( STATS.ENS.GENE.STATS ) {
	cat("Ensembl gene statistics\n")
	if(debug)print("stats::01")
	exp.stats.obj$ens.gene.length.table <- table(as.numeric(system("awk '{print $3-$2}' ensGene.bed", intern=TRUE)))
	if(debug)print("stats::02")
	exp.stats.obj$ens.exon.length.table <- table(as.numeric(system("awk '{print $3-$2}' ensGene.exon.bed", intern=TRUE)))
	if(debug)print("stats::03")
	system("mergeBed -n -i ensGene.bed | sort -k1,1 -k2,2 > ensGene.contigs.bed")
	exp.stats.obj$features.per.ens.contig.table <- table(as.numeric(system("cut -f4 ensGene.contigs.bed", intern=TRUE)))
	exp.stats.obj$dist.btw.ens.contig <- as.numeric(system(
		"awk 'BEGIN{chr=\"chr1\";e=0} {if($1!=chr){chr=$1} else {print $2-e; e=$3}}' ensGene.contigs.bed", intern=TRUE))
	if(debug)print("stats::04")
	if(debug)print("stats::05")
	if(debug)print("stats::06")
	print(head(as.data.frame(exp.stats.obj$dist.btw.ens.contig)))
#}
if(debug)print("stats::07")




###
### Genome coverage statistics
cat("Calculating genome coverage statistics\n")
ens.cov <- as.numeric(system("mergeBed -i ensGene.bed | awk '{sum+=$3-$2} END{print sum}'", intern=TRUE))
ens.tss.cov <- as.numeric(system("wc -l < ensGeneTSS.bed", intern=TRUE))  # = #TSSs
ens.exon.cov <- as.numeric(system("mergeBed -i ensGene.exon.bed | awk '{sum+=$3-$2} END{print sum}'", intern=TRUE))
ens.intron.cov <- ens.cov - ens.exon.cov
ens.2kb.d.cov <- as.numeric(system("mergeBed -i ensGene.2kb.d.bed | awk '{sum+=$3-$2} END{print sum}'", intern=TRUE))
ens.2kb.u.cov <- as.numeric(system("mergeBed -i ensGene.2kb.u.bed | awk '{sum+=$3-$2} END{print sum}'", intern=TRUE))
intergenic.cov <- as.numeric(system("awk '{sum += $2} END{print sum}' chrLengths", intern=TRUE)) - ens.cov
rmsk.cov <- as.numeric(system("mergeBed -i rmsk.bed | awk '{sum+=$3-$2} END{print sum}'", intern=TRUE))
hiseq.cov <- as.numeric(system("mergeBed -i hiSeqDepth.bed | awk '{sum+=$3-$2} END{print sum}'", intern=TRUE))
cgi.cov <- as.numeric(system("mergeBed -i cpgIslandExt.bed | awk '{sum+=$3-$2} END{print sum}'", intern=TRUE))
cgi.nonTSS.cov <- as.numeric(system("mergeBed -i cgi.nonTSS.bed | awk '{sum+=$3-$2} END{print sum}'", intern=TRUE))
cgi.TSS.cov <- as.numeric(system("mergeBed -i cgi.TSS.bed | awk '{sum+=$3-$2} END{print sum}'", intern=TRUE))
cgi.shores.cov <- as.numeric(system("mergeBed -i cpgIslandShores.2kb.bed | awk '{sum+=$3-$2} END{print sum}'", intern=TRUE))

cov.stats <- c(
	ens.cov,
	ens.exon.cov,
	ens.intron.cov,
	ens.2kb.d.cov,
	ens.2kb.u.cov,
	intergenic.cov,
	rmsk.cov,
	hiseq.cov,
	cgi.cov,
	cgi.nonTSS.cov,
	cgi.TSS.cov,
	cgi.shores.cov,
	exp.spec$rd.cov )
cov.stats.names <- c("ens","ens.exon","ens.intron","ens.2kb.d","ens.2kb.u","intergenic","rmsk","hiseq","cgi","cgi.nonTSS","cgi.TSS","cgi.shore",exp.spec$sampleid)

# calculate pooled sample stats
pool.cov <- c()
pool.aln <- c()
for ( cond in unique(exp.spec$condition) ) {
	ext.aln.pooled.file <- file.path( OUT.DIR, paste(cond, "_aligned_ext.bed", sep="" ))
	if ( !file.exists( ext.aln.pooled.file ) ) {
		ext.aln.files <- file.path( OUT.DIR, paste(cond, "-*_aligned_ext.bed", sep="" ))
		system(paste("cat", ext.aln.files, "| sort -k1,1 -k2,2n | mergeBed -i stdin >", ext.aln.pooled.file, sep=" "))
	}
	pool.cov <- c(pool.cov, as.numeric(system(paste("awk '{sum+=$3-$2} END{print sum}'", ext.aln.pooled.file, sep=" "), intern=TRUE)))
	pool.aln <- c(pool.aln, sum(exp.spec$aln[exp.spec$condition==cond]))
	pool.dist.TSS <- getDistToEnsTSS( ext.aln.pooled.file )
	cov.stats.names <- c(cov.stats.names, cond)
}
cov.stats <- c(cov.stats, pool.cov)
names( cov.stats ) <- cov.stats.names

exp.stats.obj$pool.aln <- pool.aln

print(cov.stats.names)
print(cov.stats)


exp.stats.obj$cov.stats <- cov.stats
exp.stats.obj$exp.spec <- exp.spec
save(exp.stats.obj, file="exp.stats.obj.RData")
}  # STATS.GATHER.DATA







###########################
### Sequencing stat plots
###########################
cat("Plotting statistics\n")


library(gplots)

if ( file.exists( "exp.stats.obj.RData" ) ) {
	load("exp.stats.obj.RData")
	exp.spec <- exp.stats.obj$exp.spec
	cov.stats <- exp.stats.obj$cov.stats
}



# Open up a PDF plotting device:
pdf.name <- file.path( OUT.DIR, "sample.stats.pdf" )  # plot name
pdf(pdf.name)



#subset( exp.spec, pair==1, select ...
###
### Plot the sample data statistics obtained above
for ( cond in unique( exp.spec$condition ) ) {
	for (repl in unique( exp.spec[exp.spec$condition == cond, ]$replicate )) {  # unique: if its PE then each replicate appears twice
		base.name <- paste(cond, repl, sep="-" )

		# Plot rds per chr:
		barplot(
			exp.stats.obj$rds.per.chr[[base.name]]$rds.mapped,
			names.arg=exp.stats.obj$rds.per.chr[[base.name]]$chr,
			cex.names=0.5, main="Rds per chromosome", sub=base.name)

		# aligned pr chromosome norm to chrom size
		norm.rds.per.chr <- exp.stats.obj$rds.per.chr[[base.name]]$rds.mapped / exp.stats.obj$rds.per.chr[[base.name]]$chr.len
		barplot(
			norm.rds.per.chr,
			names.arg=exp.stats.obj$rds.per.chr[[base.name]]$chr,
			cex.names=0.5, main="Rds per chr norm to chr length", sub=base.name)
		barplot(  # same but without the outlier chrM
			norm.rds.per.chr[-length(norm.rds.per.chr)],
			names.arg=exp.stats.obj$rds.per.chr[[base.name]]$chr[-length(norm.rds.per.chr)],
			cex.names=0.5, main="Rds per chr norm to chr length", sub=base.name)


		# Plot read length distribution
		mx <- max(as.numeric(as.data.frame(exp.stats.obj$rd.len.table[[base.name]], stringsAsFactors=F)[,1]))
		barplot(exp.stats.obj$rd.len.table[[base.name]], main="Read length distribution", sub=paste(base.name, "; max length = ", mx, sep=""))


		# Plot reads per contig distribution
		mx <- max(as.numeric(as.data.frame(exp.stats.obj$rds.per.contig.table[[base.name]], stringsAsFactors=F)[,1]))
		barplot(
			exp.stats.obj$rds.per.contig.table[[base.name]],
			beside=TRUE, main=paste("Reads per contig distribution (ext +", EXTEND.LENGTH, "nts), max = ", mx, sep=""), sub=base.name)


		# Plot normalized reads per contig distribution
		mx <- max(as.numeric(as.data.frame(exp.stats.obj$norm.rds.per.contig.table[[base.name]], stringsAsFactors=F)[,1]))
		print(mx)
		barplot(
			exp.stats.obj$norm.rds.per.contig.table[[base.name]], xlim=c(1,mx),
			beside=TRUE, main=paste("Normalized reads per contig distribution (ext +", EXTEND.LENGTH, "nts), max = ", mx, sep=""), sub=base.name)
		abline(v=max(as.numeric(as.data.frame(exp.stats.obj$norm.rds.per.contig.table[[base.name]])[[1]])),col="red")
		hist(
			exp.stats.obj$norm.rds.per.contig.table[[base.name]],
			nclass=300, col="purple", border="black", main="Normalized reads per contig distribution", xlab="Reads per contig", ylab="Freq")
		abline(v=max(as.numeric(as.data.frame(exp.stats.obj$norm.rds.per.contig.table[[base.name]])[[1]])),col="red")
		#print(table(rds.per.contig))

		# Plot contig size distribution
		mx <- max(as.numeric(as.data.frame(exp.stats.obj$contig.len.table[[base.name]])[[1]]))
		barplot(
			exp.stats.obj$contig.len.table[[base.name]],
			main=paste("Contig length distribution (ext +", EXTEND.LENGTH, "nts)", sep=""), sub=base.name)
		abline(v=mx, col="red")
		#print(table(contig.len))


		hist(
			exp.stats.obj$dist.to.tss[[base.name]],
			nclass=300, col="purple", border="black", main="Distance of read to closest TSS", xlab="Distance", ylab="Freq")
		hist(
			exp.stats.obj$dist.to.tss[[base.name]], plot=TRUE, xlim=c(-1e5,1e5),
			nclass=600, col="purple", border="black", main="Distance of read to closest TSS", xlab="Distance", ylab="Freq")

		# Reads per Ensembl gene.
		mx <- max(as.numeric(as.data.frame(exp.stats.obj$rds.per.ensGene.table[[base.name]])[[1]]))
		barplot(exp.stats.obj$rds.per.ensGene.table[[base.name]], beside=TRUE, main=paste("Reads per ensGene distribution (ext +", EXTEND.LENGTH, "nts), max = ", mx, sep=""), sub=base.name)
		abline(v=mx,col="red")
		#print(max(rds.per.ensGene))  # ensGene with the most amount of reads mapping within it (could b exon, intron, or both)
	}
}


###
### Ensembl gene statitics plots
barplot(exp.stats.obj$ens.gene.length.table, main="Ensembl gene length distribution")
if(debug)print("stats::08")
abline(v=max(as.numeric(as.data.frame(exp.stats.obj$ens.gene.length.table[[1]]))),col="red")
barplot(exp.stats.obj$ens.exon.length.table, main="Ensembl exon length distribution")
if(debug)print("stats::09")
barplot(exp.stats.obj$features.per.ens.contig.table, main="Features per Ensamble contig")
hist(
	exp.stats.obj$dist.btw.ens.contig,
	nclass=1500, col="purple", border="black", main="Distribution of distances between consecutive Ensembl contigs", xlab="Distance", ylab="Freq")
hist(
	exp.stats.obj$dist.btw.ens.contig, xlim=c(-1e7,1e7),
	nclass=1500, col="purple", border="black", main="Distribution of distances between consecutive Ensembl contigs", xlab="Distance", ylab="Freq")
hist(
	exp.stats.obj$dist.btw.ens.contig, xlim=c(-1e6,1e6),
	nclass=1500, col="purple", border="black", main="Distribution of distances between consecutive Ensembl contigs", xlab="Distance", ylab="Freq")



if(debug)print("stats::10")


# plot coverage
cat("Plotting genome coverage statistics\n")
n.cond <- length(unique( exp.spec$condition ))
pool.cov <- cov.stats[(length(cov.stats)-n.cond+1):length(cov.stats)]
palette(rich.colors(length(cov.stats)-length(exp.spec$rd.cov)-n.cond))


cov.col <- c(
	palette(),
	gray(seq_along(exp.spec$rd.cov)/length(exp.spec$rd.cov)),
	gray(seq_along(pool.cov)/n.cond))
names.cov.stats <- names(cov.stats)
names(cov.stats) <- ""  # trying to fix the labels
if(debug) cat("CovStats plot\n")
cov.mids <- barplot( cov.stats, main="Coverage statistics", beside=TRUE, col=cov.col, names.arg=NULL)
cov.label.y <- rep(1,length(cov.stats))
cov.label.y[seq(1,length(cov.stats),by=2)] <- 2
#mtext(names.cov.stats, 1, at=cov.mids, line=cov.label.y, cex=0.5)
mtext(names.cov.stats, 1, at=cov.mids, adj=1, line=1, cex=0.5, las=3)
names(cov.stats) <- names.cov.stats



if(debug) print(cov.stats)

# coverage vs aligned scatter plot
if(debug) cat("CovStats scatter plot\n")
plot(exp.spec$aln, exp.spec$rd.cov,
	xlim=c(min(exp.spec$aln)-1000, max(exp.stats.obj$pool.aln)),
	ylim=c(min(exp.spec$rd.cov)-10000, max(pool.cov)), pch=19,
	xlab="# aligned reads", ylab="# nts covered",
	col=factor( exp.spec$condition[exp.spec$pair==1] ))
text(exp.spec$aln, exp.spec$rd.cov, exp.spec$sampleid, pos=1, cex=0.4)
# draw a linear regression line through them and through 0,0
cov.aln.lm <- lm(exp.spec$rd.cov ~ exp.spec$aln -1 )
if(debug) cat("Regr line\n")
abline( cov.aln.lm )

# add the pooled data
points(exp.stats.obj$pool.aln, pool.cov, col=factor(unique(exp.spec$condition)),pch=19)
text(exp.stats.obj$pool.aln, pool.cov, unique(exp.spec$condition), pos=1, cex=0.4)

###
### HiSeqDepth dist to TSS histogram
if ( ORGANISM == ORG$hs && GENOME.BUILD == "hg19" ) {
	cat("HiSeqDepth dist to TSS\n")
	hiseq.dist.to.TSS <- getDistToEnsTSS( "hiSeqDepth.bed" )
	hist(	hiseq.dist.to.TSS,
			nclass=300, col="purple", border="black", main="Distance of HiSeqDept track to closest TSS", xlab="Distance", ylab="Freq")
	#hist(	hiseq.dist.to.TSS, xlim=c(-1e5,1e5),
	#		nclass=300, col="purple", border="black", main="Distance of HiSeqDept track to closest TSS", xlab="Distance", ylab="Freq")
}

###
### CGI dist to TSS histogram
cat("CGI dist to TSS\n")
cgi.dist.to.TSS <- getDistToEnsTSS( "cpgIslandExt.bed" )
hist(	cgi.dist.to.TSS,
		nclass=300, col="purple", border="black", main="Distance of CGI track to closest TSS", xlab="Distance", ylab="Freq")
#hist(	cgi.dist.to.TSS, xlim=c(-1e5,1e5),
#		nclass=300, col="purple", border="black", main="Distance of CGI track to closest TSS", xlab="Distance", ylab="Freq")



###
### CGI size distribution
cat("CGI size distribution\n")
cgi.len <- as.numeric(system("awk '{print $3-$2}' cpgIslandExt.bed", intern=TRUE))
barplot(table(cgi.len), main="CGI length distribution")  # the sizes that are not represented (0 CGIs of that size) will not be in the barplot, so the x-axis is not linear
abline(v=max(cgi.len),col="red")


###
### Exon size distribution
cat("Ens exon size distribution\n")
exon.len <- as.numeric(system("awk '{print $3-$2}' ensGene.exon.bed", intern=TRUE))
hist(exon.len, nclass=1500, col="purple", border="black", main=paste("Exon length distribution. max =", max(exon.len), sep=" "), xlab="Distance", ylab="Freq", font.axis=2, font.lab=2, font.main=2, xlim=c(0,2000))
abline(v=max(exon.len),col="red")

###
### Intron size distribution
#cat("Ens intron size distribution\n")
#intron.len <- as.numeric(system("awk '{print $3-$2}' ensGene.intron.bed", intern=TRUE))
#hist(intron.len, nclass=15000, col="purple", border="black", main=paste("Intron length distribution. max =", max(intron.len), sep=" "), xlab="Distance", ylab="Freq", #font.axis=2, font.lab=2, font.main=2, xlim=c(0,10000))
#abline(v=max(intron.len),col="red")



###
### Create matrix with number of raw, adaptor only, too short, N, clean, aligned, and contigs:
cat("Plotting experiment statistics\n")
sample.stats <- rbind(
	as.numeric(exp.spec$raw),
	as.numeric(exp.spec$clip.adapter.only),
	as.numeric(exp.spec$clip.too.short),
	as.numeric(exp.spec$clip.N),
	as.numeric(exp.spec$clean),
	as.numeric(exp.spec$failed.2.align),
	as.numeric(exp.spec$suppressed.m),
	as.numeric(exp.spec$aln),
	as.numeric(exp.spec$hiseq),
	as.numeric(exp.spec$ensGene),
	as.numeric(exp.spec$ensGene.exon),
	as.numeric(exp.spec$ensGene.2kb.d),
	as.numeric(exp.spec$ensGene.2kb.u),
	as.numeric(exp.spec$ensGene.tss),
#	as.numeric(exp.spec$ensGene.and.shores),
#	as.numeric(exp.spec$ensGene.non.rmsk),
	as.numeric(exp.spec$rmsk),
#	as.numeric(exp.spec$rmsk.non.ensGene),
	as.numeric(exp.spec$cgi),
	as.numeric(exp.spec$cgi.nonTSS),
	as.numeric(exp.spec$cgi.TSS),
	as.numeric(exp.spec$cgi.shores),
#	as.numeric(exp.spec$cgi.and.shores),
	as.numeric(exp.spec$contigs) )

rownames( sample.stats ) <- c(
	"raw","adapt only","too short","N","clean",
	"failed2aln","supp.m","aligned",
	"hiseq",
	"ensGene","ensGene.exon","ensGene.2kb.d","ensGene.2kb.u","ensGene.tss",#"ensGene.and.shores",
#	"ensGene.non.rmsk",
	"rmsk",
#	"rmsk.non.ensGene",
	"cgi","cgi.nonTSS","cgi.TSS","cgi.shores",#"cgi.and.shores",
	"contigs" )

colnames( sample.stats ) <- NULL


palette(rich.colors(nrow(sample.stats)))

if(debug) print( sample.stats )
sample.mids <- barplot(sample.stats, beside=TRUE, col=palette(), main=PROJECT.ID, cex.names=0.4 ) #,names.arg=colnames(sample.stats))
legend("topright", rownames( sample.stats ),
	col=palette(), fill=1:nrow(sample.stats))

sample.mids <- sample.mids[ seq(ceiling(nrow(sample.stats)/2), length(sample.mids), by=nrow(sample.stats)) ]
mtext(exp.spec$sampleid, 1, at=sample.mids, adj=1, line=1, cex=0.5, las=3)

# same plot normalized to # raw or clean reads
if ( any( sample.stats["raw", ] == 0 ) ) {
	normalizedToRawClean <- sweep(sample.stats, 2, sample.stats["clean", ], "/")
} else {
	normalizedToRawClean <- sweep(sample.stats, 2, sample.stats["raw", ], "/")
}
sample.mids <- barplot(normalizedToRawClean, beside=TRUE, col=palette(), main=PROJECT.ID, names.arg=colnames(sample.stats), cex.names=0.4)
sample.mids <- sample.mids[ seq(ceiling(nrow(normalizedToRawClean)/2), length(sample.mids), by=nrow(normalizedToRawClean)) ]
mtext(exp.spec$sampleid, 1, at=sample.mids, adj=1, line=1, cex=0.5, las=3)


#
### Stats b4 alignment
#
cat("b4 alignment statistics\n")
preprocess.stats <- rbind(
	as.numeric(exp.spec$raw),
	as.numeric(exp.spec$clip.adapter.only),
	as.numeric(exp.spec$clip.too.short),
	as.numeric(exp.spec$clip.N),
	as.numeric(exp.spec$clean),
	as.numeric(exp.spec$failed.2.align),
	as.numeric(exp.spec$suppressed.m),
	as.numeric(exp.spec$aln) )

rownames( preprocess.stats ) <- c(
	"raw","adapt only","too short","N","clean",
	"failed2aln","supp.m","aligned" )
colnames( preprocess.stats ) <- NULL

palette(rich.colors(nrow(preprocess.stats)))
sample.mids <- barplot(preprocess.stats, beside=TRUE, col=palette(), main=PROJECT.ID, names.arg=colnames(preprocess.stats), cex.names=0.4)
legend("topright", rownames( preprocess.stats ),
	col=palette, fill=1:nrow(preprocess.stats))

sample.mids <- sample.mids[ seq(ceiling(nrow(preprocess.stats)/2), length(sample.mids), by=nrow(preprocess.stats)) ]
mtext(exp.spec$sampleid, 1, at=sample.mids, adj=1, line=1, cex=0.5, las=3)



## norm to #raw rds
if ( any( sample.stats["raw", ] == 0 ) ) {
	normalizedToRawClean <- sweep(preprocess.stats, 2, preprocess.stats["clean", ], "/")
} else {
	normalizedToRawClean <- sweep(preprocess.stats, 2, preprocess.stats["raw", ], "/")
}
sample.mids <- barplot(normalizedToRawClean, beside=TRUE, col=palette(), main="normalized to #raw rds", names.arg=colnames(preprocess.stats), cex.names=0.4)
sample.mids <- sample.mids[ seq(ceiling(nrow(normalizedToRawClean)/2), length(sample.mids), by=nrow(normalizedToRawClean)) ]
mtext(exp.spec$sampleid, 1, at=sample.mids, adj=1, line=1, cex=0.5, las=3)




#
### Stats after alignment
#
cat("after alignment statistics\n")
annot.stats <- rbind(
	as.numeric(exp.spec$aln),
	as.numeric(exp.spec$hiseq),
	as.numeric(exp.spec$duplicates),
	as.numeric(exp.spec$ensGene),
	as.numeric(exp.spec$ensGene.2kb.d),
	as.numeric(exp.spec$ensGene.2kb.u),
	as.numeric(exp.spec$ensGene.tss),
	as.numeric(exp.spec$rmsk),
	as.numeric(exp.spec$cgi),
	as.numeric(exp.spec$cgi.nonTSS),
	as.numeric(exp.spec$cgi.TSS),
	as.numeric(exp.spec$cgi.shores),
	as.numeric(exp.spec$contigs) )

rownames( annot.stats ) <- c(
	"aligned",
	"hiseq",
	"duplicates",
	"ensGene","ensGene.2kb.d","ensGene.2kb.u","ensGene.tss",
	"rmsk",
	"cgi","cgi.nonTSS","cgi.TSS","cgi.shores",
	"contigs" )
colnames( annot.stats ) <- NULL

palette(rich.colors(nrow(annot.stats)))
sample.mids <- barplot(annot.stats, beside=TRUE, col=palette(), main=PROJECT.ID, names.arg=colnames(annot.stats), cex.names=0.4)
legend("topright", rownames( annot.stats ),
	col=palette, fill=1:nrow(annot.stats))
sample.mids <- sample.mids[ seq(ceiling(nrow(annot.stats)/2), length(sample.mids), by=nrow(annot.stats)) ]
mtext(exp.spec$sampleid, 1, at=sample.mids, adj=1, line=1, cex=0.5, las=3)



## norm to #aligned rds
normalizedToAln <- sweep(annot.stats, 2, annot.stats[1, ], "/")
sample.mids <- barplot(normalizedToAln, beside=TRUE, col=palette(), main="normalized to #aligned rds", names.arg=colnames(annot.stats), cex.names=0.4)
sample.mids <- sample.mids[ seq(ceiling(nrow(normalizedToAln)/2), length(sample.mids), by=nrow(normalizedToAln)) ]
mtext(exp.spec$sampleid, 1, at=sample.mids, adj=1, line=1, cex=0.5, las=3)


## and normalized to #nts within the annotated regions
annot.stats.density <- rbind(
	as.numeric(exp.spec$hiseq)/cov.stats["hiseq"],
	as.numeric(exp.spec$ensGene)/cov.stats["ens"],
	as.numeric(exp.spec$ensGene.2kb.d)/cov.stats["ens.2kb.d"],
	as.numeric(exp.spec$ensGene.2kb.u)/cov.stats["ens.2kb.u"],
	as.numeric(exp.spec$rmsk)/cov.stats["rmsk"],
	as.numeric(exp.spec$cgi)/cov.stats["cgi"],
	as.numeric(exp.spec$cgi.nonTSS)/cov.stats["cgi.nonTSS"],
	as.numeric(exp.spec$cgi.TSS)/cov.stats["cgi.TSS"],
	as.numeric(exp.spec$cgi.shores)/cov.stats["cgi.shore"]
)
annot.stats.density <- annot.stats.density * FRAGMENT.LENGTH
rownames( annot.stats.density ) <- c(
	"hiseq",
	"ensGene","ensGene.2kb.d","ensGene.2kb.u",
	"rmsk",
	"cgi","cgi.nonTSS","cgi.TSS","cgi.shores"
)
colnames( annot.stats.density ) <- NULL
palette(rich.colors(nrow(annot.stats.density)))
#palette(rainbow(nrow(annot.stats.density)))
if(debug)print(".....")
if(debug)print(annot.stats.density)

#mar.defaults <- par()$mar
#par(xpd=T, mar=par()$mar+c(0,0,0,4))
sample.mids <- barplot(annot.stats.density, beside=TRUE, col=palette(),
	main="reads per fragment length", names.arg=colnames(annot.stats.density), cex.names=0.4)
legend("topleft", rownames( annot.stats.density ), col=palette, fill=1:nrow(annot.stats.density))
legend("topright", rownames( annot.stats.density ), col=palette, fill=1:nrow(annot.stats.density))

sample.mids <- sample.mids[ seq(ceiling(nrow(annot.stats.density)/2), length(sample.mids), by=nrow(annot.stats.density)) ]
mtext(exp.spec$sampleid, 1, at=sample.mids, adj=1, line=1, cex=0.5, las=3)

#legend(40, 1.5, rownames( annot.stats.density ), col=palette, fill=1:nrow(annot.stats.density), bty="n")
#legend(110, 1.5, rownames( annot.stats.density ), col=palette, fill=1:nrow(annot.stats.density), bty="n")
#par(mar=mar.defaults)  # restore defaults


#
### EnsemblExon, intron, intergenic stats for aligned reads
#
cat("Exon, intron, intergenic stats\n")
#All mapped reads that don't intersect any ensGene must necessarily map to an ensGene intergenic location.
ensGene.intron <- exp.spec$ensGene - exp.spec$ensGene.exon
intergenic <- exp.spec$aln - exp.spec$ensGene

aln.stats <- rbind(
	exp.spec$ensGene.exon,
	ensGene.intron,
	intergenic,
	exp.spec$ensGene.2kb.d,
	exp.spec$ensGene.2kb.u,
	exp.spec$ensGene.tss
)
rownames(aln.stats) <- c(
	"ensGene.exon", "ensGene.intron", "intergenic", "ensGene.2kb.d", "ensGene.2kb.u", "ensGene.tss"
)
colnames( aln.stats ) <- NULL
print(aln.stats)

palette(rich.colors(nrow(aln.stats)))
sample.mids <- barplot(aln.stats, beside=TRUE, col=palette(), main=PROJECT.ID, names.arg=colnames(aln.stats), cex.names=0.4)
legend("topright", rownames( aln.stats ),
	col=1:nrow(aln.stats), fill=1:nrow(aln.stats))
sample.mids <- sample.mids[ seq(ceiling(nrow(aln.stats)/2), length(sample.mids), by=nrow(aln.stats)) ]
mtext(exp.spec$sampleid, 1, at=sample.mids, adj=1, line=1, cex=0.5, las=3)


## and normalized to number of nts covered by the exon / intron / intergenic region:
aln.stats.norm <- rbind(
	exp.spec$ensGene.exon/cov.stats["ens.exon"],
	ensGene.intron/cov.stats["ens.intron"],
	intergenic/cov.stats["intergenic"],
	exp.spec$ensGene.2kb.d/cov.stats["ens.2kb.d"],
	exp.spec$ensGene.2kb.u/cov.stats["ens.2kb.u"]
)
rownames(aln.stats.norm) <- c(
	"ensGene.exon", "ensGene.intron", "intergenic", "ensGene.2kb.d", "ensGene.2kb.u"
)
colnames( aln.stats.norm ) <- NULL
print(aln.stats.norm)

palette(rich.colors(nrow(aln.stats.norm)))
sample.mids <- barplot(aln.stats.norm * FRAGMENT.LENGTH, beside=TRUE, col=palette(), main="reads per fragment length", names.arg=colnames(aln.stats.norm), cex.names=0.4)
legend("topright", rownames( aln.stats.norm ),
	col=1:nrow(aln.stats.norm), fill=1:nrow(aln.stats.norm))
sample.mids <- sample.mids[ seq(ceiling(nrow(aln.stats.norm)/2), length(sample.mids), by=nrow(aln.stats.norm)) ]
mtext(exp.spec$sampleid, 1, at=sample.mids, adj=1, line=1, cex=0.5, las=3)




###
### Misc custom plots
###
cex.names = 0.27
cat("01\n")
annot.stats.density <- rbind(
#	as.numeric(exp.spec$hiseq)/cov.stats["hiseq"],
	as.numeric(exp.spec$ensGene)/cov.stats["ens"],
	as.numeric(exp.spec$ensGene.2kb.d)/cov.stats["ens.2kb.d"],
	as.numeric(exp.spec$ensGene.2kb.u)/cov.stats["ens.2kb.u"],
	as.numeric(exp.spec$rmsk)/cov.stats["rmsk"],
	as.numeric(exp.spec$cgi)/cov.stats["cgi"],
	as.numeric(exp.spec$cgi.nonTSS)/cov.stats["cgi.nonTSS"],
	as.numeric(exp.spec$cgi.TSS)/cov.stats["cgi.TSS"],
	as.numeric(exp.spec$cgi.shores)/cov.stats["cgi.shore"]
)
annot.stats.density <- annot.stats.density * FRAGMENT.LENGTH
rownames( annot.stats.density ) <- c(
#	"hiseq",
	"ensGene","ensGene.2kb.d","ensGene.2kb.u",
	"rmsk",
	"cgi","cgi.nonTSS","cgi.TSS","cgi.shores"
)
colnames( annot.stats.density ) <- NULL
col.names <- paste(exp.spec$sampleid, exp.spec$condition, sep="\n")
palette(rich.colors(nrow(annot.stats.density)))
#palette(rainbow(nrow(annot.stats.density)))


#mar.defaults <- par()$mar
#par(xpd=T, mar=par()$mar+c(0,0,0,4))
sample.mids <- barplot(annot.stats.density, beside=TRUE, col=palette(),
	ylab="Reads per fragment length", names.arg=colnames(annot.stats.density), cex.names=cex.names, font.axis=2, font.lab=2, cex.lab=1.2)
#legend("topright", rownames( annot.stats.density ), col=palette, fill=1:nrow(annot.stats.density))
plotRange.y <- range(annot.stats.density)[2]
legend("topright", rownames( annot.stats.density ), col=palette, fill=1:nrow(annot.stats.density))#, bty="n")
sample.mids <- sample.mids[ seq(ceiling(nrow(annot.stats.density)/2), length(sample.mids), by=nrow(annot.stats.density)) ]
mtext(col.names, 1, at=sample.mids, adj=1, line=1, cex=0.5, las=3)

#legend(43, plotRange.y, rownames( annot.stats.density ), col=palette, fill=1:nrow(annot.stats.density))#, bty="n")
#legend(110, 1.5, rownames( annot.stats.density ), col=palette, fill=1:nrow(annot.stats.density), bty="n")
#par(mar=mar.defaults)  # restore defaults




ensGene.intron <- exp.spec$ensGene - exp.spec$ensGene.exon
intergenic <- exp.spec$aln - exp.spec$ensGene
cat("02\n")
aln.stats <- rbind(
	exp.spec$ensGene.exon,
	ensGene.intron,
	intergenic,
	exp.spec$ensGene.2kb.d,
	exp.spec$ensGene.2kb.u,
	exp.spec$ensGene.tss
)
rownames(aln.stats) <- c(
	"ensGene.exon", "ensGene.intron", "intergenic", "ensGene.2kb.d", "ensGene.2kb.u", "ensGene.tss"
)
colnames( aln.stats ) <- NULL
col.names <- paste(exp.spec$sampleid, exp.spec$condition, sep="\n")
print(aln.stats)

palette(rich.colors(nrow(aln.stats)))
plotRange.y <- range(aln.stats)[2]
sample.mids <- barplot(aln.stats, beside=TRUE, col=palette(), ylab="Reads overlapping feature", names.arg=colnames(aln.stats), cex.names=cex.names, font.axis=2, font.lab=2, cex.lab=1.2)
legend("topright", rownames( aln.stats ),
	col=1:nrow(aln.stats), fill=1:nrow(aln.stats))
#legend(35, plotRange.y, rownames( aln.stats ),
#	col=1:nrow(aln.stats), fill=1:nrow(aln.stats))
sample.mids <- sample.mids[ seq(ceiling(nrow(aln.stats)/2), length(sample.mids), by=nrow(aln.stats)) ]
mtext(col.names, 1, at=sample.mids, adj=1, line=1, cex=0.5, las=3)



cat("03\n")
## and normalized to number of nts covered by the exon / intron / intergenic region:
aln.stats.norm <- rbind(
	exp.spec$ensGene.exon/cov.stats["ens.exon"],
	ensGene.intron/cov.stats["ens.intron"],
	intergenic/cov.stats["intergenic"],
	exp.spec$ensGene.2kb.d/cov.stats["ens.2kb.d"],
	exp.spec$ensGene.2kb.u/cov.stats["ens.2kb.u"]
)
aln.stats.norm <- aln.stats.norm * FRAGMENT.LENGTH
rownames(aln.stats.norm) <- c(
	"ensGene.exon", "ensGene.intron", "intergenic", "ensGene.2kb.d", "ensGene.2kb.u"
)
colnames( aln.stats.norm ) <- NULL
col.names <- paste(exp.spec$sampleid, exp.spec$condition, sep="\n")
print(aln.stats.norm)

palette(rich.colors(nrow(aln.stats.norm)))
plotRange.y <- range(aln.stats.norm)[2]
sample.mids <- barplot(aln.stats.norm, beside=TRUE, col=palette(), ylab="Reads per fragment length", names.arg=colnames(aln.stats.norm), cex.names=cex.names, font.axis=2, font.lab=2, cex.lab=1.2)
legend("topright", rownames( aln.stats.norm ),
	col=1:nrow(aln.stats.norm), fill=1:nrow(aln.stats.norm))
#legend(35, plotRange.y, rownames( aln.stats.norm ),
#	col=1:nrow(aln.stats.norm), fill=1:nrow(aln.stats.norm))
sample.mids <- sample.mids[ seq(ceiling(nrow(aln.stats.norm)/2), length(sample.mids), by=nrow(aln.stats.norm)) ]
mtext(col.names, 1, at=sample.mids, adj=1, line=1, cex=0.5, las=3)




###############################
## for Romain, TBD TBD: insert the right bars:
cat("04\n")
aln.stats.norm <- rbind(
	exp.spec$ensGene.exon/cov.stats["ens.exon"],
	intergenic/cov.stats["intergenic"],
	ensGene.intron/cov.stats["ens.intron"],
	as.numeric(exp.spec$cgi)/cov.stats["cgi"],
	as.numeric(exp.spec$cgi.shores)/cov.stats["cgi.shore"]
)
aln.stats.norm <- aln.stats.norm * FRAGMENT.LENGTH
rownames(aln.stats.norm) <- c(
	"Exon", "Intergenic", "Intron", "CpG Island", "CpG Island Shore"
)
colnames( aln.stats.norm ) <- NULL
col.names <- paste(exp.spec$sampleid, exp.spec$condition, sep="\n")
print(aln.stats.norm)

par(font=2)
palette(rich.colors(nrow(aln.stats.norm)))
#palette(heat.colors(nrow(aln.stats.norm)))
plotRange.y <- range(aln.stats.norm)[2]

sample.mids <- barplot(aln.stats.norm, beside=TRUE, col=palette(), names.arg=colnames(aln.stats.norm), cex.names=cex.names, font.axis=2, font.lab=2, cex.lab=1.2, ylab="Reads per fragment length")
legend("topright", rownames( aln.stats.norm ),
	col=1:nrow(aln.stats.norm), fill=1:nrow(aln.stats.norm))
#legend(32, plotRange.y, rownames( aln.stats.norm ),
#	col=1:nrow(aln.stats.norm), fill=1:nrow(aln.stats.norm), bty="n")
sample.mids <- sample.mids[ seq(ceiling(nrow(aln.stats.norm)/2), length(sample.mids), by=nrow(aln.stats.norm)) ]
mtext(col.names, 1, at=sample.mids, adj=1, line=1, cex=0.5, las=3)



cat("05\n")
cex.names <- 0.6
##
## Pooled samples, same plot:
aln.stats.norm <- c(
	sum(exp.spec$ensGene.exon)/cov.stats["ens.exon"],
	sum(intergenic)/cov.stats["intergenic"],
	sum(ensGene.intron)/cov.stats["ens.intron"],
	sum(as.numeric(exp.spec$cgi))/cov.stats["cgi"],
	sum(as.numeric(exp.spec$cgi.shores))/cov.stats["cgi.shore"]
)
aln.stats.norm <- aln.stats.norm * FRAGMENT.LENGTH
names(aln.stats.norm) <- c(
	"Exon", "Intergenic", "Intron", "CpG Island", "CpG Island Shore"
)
print(aln.stats.norm)

par(font=2)
palette(rich.colors(length(aln.stats.norm)))
#palette(heat.colors(nrow(aln.stats.norm)))
plotRange.y <- range(aln.stats.norm)

barplot(aln.stats.norm, beside=TRUE, main="all samples", col=palette(), names.arg=names(aln.stats.norm), cex.names=cex.names, font.axis=2, font.lab=2, cex.lab=1.2, ylab="Reads per fragment length")
legend("topright", names( aln.stats.norm ),
	col=1:length(aln.stats.norm), fill=1:length(aln.stats.norm), bty="n")



##
## Pooled per condition samples, same plot:
#cond <- "gbp"
#conds <- c("basal","min10","min30","min60")
cat("06\n")
for ( cond in unique( exp.spec$condition ) ) {
	cond.mask <- ( exp.spec$condition == cond )
	aln.stats.norm <- c(
		sum(exp.spec$ensGene.exon[cond.mask])/cov.stats["ens.exon"],
		sum(intergenic[cond.mask])/cov.stats["intergenic"],
		sum(ensGene.intron[cond.mask])/cov.stats["ens.intron"],
		sum(as.numeric(exp.spec$cgi[cond.mask]))/cov.stats["cgi"],
		sum(as.numeric(exp.spec$cgi.shores[cond.mask]))/cov.stats["cgi.shore"]
	)
	aln.stats.norm <- aln.stats.norm * FRAGMENT.LENGTH
	names(aln.stats.norm) <- c(
		"Exon", "Intergenic", "Intron", "CpG Island", "CpG Island Shore"
	)
	print(aln.stats.norm)

	par(font=2)
	palette(rich.colors(length(aln.stats.norm)))
	#palette(heat.colors(nrow(aln.stats.norm)))
	plotRange.y <- range(aln.stats.norm)

	barplot(aln.stats.norm, beside=TRUE, main=cond, col=palette(), names.arg=names(aln.stats.norm), cex.names=cex.names, font.axis=2, font.lab=2, cex.lab=1.2, ylab="Reads per fragment length")
	legend("topright", names( aln.stats.norm ),
		col=1:length(aln.stats.norm), fill=1:length(aln.stats.norm), bty="n")
	#legend(32, plotRange.y, names( aln.stats.norm ),
	#	col=1:nrow(aln.stats.norm), fill=1:nrow(aln.stats.norm), bty="n")
}






#
### TBD: This doesn't seem 2 work... I need this 2 work and replace the above plots with the nicer ggplot2 plots....
#


library("ggplot2")  # grammar of graphics package
library(plyr)
library(reshape)


# frequency barplot - besides
df <- as.data.frame(sample.stats)
df$type <- rownames(df)
d2 <- melt(df, id="type")
#if (debug) print( d2 )
ggplot(d2, aes(variable, value, fill=type)) +
	geom_bar(position="dodge") +
	opts(title="Count plot") +
	xlab("Sample") +
	ylab("Count")

# frequency barplot - besides
df <- as.data.frame(sample.stats)
df$type <- rownames(df)
d2 <- melt(df, id="type")
ggplot(d2, aes(variable, value, fill=type)) +
	geom_bar(position="dodge") +
	opts(title="Count plot") +
	xlab("Sample") +
	ylab("Count")


# sample data
d <- data.frame(expand.grid(x=letters[1:4], g=factor(1:2)), y=rnorm(8))

# Figure 1a, 1b, and 1c.
ggplot(d, aes(x=x, y=y, colour=g)) + opts(title="Figure 1a") + geom_line() + geom_point()
ggplot(d, aes(x=x, y=y, colour=g, group=g)) + opts(title="Figure 1b") + geom_line() + geom_point()
ggplot(d, aes(x=x, y=y, colour=g, group=1)) + opts(title="Figure 1c") + geom_line() + geom_point()



dev.off() # close PDF plotting device

#################################


###
###
if ( FALSE ) {
	# Pool bed files and count contigs and hw many reads in one condition also appear in the other
	cat("Pooling aligned files, finding contigs, and hw many contings are shared\n")
	cat("\n### Stats for pooled contigs\n", file=exp.stats, append=TRUE)
	for ( cond in exp.spec$condition ) {
		pool.bed <- file.path( OUT.DIR, paste( cond, "_pool.bed", sep="" ))
		system(paste("cat ", cond, "*_aligned.bed | sort -k1,1 -k2,2n -k6,6 > ", pool.bed, sep=""))
		system(paste("wc -l", pool.bed, ">>", exp.stats, sep=" " ))  # pooled #aligned rds

		# Create pooled strandless contig files
		contig.name <- file.path( OUT.DIR, paste(cond, "_pool_contigs.bed", sep=""))
		system(paste("mergeBed -n -i ", pool.bed, " > ", contig.name, sep=""))
		system(paste("wc -l", contig.name, ">>", exp.stats, sep=" "))  # count pooled contigs
		cat("'#pooled contigs, #reads/pooled contig'", file=exp.stats, append=TRUE)
		system(paste("cut -f4", contig.name, "| sort -nr | uniq -c >>", exp.stats, sep=" "))
	}
}


###
###
if ( FALSE ) {
	# How many contigs do the various conditions have in common?
	for ( i in 1:(length(exp.spec$condition)-1)) {
		for ( j in (i+1):length(exp.spec$condition)) {
			contig.name.i <- file.path( OUT.DIR, paste(exp.spec$condition[i], "_pool_contigs.bed", sep=""))
			contig.name.j <- file.path( OUT.DIR, paste(exp.spec$condition[j], "_pool_contigs.bed", sep=""))
			cat("contigs in ", contig.name.i, " that overlap with contigs in ", contig.name.j, file=exp.stats, append=TRUE)
			system(paste("intersectBed -wa -u -s -a ", contig.name.i, " -b ", contig.name.j, " | wc -l >> ", exp.stats, sep="" ))
			cat("echo contigs in ", contig.name.j, " that overlap with contigs in ", contig.name.i, file=exp.stats)
			system(paste("intersectBed -wa -u -s -a ", contig.name.j, " -b ", contig.name.i, " | wc -l >> ", exp.stats, sep="" ))
		}
	}
}


# TBD TBD TBD Clean up un-needed files

