###########################
## PIPELINE CONTROLER
###########################

debug <- TRUE

###
### Deal with command line args:
###
args <- commandArgs( trailingOnly=TRUE )  # get the arguments comming after '--args'

## Print help if # args doesn't fit
if ( is.null(args) || length(args) != 1 ) {
	cat( "\t Run the script FROM WITHIN THE SCRIPT DIRECTORY as follows:\n" )
	cat( "\t\t Rscript runPipeline.r RNApipelinesetup.r \n\n" )	
	q()
}

### Get setup file
setup.file <- args[1]
source(setup.file)

# when I source in other scripts
scriptDir <- getwd() # if the usr ran the script from the dir within which the script resides, then this will b the correct dir.

if (debug) {
	cat( "FASTQ.DIR.PATH = ", FASTQ.DIR.PATH )
	cat( "\nPROJECT.DIR.NAME = ", PROJECT.DIR.NAME )
	cat( "\nscriptDir = ", scriptDir )	
}


setwd( FASTQ.DIR.PATH )


clipSpec <- "clipSpec"   # by setting AUTO.OVERREP = FALSE, and filling in sequences into this file in the fastq dir the user can decide what to clip


EXTEND.LENGTH = FRAGMENT.LENGTH - READ.LENGTH  # Hw much to extend each read to obtrain an entire fragment.


### Get and test the spec file:
expSpec <- "expSpec"
exp.spec <- read.table( expSpec, stringsAsFactors=FALSE, header=TRUE )
names(exp.spec) <- tolower(names(exp.spec))  # I refer 2 these in the code, so need 2 make sure it works if the user capitalizes in a differen way
exp.spec$condition <- tolower(exp.spec$condition)

# logical: is it a Paired End experiment?
is.pe <- 2 %in% exp.spec$pair


## Lets look at the spec file
#print( pipeline.spec )
#cat( "\n" )
print( exp.spec )
cat( "\n" )


# Allowed pipeline actions and their correct order (an R list is ordered, so it's elements come in the specified order):
# PPL = PiPeLine
PPL.ACTIONS <- list(
	##
	## Preprocessing and Alignment are *mandatory* actions.
	FASTQC.1 = "FASTQC.1",
	PREPROCESS = "PREPROCESS",
	FASTQC.2 = "FASTQC.2",
	ALIGN = "ALIGN",
	
	##
	## The following are independent analyses that
	# can b run once we hv our mapped reads.
	STATS = "STATS",		# Print more stats
	
	MEDIPS = "MEDIPS",
	REPITOOLS = "REPITOOLS",  # TBD Under construction
	PEAKS = "PEAKS",
	DIFFBIND = "DIFFBIND",
	DMR.COUNT = "DMR.COUNT",
	DMR.REGIONS = "DMR.REGIONS",
	
	TEST.YFG = "TEST.YFG" )  # Test for Your Favorite Genes among the nearest genes to the DMRs found.

	
ORG <- list(hs="hs", rn="rn", mm="mm")

GEN.BUILD <- list(
	hs = c("hg19", "hg18"),
	rn = c("rn4"),
	mm = c("mm9")
)


# TEST: legal organism?
if ( !(ORGANISM %in% names(ORG))  ) {
	cat( paste("Unrecognized organism: '", ORGANISM, "'\n", sep="") )
	cat( "Select one of:\n" )
	cat( "\t", names(ORGS), "\n" )
	q()
}

# TEST: legal genome build?
if ( !(GENOME.BUILD %in% GEN.BUILD[[ORGANISM]])  ) {
	cat( paste("Unrecognized genome build: '", GENOME.BUILD, "' for organism ", ORGANISM, "\n", sep="") )
	cat( "Select one of:\n" )
	cat( "\t", ORGS[[ORGANISM]], "\n" )
}




######################
### Pipeline Start
######################

###
### set up proper working directory and relative dir references:
###
if ( !file.exists(PROJECT.DIR.NAME) ) {
	dir.create(PROJECT.DIR.NAME)
}	
setwd(PROJECT.DIR.NAME) # set working dir
fastqDir = ".."  # from now on the fastq files are on level up in the dir hierarchy

# the directory where the action specific files will b stored
# It will b set b4 commencing each specific pipeline action (except preproc and alignment)
OUT.DIR <- "."


# Have the files already been aligned or filtered? If so, copy them to the project directory and rename them properly.
for (i in 1:nrow(exp.spec)) {
	f.base <- paste(exp.spec$condition[i], exp.spec$replicate[i], sep="-")
	if ( PRE.ALIGNED ) {  # this is also run if both PRE.ALIGNED and PRE.FILTERED are TRUE.
		f.aln <- paste(f.base, "_aligned.bed", sep="")
		system(paste("cp", file.path(fastqDir, exp.spec$filename[i]), f.aln, sep=" "))
	}
	else if ( PRE.FILTERED ) {
		f.fastq <- paste(f.base, "_clean.fastq", sep="")
		
		file.ext <- gsub("^.*\\.(fastq.*)$", "\\1", exp.spec$filename[i])  # 
		is.gz = (file.ext == "fastq.gz")
	
		if (is.gz ) {
			system(paste("gunzip -c", file.path(fastqDir, exp.spec$filename[i]), ">", f.fastq, sep=" "))
		} else {
			system(paste("cp", file.path(fastqDir, exp.spec$filename[i]), f.fastq, sep=" "))
		}
	}
}



###
### Traverse pipeline in the correct order specified above.
###
# TBD: If some steps are skipped, hw do I check that they have been run previously? (nice 2 hv)
for ( a in PPL.ACTIONS ) {
	
	if ( a == PPL.ACTIONS$FASTQC.1 && FASTQC.1 && !PRE.ALIGNED ) {
		cat( "##\n## Initiating action", a, "\n" )
	 	#source(file.path( scriptDir, "fastqc1_allFastq.r"))
	 	
	 	for (f in exp.spec$filename) {
			system(paste("fastqc -o .", file.path(fastqDir, f, sep=" ")))
	 	}		
	}
	
	
	else if ( a == PPL.ACTIONS$PREPROC && PREPROC && !(PRE.ALIGNED || PRE.FILTERED) ) {
		cat( "##\n## Initiating action", a, "\n" )
		
		if ( !PREPROC.PLOT.ONLY ) {
			source(file.path( scriptDir, "preprocess.r"))
		}
		
		load("exp.spec.RData")  # this is the getStats part like in MBD-seq... until I expand on it.
		
		library(gplots)

		# Open up a PDF plotting device:
		pdf.name <- file.path( "preprocess.stats.pdf" )  # plot name
		pdf(pdf.name)
		
		###
		### Create matrix with number of raw, adaptor only, too short, N, clean, aligned, and contigs:
		cat("Plotting experiment statistics\n")
		sample.stats <- rbind(
			as.numeric(exp.spec$raw),
			as.numeric(exp.spec$clip.adapter.only),
			as.numeric(exp.spec$clip.too.short),
			as.numeric(exp.spec$clip.N),
			as.numeric(exp.spec$clean)
		)
				
		rownames( sample.stats ) <- c( "raw", "adapt only", "too short","N","clean" )
		colnames( sample.stats ) <- exp.spec$sampleid
		
		palette(rich.colors(nrow(sample.stats)))
		
		if(debug) print( sample.stats )
		barplot(sample.stats, beside=TRUE, col=palette(), main="read counts", names.arg=colnames(sample.stats), cex.names=.4)
		plotRange.y <- range(sample.stats)[2]
		legend("topleft", plotRange.y, rownames( sample.stats ), col=palette, fill=1:nrow(sample.stats))#, bty="n")
		
		# same plot normalized to # raw reads
		normalizedToRaw <- sweep(sample.stats, 2, sample.stats[1, ], "/")
		barplot(normalizedToRaw, beside=TRUE, col=palette(), main="counts normalized to # raw reads", names.arg=colnames(sample.stats), cex.names=.4)
		
		dev.off()
	}
	
	
	else if ( a == PPL.ACTIONS$FASTQC.2 && FASTQC.2 && !PRE.ALIGNED ) {
		cat( "##\n## Initiating action", a, "\n" )
		
		if ( !is.pe ) {
			system("fastqc *_clean.fastq")
		} else {
			system("fastqc *_sync.fastq")
		}
	}


	else if ( a == PPL.ACTIONS$ALIGN && ALIGN ) {
		cat( "##\n## Initiating action", a, "\n" )
				
		if (GENOME.BUILD == "rn4" ) {
			bowtie.index.path <- paste("/home/local/ngs_course/stud067/Thesis/mbd_cap/VN/rn4_ebwt/", GENOME.BUILD, sep="")
		} else {
			bowtie.index.path <- paste("/home/galaxy/indexes/ebwt/", GENOME.BUILD, sep="")
		}
				
		# Align Single End sequences
		if ( !is.pe ) {
			for ( i in 1:nrow(exp.spec)) {
				f.base <- paste(exp.spec$condition[i], exp.spec$replicate[i], sep="-")
				f.fastq <- paste(f.base, "_clean.fastq", sep="")
				f.sam <- paste(f.base, ".sam", sep="")
				
				if ( !file.exists( f.sam ) ) {
					# align
					command <- paste("/tools/bowtie-0.12.8/bowtie", BOWTIE.PARAMETERS.SE, "-S -p 4 --chunkmbs 256", bowtie.index.path, f.fastq, f.sam, sep=" ")
					cat(command, "\n")
					system(command)  # If u want 2 re-run the file conversion wo. aligning first, just comment this line out.
					
					# convert to indexed BAM, filter away duplicates with samtools rmdup, and create BED file
					if (ALIGN.RMDUP) {
						# the bam file comming out contains all reads (including the unmapped)
						# the BED file is generated after runing samtools rmdup, so it's without duplicates
							# ergo, aligned_ext.bed 
						system(paste("bash", file.path(scriptDir, "samToBamBedSE.bash"), f.base, sep=" "))
					} else {
						system(paste("bash", file.path(scriptDir, "samToBamBedSE_keepDup.bash"), f.base, sep=" "))
					}
				}
			}
		}
		
		# Align Paired End sequences
		else {
			for ( cond in unique(exp.spec$condition) ) {
				for (repl in unique(exp.spec[exp.spec$condition == cond, ]$replicate)) {
					f.base <- paste(cond, repl, sep="-")
			 		p1 <- paste(f.base, "_P1_sync.fastq", sep="")
			 		p2 <- paste(f.base, "_P2_sync.fastq", sep="")
			 		f.sam <- paste(f.base, ".sam", sep="")
			 		
					command <- paste("/tools/bowtie-0.12.8/bowtie", BOWTIE.PARAMETERS.PE, "-S -p 4 --chunkmbs 256", bowtie.index.path, "-1", p1, "-2", p2, f.sam, sep=" ")
					cat(command, "\n")
					system(command)
					
					if (ALIGN.RMDUP) {
						system(paste("bash", file.path(scriptDir, "samToBamBedPE.bash"), f.base, sep=" "))
					} else {			
						system(paste("bash", file.path(scriptDir, "samToBamBedPE_keepDup.bash"), f.base, sep=" "))
					}
				}
			}
		}
	}
	
	
	# ------------------------------------------------------------------
	
	
	else if ( a == PPL.ACTIONS$STATS && STATS ) {
		cat( "##\n## Initiating action", a, "\n" )
		OUT.DIR <- PPL.ACTIONS$STATS
		if ( !file.exists( OUT.DIR ) ) {
			dir.create(OUT.DIR)
		}

		# Counts of reads and contigs after alignment:		
		source(file.path( scriptDir, "getStats.r"))
	}
		
	else if ( a == PPL.ACTIONS$MEDIPS && MEDIPS ) {
		cat( "##\n## Initiating action", a, "\n" )
		OUT.DIR <- PPL.ACTIONS$MEDIPS
		if ( !file.exists( OUT.DIR ) ) {
			dir.create(OUT.DIR)
		}
		OUT.DIR <- file.path(PPL.ACTIONS$MEDIPS, MEDIPS.OUT.SUB.DIR)
		if ( !file.exists( OUT.DIR ) ) {
			dir.create(OUT.DIR)
		}			
		
		for ( i in 1:nrow(exp.spec)) {
			f.base <- paste0(exp.spec$condition[i],"-",exp.spec$replicate[i])
			f.bam <- paste0(f.base, ".bam")
			f.tmp.bam <- paste0(f.base, ".tmp.bam")
			f.mapped <- paste0(f.base, ".mapped")

			if ( !file.exists(paste0(f.mapped,".bam")) ) {	
				# Make BAM files consisting of only aligned files (MEDIPS v. 1.11 will fix this so that it won't b necessary)
				system(paste0("samtools view -bF 4 ", f.bam, " > ", f.tmp.bam))
				system(paste0("samtools sort ",f.tmp.bam," ",f.mapped))
				system(paste0("samtools index ", f.mapped,".bam"))
				
				unlink(f.tmp.bam)
			}
		}
			
		#system(paste("Rscript", file.path( scriptDir, "medipsAnalysis.r" ), scriptDir, sep=" "), wait=FALSE)  # run the command in the background - same as adding '&'
		source(file.path( scriptDir, "medipsAnalysis.r" ))
	}
	
	
	# TBD: This is under construction
	else if ( (OUT.DIR <- a) == PPL.ACTIONS$REPITOOLS && REPITOOLS ) {
		cat( "##\n## Initiating action", a, "\n" )
		if ( !file.exists( OUT.DIR ) ) {
			dir.create(OUT.DIR)
		}
		
		
		### TEST: do this manually for now
		library(Repitools)
		
		bam.files <- paste(exp.spec$condition[exp.spec$pair==1], exp.spec$replicate[exp.spec$pair==1], ".bam", sep="")
		
		# BAM2GRanges( path )  # from Repitools, uses scanBam and only reads in uniquely-mapping reads from a BAN file into a GRanges object
		samples.grl <- BAM2GRangesList( paths=bam.files )  # read in all samples into a GRangesList object
		seqinfo(samples.grl)
		
		enrichmentPlot(samples.grl, seq.len = 500,cols = palette(), xlim = c(0, 10), lwd = 2)
		library(BSgenome.Hsapiens.UCSC.hg19)
		cpgDensityPlot(samples.grl, organism = Hsapiens, w.function = "none", seq.len = 500, cols = palette(), xlim = c(0, 30), lwd = 2)
		
		design.matrix <- matrix(c(0, -1, 0, 1), dimnames = list(names(samples.grl), "C-N"))
		design.matrix
		
		data(TSS.human.GRCh37)
		tss.ensembl <- TSS.human.GRCh37
		stats <- blocksStats(samples.grl, tss.ensembl, up = 2000, down = 0, seq.len = 300, design = design.matrix)
		stats <- stats[order(stats$`adj.p.vals_C-N`), ]
		head(stats)
		
		#annotationCounts
		#genomeBlocks
	}
	
	
	else if ( a == PPL.ACTIONS$PEAKS && PEAKS ) {
		cat( "##\n## Initiating action", a, "\n" )
		OUT.DIR <- PPL.ACTIONS$PEAKS
		if ( !file.exists( OUT.DIR ) ) {
			dir.create(OUT.DIR)
		}
		
		# Counts of reads and contigs after alignment:		
		source(file.path( scriptDir, "callPeaks.r"))
				
		#( 0 - Subtract Hi-Seq track from peaks file (I mark off those DMRs that overlap the HiSeqDepth track instead (hg19)))
	}

	
	else if ( a == PPL.ACTIONS$DIFFBIND && DIFFBIND ) {
		cat( "##\n## Initiating action", a, "\n" )
		OUT.DIR <- PPL.ACTIONS$DIFFBIND
		if ( !file.exists( OUT.DIR ) ) {
			dir.create(OUT.DIR)
		}

		# Run DiffBind.
		source(paste(file.path( scriptDir, "diffBindAnalysis.r" )))
		
		#system(paste("Rscript", file.path( scriptDir, "diffBindAnalysis.r" ), sep=" ") wait=FALSE)  # run in background
	}
	

	else if ( a == PPL.ACTIONS$DMR.COUNT && DMR.COUNT ) {  # must call PEAKS.PAIRED b4 this
		cat( "##\n## Initiating action", a, "\n" )
		OUT.DIR <- PPL.ACTIONS$DMR.COUNT
		if ( !file.exists( OUT.DIR ) ) {
			dir.create(OUT.DIR)
		}
		
		if ( !GET.COMMON.DMR.FEATURES.ONLY ) {
			source(paste(file.path( scriptDir, "customDMR_COUNT.r" )))
		} else {
			source(paste(file.path( scriptDir, "annotateRegions.r" )))
			
			ctrl.list <- list.files(path=OUT.DIR, pattern="^annot\\.TSS\\.ctrl-._5_2_NORM$")
			ctrl.list <- file.path(OUT.DIR, ctrl.list)
			names(ctrl.list) <- ctrl.list
			getCommonDMRFeatures( ctrl.list )
			
			# both common to single and pooled ctrl DMRs
			ctrl.all.list <- list.files(path=OUT.DIR, pattern="^annot\\.TSS\\.ctrl.*_5_2_NORM$")
			ctrl.all.list <- file.path(OUT.DIR, ctrl.all.list)
			names(ctrl.all.list) <- ctrl.all.list
			getCommonDMRFeatures( ctrl.all.list, ".andPool" )
			
			for ( cond in unique(exp.spec$condition) ) {
				cond <- tolower(cond)	
				if ( cond != "ctrl" ) {
					treat.list <- list.files(path=OUT.DIR, pattern=paste("^annot\\.TSS\\.", cond, "-._5_2_NORM$" ,sep=""))
					treat.list <- file.path(OUT.DIR, treat.list)
					names(treat.list) <- treat.list
					getCommonDMRFeatures( treat.list )
					
					# both common to single and pooled ctrl DMRs
					treat.all.list <- list.files(path=OUT.DIR, pattern=paste("^annot\\.TSS\\.", cond, ".*_5_2_NORM$" ,sep=""))
					treat.all.list <- file.path(OUT.DIR, treat.all.list)
					names(treat.all.list) <- treat.all.list
					getCommonDMRFeatures( treat.all.list, ".andPool" )
				}
			}
			
			getCommonDMRFeatures( c(ctrl.list, treat.list), ".ctrlNtreat" )
		}
	}
	
	
	else if ( a == PPL.ACTIONS$DMR.REGIONS && DMR.REGIONS ) {  # must call PEAKS.PAIRED b4 this
		cat( "##\n## Initiating action", a, "\n" )
		OUT.DIR <- PPL.ACTIONS$DMR.REGIONS
		if ( !file.exists( OUT.DIR ) ) {
			dir.create(OUT.DIR)
		}
		
		source(paste(file.path( scriptDir, "dmrRegions.r" )))
		if ( DMR.REGIONS.YFG ) {
			getYfgPromoterCounts( DMR.REGIONS.YFG.PATH, extend.up=DMR.REGIONS.UP, extend.down=DMR.REGIONS.DOWN )
		} else if ( DMR.REGIONS.CUSTOM ) {
			customCounts( DMR.REGIONS.CUSTOM.PATH, minCount=DE.EDGER.MIN.EXPR,
				extend.up=DMR.REGIONS.UP, extend.down=DMR.REGIONS.DOWN, win.size=DMR.REGIONS.WIN.SIZE )
		}
	}
	
	
	else if ( a == PPL.ACTIONS$TEST.YFG && TEST.YFG ) {  # must call PEAKS.PAIRED b4 this
		cat( "##\n## Initiating action", a, "\n" )
		#OUT.DIR <- PPL.ACTIONS$DMR.COUNT
		#if ( !file.exists( OUT.DIR ) ) {
		#	dir.create(OUT.DIR)
		#}
		
		if ( F ) {  # prepare gene list file
			de.genes <- readLines("DE.obesePreGBPvsNormalWomen")
			de.genes <- as.data.frame(matrix(de.genes,ncol=4, byrow=T), stringsAsFactors=FALSE )
			de.genes[ ,3] <- as.numeric(de.genes[ ,3])
			names(de.genes) <- c("name","description","p.value","probeSet")
			
			de.genes <- de.genes[order(de.genes$p.value),]  # order by p.value
			write.table(
				de.genes,
				file="DE.obesePreGBPvsNormalWomen",
				sep = "\t",
				quote = FALSE,
				col.names = TRUE,
				row.names = FALSE
			)	
		}
		
		
		source(paste(file.path( scriptDir, "annotateRegions.r" )))
		
		# generate IGV snapshots file; extend 100kb to each side
		#generateIGVSnapshotsFile( TEST.GENES.PATH, extend=1e5, file.name="default.igv" )
		
			
		OUT.DIR <- PPL.ACTIONS$DMR.COUNT	
		#ctrl.list1 <- list.files(path=OUT.DIR, pattern="^annot\\.TSS\\.ctrl-._5_2_NORM$")
		ctrl.list1 <- list.files(path=OUT.DIR, pattern="^annot\\.TSS\\..*_2_NORM$")
		ctrl.list1 <- file.path(OUT.DIR, ctrl.list1 )
		names(ctrl.list1) <- ctrl.list1
		
		OUT.DIR <- PPL.ACTIONS$MEDIPS
		#ctrl.list2 <- list.files(path=OUT.DIR, pattern="^annot\\.TSS\\.ctrl-.*\\.medips$")
		ctrl.list2 <- list.files(path=OUT.DIR, pattern="^annot\\.TSS\\..*\\.medips.common.andPool$")
		ctrl.list2 <- file.path(OUT.DIR, ctrl.list2 )
		names(ctrl.list2) <- ctrl.list2
		
		OUT.DIR <- PPL.ACTIONS$PEAKS
		ctrl.list3 <- list.files(path=OUT.DIR, pattern="^annot\\.TSS\\.qeseq.*[1-5]$")
		ctrl.list3 <- file.path(OUT.DIR, ctrl.list3 )
		names(ctrl.list3) <- ctrl.list3
		
		ctrl.list <- c(ctrl.list1,ctrl.list2,ctrl.list3)
		if(debug)print(ctrl.list)
		getDMRsFromGenes( ctrl.list, TEST.YFG.PATH, file.id=".test.yfg", igv.margin=6000 )
		
		
		
		if(F){
		for ( cond in unique(exp.spec$condition) ) {
			if ( cond != "ctrl" ) {
				OUT.DIR <- PPL.ACTIONS$DMR.COUNT
				treat.list1 <- list.files(path=OUT.DIR, pattern=paste("^annot\\.TSS\\.", cond, "-._5_2_NORM$" ,sep=""))
				treat.list1 <- file.path(OUT.DIR, treat.list1 )
				names(treat.list1) <- treat.list1
		
				OUT.DIR <- PPL.ACTIONS$DMR.MEDIPS
				treat.list2 <- list.files(path=OUT.DIR, pattern=paste("^annot\\.TSS\\.", cond, "-.*\\.medips.common.andPool$" ,sep=""))
				treat.list2 <- file.path(OUT.DIR, treat.list2 )
				names(treat.list2) <- treat.list2
				
				treat.list <- c(treat.list1,treat.list2)
				getDMRsFromGenes( treat.list, TEST.YFG.PATH, file.id=".test.genes", igv.margin=6000 )
			}
		}}
		
		
		
		
		
		
		
##############		
		
			if(F){
			for ( cond in unique(exp.spec$condition) ) {
				if ( cond != "ctrl" ) {
					if(debug)print("01")
					OUT.DIR <- PPL.ACTIONS$DMR.PEAKS.RELATIVE	
					if(debug)print("02")
					ctrl.list <- list.files(path=OUT.DIR, pattern=paste("^annot\\.TSS\\.qeseq.ctrl-", cond, "-.$", sep=""))
					ctrl.list <- file.path(OUT.DIR, ctrl.list )
					names(ctrl.list) <- ctrl.list
					if(debug)print("03")
					getDMRsFromGenes( ctrl.list, DMR.PEAKS.RELATIVE.YFG.PATH, file.id=".test.genes" )

					treat.list <- list.files(path=OUT.DIR, pattern=paste("^annot\\.TSS\\.qeseq.", cond, "-ctrl-.$" ,sep=""))
					treat.list <- file.path(OUT.DIR, treat.list )
					names(treat.list) <- treat.list
					getDMRsFromGenes( treat.list, DMR.PEAKS.RELATIVE.YFG.PATH, file.id=".test.genes" )
				}
			}}
	
########
		
		
		
		
		
		
	}
	
		
	else if ( a %in% PPL.ACTIONS ) {
		cat( "##\n## Skipping action ", a, "\n\n" )
	}
	
	else { # Should never happen
		cat( "!!\n!! Action ", a, " does not exist.\n\n" )
	}
}

q()		




