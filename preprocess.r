## run FastQC for all fastq files specified in exp.spec and clip n trimm overrep from each file + sync paired end reads

trim5prime.stats <- "trim5prime.stats"  # this file will contain hw many bases r trimmed from the 5' end
unlink( trim5prime.stats )


print(getwd())



countFastqReads <- function( fastq, is.gz = F ) {
	#
	# DESCRIPTION
	# Counts how many entries there r in a given fastq file.
	#
	# INPUT
	# fastq file with 1. No empty lines, 2. reads and quality strings span ONE LINE only!
	#
	# RETURN
	# Number of reads in the fastq file.
	#

	if ( is.gz ) {
		countCommand <- paste("echo \"$(zcat", fastq, "| wc -l) / 4\" | bc", sep=" ")
	} else {
		countCommand <- paste("echo \"$(wc -l <", fastq, ") / 4\" | bc", sep=" ")
	}
	rd.count <- as.numeric(system(countCommand, intern=TRUE))

	return(rd.count)
}


if (file.exists("exp.spec.RData")) {
	load("exp.spec.RData")
}


for (i in seq_along(exp.spec$filename)) {

	### FastQC dir name:
	file.ext <- gsub("^.*\\.(fastq.*)$", "\\1", exp.spec$filename[i])  #
	is.gz = (file.ext == "fastq.gz")

	if (is.gz ) {
		f.base <- gsub("^(.*)\\.fastq.gz", "\\1", exp.spec$filename[i])  # file name without .fastq
	} else {
		f.base <- gsub("^(.*)\\.fastq", "\\1", exp.spec$filename[i])  # file name without .fastq
	}
	fastqc.dir <- paste( f.base, "_fastqc", sep="" )  # dir name with fastqc results

	if ( PREPROC.AUTO.OVERREP & !PREPROC.ITTERATED ) {
		cat( "\n\n**** Processing: ", exp.spec$filename[i], "****\n" )

		overrep.file <- "overrep_contaminant"  # tmp file to hold the overrep seqs


		### run FastQC on the file if it has not already been run
		if (!file.exists(fastqc.dir)) {
			system(paste("fastqc -o .", file.path(fastqDir, exp.spec$filename[i]), sep=" "))
		}

		### Get overrepresented sequences from the FastQC report
		# Get all sequences that cause FastQC overrep seqs to report 'failure' = those that represent > 1% of the total # of seqs in the library
		# and only those that are recognized as some sort of known contaminant, i.e., those that have something else than 'No Hit' in their description field.
		system(paste(
			"sed -n -e '/^>>Overrepresented sequences/,/>>Kmer/p' <", file.path(fastqc.dir , "fastqc_data.txt"),
			" | grep -v '^[>#]' | grep -i -v 'No Hit' | awk '$3 > 2' | sort -k3,3nr > ", overrep.file, sep="" ))

		overrep.count <- as.numeric(system(paste("wc -l <", overrep.file, sep=" " ), intern=TRUE))
		cat("\n\nFound", overrep.count, "overrepresented sequences\n\n")

		if ( overrep.count > 0 ) {
			#system(paste("cat", overrep.file, sep=" "))

			# Pick out each overrep seq contaminant description (except 'No Hit')
			overrep.all <- read.table( overrep.file, stringsAsFactors=FALSE, header=FALSE, sep="\t" )
			names( overrep.all ) <- c("seq", "count", "pct", "description")
			#overrep.all <- overrep.all[ order(overrep.all$pct, decreasing=TRUE), ]
			write.table(overrep.all[ ,1], file=clipSpec, quote = FALSE, col.names = FALSE, row.names = FALSE)
			print( overrep.all )

			#overrep.type <- unique( gsub("^(.*)\\(.*\\)$", "\\1", overrep.all$description) )  # discard the "(96% over 25bp)" kind of stuff
			#for ( overrep in overrep.type ) {
			#	to.clip <- overrep.all[ grep(overrep, overrep.all$description)[1], ]  # just pick the first, as it's the one with the highest %age
			#	cat( to.clip[[1,1]], "\n", file=clipSpec )
			#}
		}
	}


	### New file naming convention from now on: condition-replicate-pair_xyz.fastq/sam/bam/bed
	if ( is.pe ) {
		f.base <- paste(exp.spec$condition[i], exp.spec$replicate[i], exp.spec$pair[i], sep="-")
	} else {
		f.base <- paste(exp.spec$condition[i], exp.spec$replicate[i], sep="-")
	}

	# Name of file after all is done:
	f.clean <- paste(f.base, "_clean.fastq", sep="")


	### Start clipping and trimming:
	if ( PREPROC.ITTERATED ) {  # If we hv run preproc b4.
		if ( PREPROC.AUTO.OVERREP ) {
			cat("Cannot run auto clipping of overrepresented sequences when PREPROC.ITTERATED is set.\n")
			q()
		}

		is.gz <- FALSE
		f.out <- f.clean  # since we've done some preprocessing b4, the input file name ends in '_clean.fastq'
		cat( "\n\n**** Processing: ", f.out, "****\n" )
	} else {
	  	f.out <- file.path(fastqDir, exp.spec$filename[i] )

	  	# if this exists
		if ( file.exists( f.clean ) ) next
	}


	# number raw reads; add them as a new column to the exp.spec data.frame
	cat("Counting raw reads.\n")
	exp.spec$raw[i] <- 0
	exp.spec$raw[i] <- countFastqReads( file.path(fastqDir, exp.spec$filename[i]), is.gz = is.gz )  # number of reads b4 clipping and trimming


	# I prepare these fields here because I have two adaptor clipping clauses below.
  	exp.spec$clip.adapter.only[i] <- 0
  	exp.spec$clip.too.short[i] <- 0
  	exp.spec$clip.N[i] <- 0

  	if ( debug ) print( exp.spec )


  	# Clip adaptors and overrepresented sequences.
  	if (PREPROC.OVERREP.CLIP && overrep.count > 0) {
	  	f.in.clip <- f.out

	  	## load file with seqs 2 b clipped:
		clip.spec <- read.table( clipSpec, stringsAsFactors=FALSE, header=FALSE )

	  	for ( j in 1:nrow(clip.spec) ) {
	  		cat( "\nClipping:", clip.spec[j,1], "\n" )
	  		f.out.clip <- paste(f.base, "_clip", j, ".fastq", sep="")
	  		# -Q 33: assume Sanger encoding, i.e., Phred scores are encoded as ASCII value = PHRED + 33
	  		if ( is.gz ) {
	  			clip.stats <- system(paste("zcat", f.in.clip, "| fastx_clipper -a", clip.spec[j,1], "-l 15 -v -o", f.out.clip, "-Q 33", sep=" "), intern=TRUE)
	  		} else {
	  			clip.stats <- system(paste("fastx_clipper -a", clip.spec[j,1], "-l 15 -v -i", f.in.clip, "-o", f.out.clip, "-Q 33", sep=" "), intern=TRUE)
	  		}

  			if (debug) print( clip.stats )
  			# parse clip.stats: increment adaptor only, increment too short after clipping adaptor

  			too.short <- clip.stats[ grep("too-short reads", clip.stats) ]
  			too.short.count <- as.numeric(gsub("^discarded.([0-9]*).*", "\\1", too.short))
  			exp.spec$clip.too.short[i] <- exp.spec$clip.too.short[i] + too.short.count

  			adapter.only <- clip.stats[ grep("adapter-only reads", clip.stats) ]
  			adapter.only.count <- as.numeric(gsub("^discarded.([0-9]*).*", "\\1", adapter.only))
  			exp.spec$clip.adapter.only[i] <- exp.spec$clip.adapter.only[i] + adapter.only.count

  			N.reads <- clip.stats[ grep("N reads", clip.stats) ]
  			N.reads.count <- as.numeric(gsub("^discarded.([0-9]*).*", "\\1", N.reads))
  			exp.spec$clip.N[i] <- exp.spec$clip.N[i] + N.reads.count

  			if ( debug ) print( exp.spec )

	  		f.in.clip <- f.out.clip
	  		is.gz <- FALSE
	  	}

	  	f.out <- f.in.clip
  	}


  	# Clip adaptors and overrepresented sequences.
  	if (PREPROC.CUSTOM.ADAPTOR != "") {
	  	f.in.clip <- f.out

	  		cat( "\nClipping:", PREPROC.CUSTOM.ADAPTOR, "\n" )
	  		f.out.clip <- paste(f.base, "_clip_custom.fastq", sep="")
	  		# -Q 33: assume Sanger encoding, i.e., Phred scores are encoded as ASCII value = PHRED + 33
	  		if ( is.gz ) {
	  			clip.stats <- system(paste("zcat", f.in.clip, "| fastx_clipper -a", PREPROC.CUSTOM.ADAPTOR, "-l 15 -v -o", f.out.clip, "-Q 33", sep=" "), intern=TRUE)
	  		} else {
	  			clip.stats <- system(paste("fastx_clipper -a", PREPROC.CUSTOM.ADAPTOR, "-l 15 -v -i", f.in.clip, "-o", f.out.clip, "-Q 33", sep=" "), intern=TRUE)
	  		}

  			if (debug) print( clip.stats )
  			# parse clip.stats: increment adaptor only, increment too short after clipping adaptor

  			too.short <- clip.stats[ grep("too-short reads", clip.stats) ]
  			too.short.count <- as.numeric(gsub("^discarded.([0-9]*).*", "\\1", too.short))
  			exp.spec$clip.too.short[i] <- exp.spec$clip.too.short[i] + too.short.count

  			adapter.only <- clip.stats[ grep("adapter-only reads", clip.stats) ]
  			adapter.only.count <- as.numeric(gsub("^discarded.([0-9]*).*", "\\1", adapter.only))
  			exp.spec$clip.adapter.only[i] <- exp.spec$clip.adapter.only[i] + adapter.only.count

  			N.reads <- clip.stats[ grep("N reads", clip.stats) ]
  			N.reads.count <- as.numeric(gsub("^discarded.([0-9]*).*", "\\1", N.reads))
  			exp.spec$clip.N[i] <- exp.spec$clip.N[i] + N.reads.count

  			if ( debug ) print( exp.spec )

	  		f.in.clip <- f.out.clip
	  		is.gz <- FALSE

	  	f.out <- f.in.clip
  	}




  	# Quality trimm 3' end
  	if (PREPROC.TRIM3) {
  		cat( "\n3' quality trim.\n" )
	  	f.in.qt <- f.out
	  	f.out.qt <- paste(f.base, "_qt.fastq", sep="")

	  	if ( is.gz ) {
	  		system(paste("zcat", f.in.qt, "| fastq_quality_trimmer -t 20 -l 15 -o", f.out.qt, "-Q 33", sep=" "))
	  	} else {
	  		system(paste("fastq_quality_trimmer -t 20 -l 15 -i", f.in.qt, "-o", f.out.qt, "-Q 33", sep=" "))
	  	}

	  	f.out <- f.out.qt
	  	is.gz <- FALSE
  	}


	# Filter away low qulity reads
	if (PREPROC.QUALITY.FILTER) {
  		cat( "\nQuality filtering.\n" )
	  	f.in.qf <- f.out
	  	f.out.qf <- paste(f.base, "_qf.fastq", sep="")
	  	# 70% of the nts must hv qual >= 20:
	  	system(paste("fastq_quality_filter -i", f.in.qf, "-o", f.out.qf, "-q 20 -p 70 -Q 33 -v", sep=" "))
	  	f.out <- f.out.qf
	}


	# Trim 5' end as many bases as there are base positions with a 10th percentile quality score under 20
	# TBD is this too strict?
	if (PREPROC.TRIM5) {
	  	n.bases.to.trim <- as.numeric(system(paste(
			"sed -n -e '/^>>Per base sequence quality/,/>>Per sequence quality scores/p' <", file.path(fastqc.dir , "fastqc_data.txt"),
			" | grep -v '^[>#]' | awk 'BEGIN{n=0} {while($6<20){n++;next}print n;exit}'", sep="" ), intern=TRUE))

		print( n.bases.to.trim )

		if ( length(n.bases.to.trim) > 0 && n.bases.to.trim > 0 ) {
			f.in.trim <- f.out
		  	f.out.trim <- paste(f.base, "_trim.fastq", sep="")
		  	f.out.trim.len <- paste(f.base, "_trim_len.fastq", sep="")

		  	# -f = first base to keep
		  	cat( "\n5' trimming the first", n.bases.to.trim, "bases from", exp.spec$filename[i], "\n" )
		  	cat( "\n5' trimming the first", n.bases.to.trim, "bases from", exp.spec$filename[i], "\n", file=trim5prime.stats, append=TRUE )
		  	system(paste("fastx_trimmer -f", n.bases.to.trim + 1, "-i", f.in.trim, "-o", f.out.trim, "-Q 33", sep=" "))
		  	clip.stats <- system(paste("fastx_clipper -a AAAAAAAAAAAAAAA -l 15 -v -i", f.out.trim, "-o", f.out.trim.len, "-Q 33", sep=" "), intern=TRUE)  # only keep reads that r longer than 15 nts

		  	too.short <- clip.stats[ grep("too-short reads", clip.stats) ]
			too.short.count <- as.numeric(gsub("^discarded.([0-9]*).*", "\\1", too.short))
			exp.spec$clip.too.short[i] <- exp.spec$clip.too.short[i] + too.short.count

			adapter.only <- clip.stats[ grep("adapter-only reads", clip.stats) ]
			adapter.only.count <- as.numeric(gsub("^discarded.([0-9]*).*", "\\1", adapter.only))
			exp.spec$clip.adapter.only[i] <- exp.spec$clip.adapter.only[i] + adapter.only.count

			N.reads <- clip.stats[ grep("N reads", clip.stats) ]
			N.reads.count <- as.numeric(gsub("^discarded.([0-9]*).*", "\\1", N.reads))
			exp.spec$clip.N[i] <- exp.spec$clip.N[i] + N.reads.count

			f.out <- f.out.trim.len

			if ( debug ) print( exp.spec )
		}
	}

	## Trimmomatic example
	# java -jar ../../bin/Trimmomatic-0.27/trimmomatic-0.27.jar SE -phred33 -trimlog trimlog.log ctrl-1-1_clean.fastq ctrl-1-1_clean_trimm.fastq LEADING:20 TRAILING:20 SLIDINGWINDOW:5:20 MINLEN:40
	# for f in ctrl-*_clean.fastq; do java -jar ../../bin/Trimmomatic-0.27/trimmomatic-0.27.jar SE -phred33 -trimlog trimlog.log $f $(echo $f | sed 's/\(ctrl.*\)\.fastq/\1_trimm_min75.fastq/') LEADING:20 TRAILING:20 SLIDINGWINDOW:4:23 MINLEN:75; done

	# Re-name the last file:
	system(paste("mv", f.out, f.clean, sep=" "))  # I could 'cp' instead of 'mv' but that will take more time if the file is very large. So I'll live w the warning we get when unlinking a file that does not exist (which could probably b disabled).

	# clean up
  	unlink("*_clip*.fastq")
  	unlink("*_qt.fastq")
  	unlink("*_qf.fastq")
  	unlink("*_trim.fastq")
  	unlink("*_trim_len.fastq")



  	if ( !is.pe ) {  # if it is PE then we first have to sync the mate files (see below)
  		cat("Counting clean reads b4 alignment.\n")
		exp.spec$clean[i] <- 0
		exp.spec$clean[i] <- countFastqReads( f.clean )
  	}


  	print( exp.spec )
  	save(exp.spec, file="exp.spec.RData")
}


if ( is.pe ) {
	### Create properly paired fastq-files (synchronize paired-end reads):
	# for each condition
		# for each replicate
			# run sync bash-script on pair1, pair2
	for ( cond in unique(exp.spec$condition) ) {
		for (repl in unique(exp.spec[exp.spec$condition == cond, ]$replicate)) {
			f.base <- paste(cond, repl, sep="-" )

	 		p1 <- paste(f.base, "-1_clean", ".fastq", sep="")
	 		p2 <- paste(f.base, "-2_clean", ".fastq", sep="")

	 		command <- paste("bash", file.path( scriptDir, "syncronizePairedEnd.bash" ), p1, p2, f.base, sep=" ")
	 		cat( command, "\n" )
			system(command)

			idx <- which(exp.spec$condition == cond & exp.spec$replicate == repl)

			cat("Counting clean reads b4 alignment.\n")
			f.clean <- paste(f.base, "_P1_sync.fastq", sep="")  # name of one of the pre-alignment files
			exp.spec$clean[idx] <- countFastqReads( f.clean )  # both pairs should contain the same # of sequences so I assign them both at once
		}
	}
}



# Count # raw and clean reads per sample
#for ( cond in unique( exp.spec$condition ) ) {
#	for (repl in unique( exp.spec[exp.spec$condition == cond, ]$replicate )) {  # unique: if its PE then each replicate appears twice
#		f.base <- paste(cond, repl, sep="-" )
#		cat("\n\n# Stats for", f.base, "\n")

		# the index of the file in the exp.spec data.frame
#		idx <- which(exp.spec$condition == cond & exp.spec$replicate == repl)
#		idx <- ifelse( is.pe, idx[1], idx )  ## If PE then i is a vector of length 2. In that case, pick the index of the first pair. (b sure that the pairs r specified in order)

		# number raw reads; add them as a new column to the exp.spec data.frame
	#	cat("Counting raw reads.\n")
	#	exp.spec$raw[idx] <- countFastqReads( file.path(fastqDir, exp.spec$filename[idx]) )  # number of reads b4 clipping and trimming
	#	if (is.pe) exp.spec$raw[idx+1] <- countFastqReads( file.path(fastqDir, exp.spec$filename[idx+1]) )  # as above for the mate

		# number rds b4 alignment (clean)
#		cat("Counting clean reads b4 alignment.\n")
#		f.clean <- paste(f.base, ifelse( is.pe, "_P1_sync.fastq", "_clean.fastq" ), sep="")  # name of pre-alignment file
#		exp.spec$clean[idx] <- countFastqReads( f.clean )
#		if (is.pe) exp.spec$clean[idx+1] <- exp.spec$clean[idx]
#	}
#}

print( exp.spec )

# store exp.spec file for use in getStats.r
save(exp.spec, file="exp.spec.RData")
