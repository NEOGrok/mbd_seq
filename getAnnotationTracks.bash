#!/bin/bash
# The purpose of this script is to document hw I get the annotation tracks, and how I process them into the ones I use in the pipeline(s).

# Check the schemas and tables first:
# mysql --user=genome --host=genome-mysql.cse.ucsc.edu -A
# SHOW SCHEMAS;
# USE hg19;
# USE rn4;
# USE mm9;
# DESCRIBE refGene;


### chromInfo
# for use with the -g option with some BEDTools programs
mysql --user=genome --host=genome-mysql.cse.ucsc.edu -A -e "select chrom, size from hg19.chromInfo" | grep -v ^chrom |  grep -w "chr[0-9]*[XYM]*" > hg19.chromInfo
# or
mysql --user=genome --host=genome-mysql.cse.ucsc.edu -A -D hg19 -e "select chrom, size from chromInfo" | grep -v ^chrom |  grep -w "chr[0-9]*[XYM]*" > hg19.chromInfo


### RefSeq HG19
# get UCSC BED file containing refGene:
mysql -N -B -h genome-mysql.cse.ucsc.edu -A -u genome -D hg19 -e 'select chrom, txStart, txEnd, name, name2, strand from refGene' | grep -w "chr[0-9]*[XYM]*" > hg19.refGene


# get wgEncodeSydhNsomeGm12878Sig
mysql -N -B -h genome-mysql.cse.ucsc.edu -A -u genome -D hg19 -e 'select * from wgEncodeSydhNsomeGm12878Sig' > wgEncodeSydhNsomeGm12878Sig.bigWig


# if I merge overlapping genes, I go from 41088 rows to 22099, i.e., around half as many. Q: Is it the isoforms I loose?
mergeBed -s -d -20 -nms -i refseqHG19.bed > refseqHG19_collapsed-20.bed

### knownGene HG19
# get UCSC BED file containing refGene:
mysql -N -B -h genome-mysql.cse.ucsc.edu -A -u genome -D hg19 -e 'select chrom, txStart, txEnd, name, , strand from knownGene' | grep -w "chr[0-9]*[XYM]*" > hg19.knownGene


### ensGene HG19
# get UCSC BED file containing ensGene:
mysql -N -B -h genome-mysql.cse.ucsc.edu -A -u genome -D hg19 -e 'select chrom, txStart, txEnd, name, name2, strand from ensGene' | grep -w "chr[0-9]*[XYM]*" > hg19.ensGene


### all_mrna
# get UCSC BLAT of all mRNAs from GenBank:
mysql -N -B -h genome-mysql.cse.ucsc.edu -A -u genome -D hg19 -e 'select tName, tStart, tEnd, qName, qSize, strand from all_mrna' | grep -w "chr[0-9]*[XYM]*" > hg19.all_mrna


### rmsk HG19
# get UCSC BED file containing ensGene:
mysql -N -B -h genome-mysql.cse.ucsc.edu -A -u genome -D hg19 -e 'select genoName, genoStart, genoEnd, repName, repClass, strand from rmsk' | grep -w "chr[0-9]*[XYM]*" > hg19.rmsk



### 
# Hi Seq Depth 0.1%
mysql -N -B -h genome-mysql.cse.ucsc.edu -A -u genome -D hg19 -e 'select chrom, chromStart, chromEnd from hiSeqDepthTopPt1Pct' | grep -w "chr[0-9]*[XYM]*" > hg19.hiSeqDepthTopPt1Pct

# Hi Seq Depth 0.5%
mysql -N -B -h genome-mysql.cse.ucsc.edu -A -u genome -D hg19 -e 'select chrom, chromStart, chromEnd from hiSeqDepthTopPt5Pct' | grep -w "chr[0-9]*[XYM]*" > hg19.hiSeqDepthTopPt5Pct

# Hi Seq Depth 1%
mysql -N -B -h genome-mysql.cse.ucsc.edu -A -u genome -D hg19 -e 'select chrom, chromStart, chromEnd from hiSeqDepthTop1Pct' | grep -w "chr[0-9]*[XYM]*" > hg19.hiSeqDepthTop1Pct

# Hi Seq Depth 5%
mysql -N -B -h genome-mysql.cse.ucsc.edu -A -u genome -D hg19 -e 'select chrom, chromStart, chromEnd from hiSeqDepthTop5Pct' | grep -w "chr[0-9]*[XYM]*" > hg19.hiSeqDepthTop5Pct

# Hi Seq Depth 10%
mysql -N -B -h genome-mysql.cse.ucsc.edu -A -u genome -D hg19 -e 'select chrom, chromStart, chromEnd from hiSeqDepthTop10Pct' | grep -w "chr[0-9]*[XYM]*" > hg19.hiSeqDepthTop10Pct



# get UCSC BED file containing CGIs:
mysql -N -B -h genome-mysql.cse.ucsc.edu -A -u genome -D hg19 -e 'select chrom, chromStart, chromEnd, name, cpgNum from cpgIslandExt' | grep -w 'chr[0-9]*[XYM]*' > hg19.cpgIslandExt.bed



###
### Rat: rn4
###
# for use with the -g option with some BEDTools programs
mysql --user=genome --host=genome-mysql.cse.ucsc.edu -A -e "select chrom, size from rn4.chromInfo" | grep -v ^chrom |  grep -w "chr[0-9]*[XYM]*" > rn4.chromInfo


### RefSeq rn4
# get UCSC BED file containing refGene:
mysql -N -B -h genome-mysql.cse.ucsc.edu -A -u genome -D rn4 -e 'select chrom, txStart, txEnd, name, name2, strand from refGene' | grep -w "chr[0-9]*[XYM]*" > rn4.refGene

### miRNA rn4
# get UCSC BED file containing refGene:
mysql -N -B -h genome-mysql.cse.ucsc.edu -A -u genome -D rn4 -e 'select chrom, chromStart, chromEnd, name, score, strand from miRNA' > UCSC.rn4.miRNA.bed



###
### Mouse: mm9
###
# for use with the -g option with some BEDTools programs
mysql --user=genome --host=genome-mysql.cse.ucsc.edu -A -e "select chrom, size from mm9.chromInfo" | grep -v ^chrom |  grep -w "chr[0-9]*[XYM]*" > mm9.chromInfo


### RefSeq mm9
# get UCSC BED file containing refGene:
mysql -N -B -h genome-mysql.cse.ucsc.edu -A -u genome -D mm9 -e 'select chrom, txStart, txEnd, name, name2, strand from refGene' | grep -w "chr[0-9]*[XYM]*" > mm9.refGene


