# call peaks in various ways

# 

if ( PEAKS.POOLED ) {
	cat( "Pooling conditions. " )
	# Pool BED files for each condition
	for ( cond in unique(exp.spec$condition) ) {
		in.file <- paste( cond, ".mapped.bam", sep="" )
		if ( !file.exists( in.file ) ) {
			system(paste("cat ", cond, "-*.mapped.bam | sort -k1,1 -k2,2n > ", in.file, sep=""))
		}
	}
	cat( "Done.\n" )
}


if ( PEAKS.RELATIVE ) {
	
	if ( PEAKS.SINGLE ) {
		# I assume that for each replicate there are ONLY TWO conditions: 'ctrl' and some tretment condition
		for ( repl in unique(exp.spec$replicate) ) {
			ctrl <- paste0(CTRL_CONDITION, "-", repl, ".mapped.bam")
			
			#if ( repl %in% 3:6 ) next
			
			if ( !file.exists( ctrl ) ) {
				cat(ctrl, " missing.\n\n")
				next
			}
			
			for ( cond in unique(exp.spec$condition) ) {
				
				#if ( cond == "min10" ) next
				
				if ( cond != CTRL_CONDITION ) {
					treat <- paste(cond, "-", repl, ".mapped.bam", sep="")
					cat( CTRL_CONDITION, "and", cond, "replicate", repl, "\n" )

					ctrl.id <- paste(CTRL_CONDITION, cond, repl, sep="-")
					treat.id <- paste(cond, CTRL_CONDITION, repl, sep="-")
					out.name.c.t <- file.path(OUT.DIR, ctrl.id)
					out.name.t.c <- file.path(OUT.DIR, treat.id)
					
					
					if ( PEAKS.MACS ) {
						cat( "Calling single relative MACS peaks.\n" )
						system(paste("macs14 -t", treat, "-c", ctrl, "-f BAM", PEAKS.MACS.RELATIVE.PARAMETERS, "-n", out.name.t.c, sep=" "))
					#	system(paste("macs14 -t", ctrl, "-c", treat, "-f BAM", PEAKS.MACS.RELATIVE.PARAMETERS, "-n", out.name.c.t, sep=" "))
						
						
						pdf(paste0(out.name.t.c,"_peakStats.pdf"))
							peak.file.xls <- paste0( out.name.t.c, "_peaks.xls" )
							
							# plot peak length distribution
							peak.lengths <- as.numeric(system(paste0( "egrep -v \"#|^$\" ", peak.file.xls, " | sed '1d' | cut -f4" ), intern=TRUE))
							barplot(table(peak.lengths), main="Peak Length Distribution")
							
							# reads/peak distribution
							rds.per.peak <- as.numeric(system(paste0( "egrep -v \"#|^$\" ", peak.file.xls, " | sed '1d' | cut -f6" ), intern=TRUE))
							barplot(table(rds.per.peak), main="Reads/Peak Distribution")
						dev.off()
					}
					
					if ( PEAKS.QESEQ ) {
						
						
						if ( PEAKS.QESEQ.CALL ) {
							cat( "Calling single relative Qeseq peaks.\n" )
							# prepare Qeseq input files
							qeseq.ctrl <- file.path(OUT.DIR, paste0(CTRL_CONDITION, "-", repl, ".qeseq"))
							qeseq.treat <- file.path(OUT.DIR, paste(cond, "-", repl, ".qeseq",sep=""))
							
							system(paste("samtools view", ctrl, "| cut -f 3,4,2 | awk 'BEGIN{OFS=\"\\t\"}{if ($3==0) {print $1,$2,\"+\"} else {print $1,$2,\"-\"}}' >", qeseq.ctrl, sep=" ")) 
							system(paste("samtools view", treat, "| cut -f 3,4,2 | awk 'BEGIN{OFS=\"\\t\"}{if ($3==0) {print $1,$2,\"+\"} else {print $1,$2,\"-\"}}' >", qeseq.treat, sep=" "))
							
							# and call relative peaks
							if ( !exists("PEAKS.QESEQ.P.CUTOFF") ) {
								PEAKS.QESEQ.P.CUTOFF <- 0.05  # this is also the default qeseq cutoff
							}
							cat(paste0(qeseq.treat, " vs. ", qeseq.ctrl), "\n")
							system(paste(file.path( scriptDir, "qeseq"), "-p", PEAKS.QESEQ.P.CUTOFF, "-t 4 -s", FRAGMENT.LENGTH, qeseq.treat, qeseq.ctrl, "-o", out.name.t.c, sep=" "))
							cat(paste0(qeseq.ctrl, " vs. ", qeseq.treat), "\n")
							system(paste(file.path( scriptDir, "qeseq"), "-p", PEAKS.QESEQ.P.CUTOFF, "-t 4 -s", FRAGMENT.LENGTH, qeseq.ctrl, qeseq.treat, "-o", out.name.c.t, sep=" "))
							
							# get chr, start, end, p-value for DiffBind raw input
							# for f in *.txt; do cut -f1-3,8 $f | sed '1d' > $(echo $f | sed -e 's/\(.*\)\.txt/\1.qeseq.raw/'); done;
							
							unlink(qeseq.ctrl)
							unlink(qeseq.treat)
						}
												
						
						if ( PEAKS.QESEQ.ANNOTATE ) {
							
							pdf(paste0(out.name.t.c,"_peakStats.pdf"))
								cat("Plotting peak stats.\n")
								
								peak.file <- paste0( out.name.t.c, ".txt" )
								
								# plot peak length distribution
								peak.lengths <- as.numeric(system(paste0( "sed \"1d\" ", peak.file, " | awk '{print $3-$2}'" ), intern=TRUE))
								hist(peak.lengths, color="green", breaks=100, main="Peak Length Distribution")
								
								# positive reads/peak distribution
								rds.per.peak <- table(as.numeric(system(paste0( "sed \"1d\" ", peak.file, " | cut -f5" ), intern=TRUE)))
								barplot(table(rds.per.peak), color="red", main="Positive Reads/Peak Distribution")
	
								# negative reads/peak distribution
								rds.per.peak <- table(as.numeric(system(paste0( "sed \"1d\" ", peak.file, " | cut -f6" ), intern=TRUE)))
								barplot(rds.per.peak, color="blue", main="Negative Reads/Peak Distribution")
								
								
								peak.file <- paste0( out.name.c.t, ".txt" )
								
								# plot peak length distribution
								peak.lengths <- as.numeric(system(paste0( "sed \"1d\" ", peak.file, " | awk '{print $3-$2}'" ), intern=TRUE))
								hist(peak.lengths, color="green", breaks=100, main="Peak Length Distribution")
								
								# positive reads/peak distribution
								rds.per.peak <- table(as.numeric(system(paste0( "sed \"1d\" ", peak.file, " | cut -f5" ), intern=TRUE)))
								barplot(table(rds.per.peak), color="red", main="Positive Reads/Peak Distribution")
	
								# negative reads/peak distribution
								rds.per.peak <- table(as.numeric(system(paste0( "sed \"1d\" ", peak.file, " | cut -f6" ), intern=TRUE)))
								barplot(rds.per.peak, color="blue", main="Negative Reads/Peak Distribution")
							dev.off()

							
							source(paste(file.path( scriptDir, "annotateRegions.r" )))
							
							cat( "Annotating relative single Qeseq peaks.\n" )
							peak.file.suffix <- ".txt"
							ctrl.peak <- paste0( out.name.c.t, peak.file.suffix )
							treat.peak <- paste0( out.name.t.c, peak.file.suffix )
							
							ctrl.peak <- read.table(ctrl.peak,sep="\t",stringsAsFactors=FALSE,header=FALSE)
							treat.peak <- read.table(treat.peak,sep="\t",stringsAsFactors=FALSE,header=FALSE)
							names(ctrl.peak) <- names(treat.peak) <- c("chr","start","end","mean.pos","n.pos.tags","n.neg.tags","avg.enrich","p.val")
							
							if(debug) { print(dim(ctrl.peak)); print(head(ctrl.peak)) }
							file.name <- paste0("qeseq.", ctrl.id)
							annotate.regions( ctrl.peak, file.name, meta.data=5:8, annot.cgi=FALSE )
							
							if(debug) {print(dim(treat.peak)); print(head(treat.peak))}
							file.name <- paste0("qeseq.", treat.id)
							annotate.regions( treat.peak, file.name, meta.data=5:8, annot.cgi=FALSE )					
						}
						
					}
					
					if ( PEAKS.BALM ) {
						cat( "Calling single relative BALM peaks.\n" )
						system(paste(file.path( scriptDir, "BALM1.0.1_linux64_multithread" ), "-g hg19 -M CpG_hg19_coordinates.txt -h", OUT.DIR, "-b", treat, "-cb", ctrl, sep=" "))
						system(paste(file.path( scriptDir, "BALM1.0.1_linux64_multithread" ), "-g hg19 -M CpG_hg19_coordinates.txt -h", OUT.DIR, "-b", ctrl, "-cb", treat, sep=" "))
					}
				}
			}	
		}
	}
	
	
	# TBD TBD convert sot that it works with BAM
	if ( PEAKS.POOLED ) {
		if ( PEAKS.MACS ) {
			cat( "Calling pooled relative MACS peaks.\n" )
			for ( cond in unique(exp.spec$condition) ) {
				in.file <- paste(cond,"_aligned.bed", sep="")
				
				cat("Calling MACS paired peaks\n")
				if ( cond != CTRL_CONDITION ) {
					out.name.t.c <- file.path(OUT.DIR, paste0(cond, "-", CTRL_CONDITION))
					out.name.c.t <- file.path(OUT.DIR, paste0(CTRL_CONDITION, "-", cond))
					system(paste0("macs14 -t ", in.file, " -c ", CTRL_CONDITION, "_aligned.bed", " -f BED ", PEAKS.MACS.RELATIVE.PARAMETERS, " -n ", out.name.t.c))
					system(paste0("macs14 -t ", CTRL_CONDITION, "_aligned.bed -c", in.file, "-f BED", PEAKS.MACS.RELATIVE.PARAMETERS, "-n", out.name.c.t))
				}
			
			}
		}
			
		if ( PEAKS.QESEQ ) {
		
			for ( cond in unique(exp.spec$condition) ) {
				if ( cond != CTRL_CONDITION ) {
					
					treat.id <- paste0(cond, "-", CTRL_CONDITION)
					ctrl.id <- paste0(CTRL_CONDITION, "-", cond)
					out.name.t.c <- file.path(OUT.DIR, treat.id)
					out.name.c.t <- file.path(OUT.DIR, ctrl.id)
				
					if ( PEAKS.QESEQ.CALL ) {
						qeseq.ctrl <- paste0(CTRL_CONDITION,".qeseq")
						if ( !file.exists(qeseq.ctrl) ) {
							system(paste0("cut -f 1,2,6 ", CTRL_CONDITION, "_aligned.bed > ", qeseq.ctrl)) 
						}
						
						treat <- paste(cond, "_aligned.bed", sep="")
						qeseq.treat <- paste(cond,".qeseq",sep="")
						system(paste("cut -f 1,2,6", treat, ">", qeseq.treat, sep=" ")) 

						
						cat( "Calling pooled relative Qeseq peaks.\n" )
						cat("1. treat vs ctrl\n")
						system(paste(file.path( scriptDir, "qeseq"), "-t 4 -s", FRAGMENT.LENGTH, qeseq.treat, qeseq.ctrl, "-o", out.name.t.c, sep=" "))
						cat("2. ctrl vs treat\n")
						system(paste(file.path( scriptDir, "qeseq"), "-t 4 -s", FRAGMENT.LENGTH, qeseq.ctrl, qeseq.treat, "-o", out.name.c.t, sep=" "))
						
						# get chr, start, end, p-value for DiffBind raw input
						# for f in *.txt; do cut -f1-3,8 $f | sed '1d' > $(echo $f | sed -e 's/\(.*\)\.txt/\1.qeseq.raw/'); done;
						
						unlink(qeseq.treat)
					}
				
		
					if ( PEAKS.QESEQ.ANNOTATE ) {
						source(paste(file.path( scriptDir, "annotateRegions.r" )))
									
						cat( "Annotating relative pooled Qeseq peaks.\n" )
						peak.file.suffix <- ".txt"
						ctrl.peak <- paste0( out.name.c.t, peak.file.suffix )
						treat.peak <- paste0( out.name.t.c, peak.file.suffix )
						
						ctrl.peak <- read.table(ctrl.peak,sep="\t",stringsAsFactors=FALSE,header=FALSE)
						treat.peak <- read.table(treat.peak,sep="\t",stringsAsFactors=FALSE,header=FALSE)
						names(ctrl.peak) <- names(treat.peak) <- c("chr","start","end","mean.pos","n.pos.tags","n.neg.tags","avg.enrich","p.val")
						
						if(debug) { print(dim(ctrl.peak)); print(head(ctrl.peak)) }
						file.name <- paste0("qeseq.", ctrl.id)
						annotate.regions( ctrl.peak, file.name, meta.data=5:8, annot.cgi=FALSE )
						
						if(debug) {print(dim(treat.peak)); print(head(treat.peak))}
						file.name <- paste0("qeseq.", treat.id)
						annotate.regions( treat.peak, file.name, meta.data=5:8, annot.cgi=FALSE )
					}
				} # if != CTRL_CONDITION
			} # for cond
			if ( file.exists( qeseq.ctrl ) ) unlink(qeseq.ctrl)
			
		} # QESEQ
	} # POOLED
} # RELATIVE




if ( PEAKS.ABSOLUTE ) {
	
	if ( PEAKS.SINGLE ) {
		files <- list.files(pattern="^.*_aligned\\.bed")
		
		# Call peaks with MACS
		if ( PEAKS.MACS ) {
			cat( "Calling single absolute MACS peaks.\n" )
			for ( f in files ) {
				file.id <- gsub("^(.*)_aligned\\.bed", "\\1", f)
				out.name <- file.path(OUT.DIR, file.id)
				system(paste("macs14 -t", f, "-f BED", PEAKS.MACS.ABSOLUTE.PARAMETERS, "-n", out.name, sep=" "))

				if ( PEAKS.ANNOTATE.MACS ) {
					source(paste(file.path( scriptDir, "annotateRegions.r" )))
					
					cat( "Annotating relative single MACS peaks.\n" )
					peak.file.name <- paste0( out.name, "_peaks.bed" )
					
					peak.bed <- read.table(peak.file.name,sep="\t",stringsAsFactors=FALSE,header=FALSE)
					names(peak.bed) <- c("chr","start","end","peak.name","mlog10.P.value")
					
					if(debug) { print(dim(peak.bed)) }
					file.name <- paste0("macs.", file.id)
					annotate.regions( peak.bed, file.name, meta.data=5, annot.cgi=FALSE )			
				}
				
				
				pdf(paste0(out.name,"_peakStats.pdf"))
					peak.file.xls <- paste0( out.name, "_peaks.xls" )
					
					# plot peak length distribution
					peak.lengths <- as.numeric(system(paste0( "egrep -v \"#|^$\" ", peak.file.xls, " | sed '1d' | cut -f4" ), intern=TRUE))
					barplot(table(peak.lengths), main="Peak Length Distribution")
					
					# reads/peak distribution
					rds.per.peak <- as.numeric(system(paste0( "egrep -v \"#|^$\" ", peak.file.xls, " | sed '1d' | cut -f6" ), intern=TRUE))
					barplot(table(rds.per.peak), main="Reads/Peak Distribution")
				dev.off()
			}
		}
		
		# Call peaks with BALM
		if ( PEAKS.BALM ) {
			cat( "Calling single absolute BALM peaks.\n" )
			for ( f in files ) {
				system(paste(file.path( scriptDir, "BALM1.0.1_linux64_multithread" ), "-g hg19 -M CpG_hg19_coordinates.txt -h", OUT.DIR, "-b", f, sep=" "))
			}
		}
	}
	
	
	if ( PEAKS.POOLED ) {
		# Call peaks with MACS
		if ( PEAKS.MACS ) {
			for ( cond in unique(exp.spec$condition) ) {
				cat("Calling pooled absolute MACS peaks\n")
				in.file <- paste(cond,"_aligned.bed", sep="")
				out.name <- file.path(OUT.DIR, cond)
				system(paste("macs14 -t", in.file, "-f BED", PEAKS.MACS.ABSOLUTE.PARAMETERS, "-n", out.name, sep=" "))
			}
		}	
	}
}


