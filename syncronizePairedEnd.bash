#!/bin/bash
# script name: ./syncronizePairedEnd.bash file1.fastq file2.fastq cond_name
# run the script for each pair of filtered fastq files
# Discards unpaired reads.

E_WRONG_ARGS=85
Number_of_expected_args=3

if [ $# -ne $Number_of_expected_args ]then  echo "Usage: `basename $0` file1.fastq file2.fastq cond_name"  # `basename $0` is the script's filename.  exit $E_WRONG_ARGSfi

# get root names
p1=`basename $1 .fastq`
p2=`basename $2 .fastq`
cond_name=$3
scriptDir=`dirname $0`

# convert to tabular form (assume each fastq file consists of 4 line entries wo. empty lines), concat the files, and sort based on ID
cat $1 | perl ${scriptDir}/mergelines.pl > ${p1}_merged
cat $2 | perl ${scriptDir}/mergelines.pl > ${p2}_merged
cat ${p1}_merged ${p2}_merged | sort --stable -k1,1 > ${cond_name}_merged_cat_sorted  # --stable preservs the original order

# pick out the matched pairs based on the sequence IDs: two consecutive identical IDs.
awk -v out1=${cond_name}_P1_sync -v out2=${cond_name}_P2_sync '
#BEGIN{prevID=$1; prevLine=$0}  # I think running this line is wrong, as then the first line will always go into file 1
{
	if ($1==prevID) {  # first time prevID is empty so this test fails
		print prevLine > out1;
		print $0 > out2
	} else {  # if first line is from file 2 then the second cannot b from file 1 bc. of the stable sort
		prevID=$1;
		prevLine=$0
	}
}' ${cond_name}_merged_cat_sorted

# tabular to fastq
cat ${cond_name}_P1_sync | perl ${scriptDir}/splitlines.pl > ${cond_name}_P1_sync.fastq
cat ${cond_name}_P2_sync | perl ${scriptDir}/splitlines.pl > ${cond_name}_P2_sync.fastq

# clean up
rm ${p1}_merged
rm ${p2}_merged
rm ${cond_name}_merged_cat_sorted
rm ${cond_name}_P1_sync
rm ${cond_name}_P2_sync