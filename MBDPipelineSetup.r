##################################################
############# MBD PIPELINE SETUP #################
##################################################

PROJECT.ID = "AE.H"

ORGANISM = "mm"  # "hs", "mm", "rn"
GENOME.BUILD = "mm9"  # "hg19", "mm9", "rn4"


FRAGMENT.LENGTH = 500  # set this to the avg fragment.length
READ.LENGTH = 50  #

CTRL_CONDITION = "basal"  # the control condition name; used e.g. for looking for DMRs: treatment samples r cmp against ctrl

FASTQ.DIR.PATH = "../Project_Acute_exercise/H/"  # path to folder with fastq files and the expSpec file
PROJECT.DIR.NAME = "hmeth00"  # will be created as a subdirectory to FASTQ.DIR

PRE.FILTERED = F  # have the fastq files been pre filtered?
PRE.ALIGNED = F  # are the files already aligned? If TRUE, the bam file names r given in the expSpec file


##################################
####### PIPELINE ACTIONS #########

###
### Run FastQC on all samples
FASTQC.1 = F



###
### Clip and filter samples, and pair up paired end samples.
PREPROC = F	# Filter: clip overrep + quality trim of the user specified sequences
	PREPROC.PLOT.ONLY = F
	
	PREPROC.AUTO.OVERREP = T  # automatically find overrep sequences, or user supplied
	PREPROC.OVERREP.CLIP = T
	PREPROC.CUSTOM.ADAPTOR = ""  # e.g. u could clip the adaptor seq specified in the adaptor spec. sheet
	PREPROC.TRIM3 = T
	PREPROC.TRIM5 = F
	PREPROC.QUALITY.FILTER = T

	
	# This and AUTO.OVERREP are mutually exclusive actions
	# U only run an itterated preprocessing step if u're not satisfied with a previous manual or auto preprocessing run.
	PREPROC.ITTERATED = F
	



###
### Run FastQC on the cleaned reads.
FASTQC.2 = F



###
### Align reads using bowtie
ALIGN = F
	ALIGN.RMDUP = F
	BOWTIE.PARAMETERS.SE = "-m 1 -n 2 --best --strata"
	BOWTIE.PARAMETERS.PE = "-m 1 -n 2 -I 50 -X 800 --fr --best"


### ----------------------------------------------------------------------


###
### Generate various stats and plots
STATS = F		# Print more stats
	STATS.GATHER.DATA = F  # calculate all stats and bedgraph stuff again, or just load the exp.spec file and plot?
	
	#STATS.CONTIGS.REMOVE.DUPLICATES = F
	
	STATS.ANNOT.OVERLAP.PCT = 0.03  # 500 * 0.05 = 25 nt overlap; 500 * 0.03 = 15
	
	STATS.UCSC.TRACKS = T
	STATS.BEDGRAPH = T
	
	STATS.ENS.GENE.STATS = T
	
	#STATS. ... SHARED CONTIGS
	#STATS. ... SHARED READS



###
### This applies to both MEDIPS and DMR.CUSTOM called DMRs
GET.COMMON.DMR.FEATURES.ONLY = F  # after having annotated the samples, setting this will return the common feature DMRs per condition



###	
### BioC MEDIPS analysis:
# Plot Saturation and Coverage plots, calculate CpG enrichment, find DMRs and annotate them.
MEDIPS = T
        MEDIPS.OUT.SUB.DIR = "RefSeq.collapsed.promoter.1000u1000d.win1.remDup.mrs1"  # Store MEDIPS output in a subdirectory to the main MEDIPS/ directory.

        MEDIPS.SKIP.CHR = c("")
        
        MEDIPS.ROI.ANALYSIS.FILE = "mm9.refGene.collapsed.bed" #"ensGene.bed"   # If this is set then perform MEDIPS analysis on the ROIs in the specified ROI-file only instead of the entire genome.
                MEDIPS.PROMOTER.ANALYSIS = T
                MEDIPS.PROMOTER.EXTEND.UP = 1000
                MEDIPS.PROMOTER.EXTEND.DOWN = 1000
                MEDIPS.ROI.ANALYSIS.BINS = 1 # ceiling((MEDIPS.PROMOTER.EXTEND.UP + MEDIPS.PROMOTER.EXTEND.DOWN) / 25)

        MEDIPS.REM.DUPLICATES = T
        MEDIPS.WIN.SIZE = 0

        MEDIPS.WIG = F
	MEDIPS.WIG.ONLY = F
        
        MEDIPS.SAT = F  # run only Saturation Analysis
        MEDIPS.COV = F  # run only Coverage Analysis
                        
        # Run DMR and annotation or just the quality stats above?
        MEDIPS.DMR = T  #  Find DMRs and annotate
                # DMR parameters:
        MEDIPS.CHROMOSOMEWISE = T
                MEDIPS.DMR.MINROWSUM = 1
                MEDIPS.RATIO = NULL
                MEDIPS.BG.COUNTS = NULL

                MEDIPS.SELECT.P.VAL = F
		MEDIPS.SELECT.ALL = T

	
###
### TBD Under construction
REPITOOLS = F



###
### If u run DiffBind or DMR.COUNT, u must call peaks fist.
PEAKS = F
	# How do you wanna call peaks?
	PEAKS.ABSOLUTE = T
	PEAKS.RELATIVE = F
		
	PEAKS.SINGLE = T
	PEAKS.POOLED = F
	

	# Which peak callers?
	PEAKS.MACS = T
		PEAKS.MACS.ABSOLUTE.PARAMETERS = "-g 2267239048 --nomodel --nolambda --shiftsize=100 -p 1e-5 -s 50"
		PEAKS.MACS.RELATIVE.PARAMETERS = "-g 2.8e9 --nomodel --shiftsize=200 -p 1e-5 -s 50"  # when doing relative peak calling, we can use a local lambda
		PEAKS.ANNOTATE.MACS = F
	PEAKS.QESEQ = F  # NB: only calls relative peaks (default p-value = ... )
		# Annotate relative Qeseq peaks?
		# Since Qeseq is themost fit peak-caller, this action is only implemeted for Qeseq.
		PEAKS.ANNOTATE.QESEQ = T
	PEAKS.BALM = F	# TBD ignore this for now



### 	
### Two ways of running DiffBind.
DIFFBIND = F
	DIFFBIND.MIN.OVERLAPS = 1
	DIFFBIND.MIN.RD.PEAK = 1
	DIFFBIND.DMR = T
	
	DIFFBIND.PEAK.STATS = T
		DIFFBIND.ANNOTATE.COMMON.PEAKS = T
		DIFFBIND.ANNOTATE.ALL.CONSENSUS.PEAKS = T
	
	DIFFBIND.NORM.FILTER.MX = F

	DIFFBIND.RELATIVE = F

	## TBD: Right now only MACS alone is implemented - 
	DIFFBIND.PEAKS = "MACS"  # "MACS", "QESEQ", (or TBD "BOTH", or "BALM") NB: "QESEQ" only for relative peaks!!!
	
	DIFFBIND.CUSTOM = F	 # run DiffBind on a subset of the replicates?
		DIFFBIND.REPLICATES = 1:5



###			
### Custom approache to finding DMRs (non-statistical approach).
# Must call MACS peaks first
DMR.COUNT = F
	# Pooled or each sample individually?
	DMR.COUNT.POOLED = F
	DMR.COUNT.SINGLE = T
	
	# Hw r the peaks called?
	DMR.COUNT.RELATIVE = F
	
	# Which peak-caller?
	DMR.COUNT.PEAKS = "MACS"  # "MACS", "QESEQ"; NB: "QESEQ" only for relative peaks!!! (It's getting messy)
	
	# Settings
	DMR.COUNT.NORMALIZE = TRUE
	DMR.COUNT.MIN.RD.PEAK = 5
	DMR.COUNT.MIN.FC = 2


###
### For each YFG, get the promoter region, count how many reads map to the region from each sample (i.e. build a YFG promoter expression matrix).
DMR.REGIONS = F
        DMR.REGIONS.YFG.PATH = "paperGenes.fig1c.yfg"  # Path to a tab separated file with Your Favorite Gene names. The gene names must be in column 1. Other columns are ignored.
        DMR.REGIONS.UP = 20000  # How many nts upstream of a TSS should we look at?
        DMR.REGIONS.DOWN = 5000  # How many nts upstream of a TSS should we look at?
        DMR.REGIONS.EDGER = T  # 
        	DE.EDGER.FDR = 0.05
			DE.EDGER.MIN.EXPR = 2  # keep only those genes that have more than this # of reads in at least one sample
			DE.EDGER.NORM = F  # perform TMM normalization?
			DE.EDGER.TAGWISE.DISPERSION = T  # Estimate tagwise dispersion? (more accurate but it increases the FDR and you get fewer DE genes)
			DE.EDGER.GLM = T  # F = Classic edgeR; T = GLM
				DE.EDGER.PAIRED = T  # Are the samples paired?

				DE.EDGER.CONTRASTS.ALL = T  # try all possible contrasts
				
				DE.EDGER.CONTRASTS.CUSTOM = F  # specify the contrasts u're interested in in DE.EDGER.CONRTASTS bellow
				DE.EDGER.CONTRASTS =  # specify the contrasts you would like to run
					c()

				# Example:
				#	DE.EDGER.CONTRASTS =  # specify the contrasts you would like to run
				#		c(
				#			"Drug.1h-Drug.0h",
				#			"Drug.2h-Drug.0h",
				#			"Placebo.1h-Placebo.0h",
				#			"Placebo.2h-Placebo.0h",
				#			"Drug.0h-Placebo.0h",
				#			"(Drug.1h-Drug.0h)-(Placebo.1h-Placebo.0h)",
				#			"(Drug.2h-Drug.0h)-(Placebo.2h-Placebo.0h)",
				#		)


	

### ----------------------------------------------------------------------

###
### Check if any gene in a user specified gene list is among any of nearest genes to the DMRs we've found.
TEST.YFG = F
	TEST.YFG.MEDIPS = T
	TEST.YFG.DMR.COUNT = T
	TEST.YFG.DIFFBIND = T
	TEST.YFG.PEAKS.QESEQ = T  # Only for Qeseq relative single peaks right now.
	
	TEST.YFG.PATH = "DE.obesePreGBPvsNormalWomen"  # Path to a tab separated file with Your Favorite Gene names. The gene names must be in column 1. Other columns are ignored.




## Mappable Genome Size for MACS' '-g' parameter:
# This depends on the organism and the read length, so set this to fit your experiment.
#MGS.HS.30nt.se = "2.5e9" # mappable with 30 nt single-end reads
#MGS.HS.25nt.pe = "2.88e9" # mappable with 25 nt paired-end reads (guess: 3.2 * .9)
#MGS.RN = "2.2e9"  # TBD
#MGS.MM.50 = "2267239048"   https://groups.google.com/forum/#!msg/macs-announcement/9DeYzN3vFWA/TgGGKUfB-PEJ


### ------------
### Some Ideas:

###
### Given a regions file, e.g., all Ensembl genes extended 5kb up- and down-stream
# Mask out all sample contigs that do not overap any of these regions.
# Load remaining contigs as into DiffBind as raw peak files together with the reads.
# Comment: Bad idea to take contigs directly; need to filter fake peaks and duplicates first... and perhaps CpG normalize.
# DMR.REGION = F

###
###
# DMR.CONTIG = F

###
### Given a list of gene names, get nearest contigs and related read info.

