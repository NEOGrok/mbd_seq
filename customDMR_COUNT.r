#####################################################################################################################
#
# DESCRIPTION
#
# There must always be 2 peakfiles from 2 conditions. These can be any of the following
#	- pooled:
#		-- pool replicates and call peaks on each
#		-- pool replicates and call pairwise relative peaks both ways and feed those two peak files and reads in here
#	- non-pooled:
#		-- call peaks for each condition and replicate individaully and analyse ctrl vs treat for each replicate
#		-- call pairwise relative peaks both ways for each replicate
#
# If DMR.COUNT.NORMALIZE == TRUE then the counts within each peak are normalized by the number of aligned reads.
#
#####################################################################################################################



#library( "gplots" )
#library( "RColorBrewer" )

library(DiffBind)

## Load annotation function
source(paste(file.path( scriptDir, "annotateRegions.r" )))

## DMR count function
dmr.count <- function( csv.file, plotname ) {
	# load DiffBind consensus peaks and counts			
	# hw many peaks in each sample, consensus peaks, and peaks shared by at least (minOverlap) 1 samples (i.e. keep all consensus peaks)
	mbdSeq <- dba(sampleSheet=csv.file, minOverlap=1, bRemoveM=TRUE, bRemoveRandom=TRUE, bCorPlot=FALSE)
	
	
	plotname.complete <- paste( plotname, DMR.COUNT.MIN.RD.PEAK, DMR.COUNT.MIN.FC, ifelse(DMR.COUNT.NORMALIZE, "NORM", "RAW"), sep="_" )
	pdf(file.path(OUT.DIR, paste("dmr.count.", plotname.complete, ".pdf", sep="")))
		#cons.peaks <- mbdSeq$vectors
		#colnames( cons.peaks ) <- c(colnames( cons.peaks )[1:3], paste("p-", colnames( cons.peaks )[4:5], sep=""))
		#cons.peaks$CHR <- gsub( "(.*)", "chr\\1", cons.peaks$CHR )
		## 
		# minOverlap = as above
		# score = affinity score to use; we use the raw read counts (after removing duplicates (see bRemoveDuplicates parameter))
		# insertLength = extend each rd this many bases
		# bRemoveDuplicates = remove reads that map to the same genomic position (I think: chr + start)
		# maxFilter = select consensus peaks (intervals) with at least this many reads in at least one sample (e.g. 5)
		# bCorPlot = 
		mbdSeq <- dba.count( mbdSeq, minOverlap=1, score=DBA_SCORE_READS, insertLength=EXTEND.LENGTH, bRemoveDuplicates=TRUE, maxFilter=DMR.COUNT.MIN.RD.PEAK, bCorPlot=FALSE )	
			
	
		# overlap rates: hw many consensus peaks, peaks shared btw at least 2 samples, at least 3, ...
		olap.rate = dba.overlap(mbdSeq,mode=DBA_OLAP_RATE)
		olap.rate
		plot(olap.rate,type='b',ylab='# peaks', xlab='Overlap at least this many peaksets')

		plot( mbdSeq )  # occupancy clustering based on correlation among the samples


		peak.counts <- mbdSeq$vectors
		colnames(peak.counts) <- tolower(colnames(peak.counts))
		cat( nrow( peak.counts ), "consensus peaks for", colnames(peak.counts)[4], "and", colnames(peak.counts)[5], "\n" )
		if (debug) print(head( peak.counts))


		if ( DMR.COUNT.NORMALIZE ) {	# normalize to reads per million		
			n.aligned.1 <- as.numeric(system(paste("wc -l <", mbdSeq$samples$bamReads[1]),intern=TRUE))
			n.aligned.2 <- as.numeric(system(paste("wc -l <", mbdSeq$samples$bamReads[2]),intern=TRUE))
			peak.counts[,4:5] <- sweep( peak.counts[,4:5], 2, c(n.aligned.1,n.aligned.2), "/") * 1000000
		}

		if (debug) print(head( peak.counts))
		
		# scatter and MA-plots
		plot( peak.counts[ ,4],  peak.counts[ ,5] )
		M <- log2(peak.counts[ ,5]+1) - log2(peak.counts[ ,4]+1)  # cond over ctrl
		A <- .5*log2((peak.counts[ ,4]+1)*(peak.counts[ ,5]+1))
		plot( A, M )
		
		# highlight the DMRs
		A.fc <- A[abs(M) >= log2(DMR.COUNT.MIN.FC)]
		M.fc <- M[abs(M) >= log2(DMR.COUNT.MIN.FC)]
		points( A.fc, M.fc, col="red" )
	dev.off()
	
	peak.counts$M <- M
	if (debug) print(head(peak.counts))
	# select peaks where FC >=  (e.g. 2)
	peaks.2.fc.1 <- peak.counts[M >= log2(DMR.COUNT.MIN.FC), ]  # higer in cond over ctrl
	peaks.1.fc.2 <- peak.counts[M <= -log2(DMR.COUNT.MIN.FC), ]  # higher in ctrl over cond
	if (debug) print(head(peaks.2.fc.1))
	if (debug) print(head(peaks.1.fc.2))
	
	cat(nrow( peaks.1.fc.2 ), "peaks where", colnames(peaks.1.fc.2)[4], ">=", DMR.COUNT.MIN.FC, "*", colnames(peaks.1.fc.2)[5], "\n")
	cat(nrow( peaks.2.fc.1 ), "peaks where", colnames(peaks.2.fc.1)[5], ">=", DMR.COUNT.MIN.FC, "*", colnames(peaks.2.fc.1)[4], "\n")

	
	###
	### Annotation:
	if (nrow( peaks.1.fc.2 ) > 0) {
		annotname <- paste( colnames(peaks.1.fc.2)[4], DMR.COUNT.MIN.RD.PEAK, DMR.COUNT.MIN.FC, ifelse(DMR.COUNT.NORMALIZE, "NORM", "RAW"), sep="_" )
		annotate.regions( peaks.1.fc.2, annotname, meta.data=c(4,5,6) )
	}
	if (nrow( peaks.2.fc.1 ) > 0) {
		annotname <- paste( colnames(peaks.2.fc.1)[5], DMR.COUNT.MIN.RD.PEAK, DMR.COUNT.MIN.FC, ifelse(DMR.COUNT.NORMALIZE, "NORM", "RAW"), sep="_" )
		annotate.regions( peaks.2.fc.1, annotname, meta.data=c(4,5,6) )	
	}
}



#########
## Main
#########

if ( DMR.COUNT.PEAKS == "MACS" ) {
	peak.file.suffix <- "_peaks.xls"
	peak.caller <- "macs"
} else if ( DMR.COUNT.PEAKS == "QESEQ" ) {
	peak.file.suffix <- ".qeseq.raw"
	peak.caller <- "raw"
} else {
	cat("Unknown peak caller", DMR.COUNT.PEAKS)
	q()
}	


if ( DMR.COUNT.POOLED ) {
	ctrl <- "ctrl_aligned.bed"

	if ( !file.exists( ctrl ) ) {
		cat(ctrl, " missing.\n\n")
		q()
	}
		
	for ( cond in unique(exp.spec$condition) ) {
		cond <- tolower(cond)
		
		if ( cond != "ctrl" ) {
			treat <- paste(cond, "_aligned.bed", sep="")
			
			# Generate DiffBind PAIRED experiment spec csv-file with peaks from MACS (must b run b4 this step)	
			cat("Generating DiffBind csv-file for: ", ctrl, "&", treat, "\n")					
			
			if ( DMR.COUNT.RELATIVE ) {
				ctrl.peak <- file.path( PPL.ACTIONS$PEAKS, paste("ctrl-", cond, peak.file.suffix, sep=""))
				treat.peak <- file.path( PPL.ACTIONS$PEAKS, paste(cond, "-ctrl", peak.file.suffix, sep="" ))
				ctrl.id <- paste("ctrl", cond, sep="-")
				treat.id <- paste(cond, "ctrl", sep="-")
				csv.file <- file.path(OUT.DIR, paste("Pooled.Relative.", DMR.COUNT.PEAKS, ".DMRs_", "ctrl", "-", cond, ".csv", sep=""))
			} else {
				ctrl.peak <- file.path( PPL.ACTIONS$PEAKS, paste("ctrl", peak.file.suffix, sep=""))
				treat.peak <- file.path( PPL.ACTIONS$PEAKS, paste( cond, peak.file.suffix, sep="" ))
				ctrl.id <- "ctrl"
				treat.id <- cond
				csv.file <- file.path(OUT.DIR, paste("Pooled.", DMR.COUNT.PEAKS, ".DMRs_", "ctrl", "-", cond, ".csv", sep=""))
			}
						
			
			# Skip if any of the 2 conditions have 0 peaks
			peak.files <- c(ctrl.peak, treat.peak)
			for ( i in 1:length(peak.files) ) {
				n.peaks <- as.numeric(system(paste("wc -l <", peak.files[i], sep=" "), intern=TRUE))
				
				if (n.peaks == 0) {
					cat("File ", peak.files[i], "contains 0 peaks. Cannot run paired DMR analysis\n")
					q()
				}
			}
			
			diffBind.csv <- data.frame(	
				SampleID = c(ctrl.id, treat.id),
				Tissue = exp.spec$tissue[1],
				Factor = "meth",
				Condition = c("ctrl", cond),
				Replicate = 1,
				bamReads = c(ctrl, treat),
				Peaks = c(ctrl.peak, treat.peak),
				PeakCaller = peak.caller )
							
			write.csv( diffBind.csv, file=csv.file, quote=FALSE, row.names=FALSE )
			
			plotname <- paste(cond, "ctrl", sep="-")
			dmr.count( csv.file, plotname )
		}
	}
}


if ( DMR.COUNT.SINGLE ) {
for ( repl in unique(exp.spec$replicate) ) {  # I assume that for each replicate there are ONLY TWO conditions: 'ctrl' and some tretmet condition
	ctrl <- paste("ctrl-", repl, "_aligned.bed", sep="")

	if ( !file.exists( ctrl ) ) {
		cat(ctrl, " missing.\n\n")
		next
	}
		
	for ( cond in unique(exp.spec$condition) ) {
		cond <- tolower(cond)
		
		if ( cond != "ctrl" ) {
			treat <- paste(cond, "-", repl, "_aligned.bed", sep="")
			
			if ( !file.exists( treat ) ) {
				cat(treat, " missing.\n\n")
				next
			}
			
			# Generate DiffBind PAIRED experiment spec csv-file with peaks from MACS (must b run b4 this step)	
			cat("Generating DiffBind csv-file for: ", ctrl, "&", treat, "\n")					
			
			if ( DMR.COUNT.RELATIVE ) {
				ctrl.peak <- file.path( PPL.ACTIONS$PEAKS, paste( "ctrl-", cond, "-", repl, peak.file.suffix, sep="" ))
				treat.peak <- file.path( PPL.ACTIONS$PEAKS, paste( cond,"-ctrl-", repl, peak.file.suffix, sep="" ))
				ctrl.id <- paste("ctrl", cond, repl, sep="-")
				treat.id <- paste(cond, "ctrl", repl, sep="-")
			
				csv.file <- file.path(OUT.DIR, paste("Single.Relative.", DMR.COUNT.PEAKS, ".DMRs_", "ctrl", "-", cond, "-", repl, ".csv", sep=""))
			} else {
				ctrl.peak <- file.path( PPL.ACTIONS$PEAKS, paste( "ctrl-", repl, peak.file.suffix, sep="" ))
				treat.peak <- file.path( PPL.ACTIONS$PEAKS, paste( cond,"-", repl, peak.file.suffix, sep="" ))
				ctrl.id <- paste("ctrl", repl, sep="-")
				treat.id <- paste(cond, repl, sep="-")
				
				csv.file <- file.path(OUT.DIR, paste("Single.", DMR.COUNT.PEAKS, ".DMRs_", "ctrl", "-", cond, "-", repl, ".csv", sep=""))
			}
							
			# Skip if any of the 2 conditions have 0 peaks
			peak.files <- c(ctrl.peak, treat.peak)
			for ( i in 1:length(peak.files) ) {
				n.peaks <- as.numeric(system(paste("wc -l <", peak.files[i], sep=" "), intern=TRUE))
				
				if (n.peaks == 0) {
					cat("File ", peak.files[i], "contains 0 peaks. Cannot run paired DMR analysis\n")
				}
			}
			
			if(debug) print("DMR.COUNT.SINGLE::00")
			diffBind.csv <- data.frame(	
			SampleID = c(ctrl.id, treat.id),
			Tissue = exp.spec$tissue[1],
			Factor = "meth",
			Condition = c("ctrl", cond),
			Replicate = repl,
			bamReads = c(ctrl, treat),
			Peaks = c(ctrl.peak, treat.peak),
			PeakCaller = peak.caller )
			
			if(debug) print("DMR.COUNT.SINGLE::01")				
			if(debug) print(diffBind.csv)

			write.csv( diffBind.csv, file=csv.file, quote=FALSE, row.names=FALSE )
			if(debug) print("DMR.COUNT.SINGLE::02")
			
			plotname <- paste( cond, "ctrl", repl, sep="-")
			dmr.count( csv.file, plotname )
		}
	}	
}}





