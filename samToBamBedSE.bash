#!/bin/bash
# script name: ./alignSingleEnd.bash file.fastq cond_name genome_build
# run the script for each filtered fastq file

# get root name
cond_name="$1"

# sam to bam
samtools view -Sb ${cond_name}.sam > ${cond_name}_tmp.bam

# keep only mapped reads
## samtools view -bF 4 ${cond_name}_tmp.bam > ${cond_name}_mapped.bam

# sort by leftmost coordinate and index
samtools sort ${cond_name}_tmp.bam ${cond_name}
samtools index ${cond_name}.bam
samtools rmdup -s ${cond_name}.bam ${cond_name}_rmdup.bam

# Create BED file.
bamToBed -i ${cond_name}_rmdup.bam | grep -v -i "_random" | grep -v -i "chrUn"| sort -k1,1 -k2,2n -k6,6 > ${cond_name}_aligned.bed

# clean up:
rm ${cond_name}.sam
rm ${cond_name}_tmp.bam  # keep this for stats
#rm ${cond_name}_mapped.bam