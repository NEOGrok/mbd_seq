## NB: ONLY INCLUDE FILES THAT CONTAIN AT LEAST 1 PEAK: That's y it's better to automate the script n generate the csv file automatically, e.g., from within R.


###
### DiffBind pipeline in R: diffBindAnalysis.r
###
library(DiffBind)

## Load annotation function
source(paste(file.path( scriptDir, "annotateRegions.r" )))


diffbind.analysis <- function( csv.file ) {
	# create dba object:
	#	get samples
	#	keep only peaks that r shared by at least DIFFBIND.MIN.OVERLAPS samples
	#	keep mitochondiral chr peaks
	#	discard peaks on rnd chrs

	mbdSeq <- dba(sampleSheet=csv.file, minOverlap=DIFFBIND.MIN.OVERLAPS, bRemoveM=FALSE, bRemoveRandom=TRUE, bCorPlot=FALSE)

	#csv.file.custom <- "DIFFBIND/diffBind.single.basal.min60.csv"
	#mbdSeq <- dba(sampleSheet=csv.file.custom, minOverlap=DIFFBIND.MIN.OVERLAPS, bRemoveM=FALSE, bRemoveRandom=TRUE, bCorPlot=FALSE)

	#print( mbdSeq )
	#print(dim( mbdSeq$vectors ))
	#print(head( mbdSeq$vectors ))  # chr, start, end, -10*log(p) from MACS
	#print(mbdSeq$masks)
	#print(mbdSeq$contrasts)  # not set yet

	pdf(file.path(OUT.DIR, "diffbind.analysis.pdf"))
		if (debug) print("DIFFBIND:00")
		###
		### Occupancy Analysis
		###
		# "When the drop-off is extremely steep (geometric), this is an indication that the peaksets do not agree very well.
		# For example, if there are replicates you expect to agree, there may be a problem with the experiment." [DiffBind p.24]
		#
		# So checking the overlap rate is a good first analysis step:
		olap.rate = dba.overlap(mbdSeq,mode=DBA_OLAP_RATE)
		print(olap.rate)

		if (debug) print("DIFFBIND:01")
		plot(olap.rate,type='b',ylab='# peaks', xlab='Overlap at least this many peaksets', main='Overlap rate: all samples')
		#plot(olap.rate[2:length(olap.rate)],type='b',ylab='# peaks', xlab='Overlap at least this many peaksets', main='Overlap rate: starting from 3 samples')
	#	dba.plotVenn(mbdSeq)

		if (debug) print("DIFFBIND:02")

		for ( cond in unique(mbdSeq$samples$Condition) ) {
			cat("Overlap rate for condition: ", cond, "\n")

			if ( sum(mbdSeq$masks[[cond]]) == 1 ) {
				cat("Only one sample. No overlap rate calc. needed.")
				next
			}

			olap.rate <- dba.overlap(mbdSeq, mbdSeq$masks[[cond]], mode=DBA_OLAP_RATE)
	#		dba.plotVenn(mbdSeq, mbdSeq$masks[[cond]] & (mbdSeq$masks$Replicate.1 | mbdSeq$masks$Replicate.2 | mbdSeq$masks$Replicate.3 ))
			print(olap.rate)
			plot(olap.rate,type='b',ylab='# peaks', xlab='Overlap at least this many peaksets', main=paste("Overlap rate:", cond, sep=" "))
		#	plot(olap.rate[2:length(olap.rate)],type='b',ylab='# peaks', xlab='Overlap at least this many peaksets', main=paste("Overlap rate starting from 3 samples:", cond, sep=" "))
			#dba.plotVenn(mbdSeq, mbdSeq$masks[[cond]], mode=DBA_OLAP_RATE)
		}
		if (debug) print("DIFFBIND:03")
		# Plot occupancy heatmap based on cross-correlation of common peaks
		plot(mbdSeq, main="occupancy heatmap based on cross-corr of consensus peaks")
		if (debug) print("DIFFBIND:04")
		#
	#	dba.plotHeatmap(mbdSeq, score=DBA_SCORE_READS)
	#	dba.plotHeatmap(mbdSeq, score=DBA_SCORE_RPKM)
	#	dba.plotHeatmap(mbdSeq, score=DBA_SCORE_TMM_MINUS_FULL)  # default
	#	dba.plotHeatmap(mbdSeq, score=DBA_SCORE_TMM_MINUS_FULL, olPlot=DBA_OLAP)  # default
	#	dba.plotHeatmap(mbdSeq, score=DBA_SCORE_TMM_MINUS_FULL, olPlot=DBA_INALL)  # default
	#	dba.plotHeatmap(mbdSeq, score=DBA_SCORE_TMM_MINUS_EFFECTIVE)  # default
	#	pvals = dba.plotBox(mbdSeq)


		print( head(data.frame( mbdSeq$vectors[ ,c("CHR","START","END") ], ID=paste0("consP:",rownames(mbdSeq$vectors)) )))

		## write consensus peaks to file
		write.table(
			data.frame(
				mbdSeq$vectors[ ,c("CHR","START","END") ],
				ID=paste0("consP:",rownames(mbdSeq$vectors)) ),
			file = file.path(OUT.DIR,paste0("consensusPeaks.",DIFFBIND.MIN.OVERLAPS,".bed")),
			sep = "\t", quote = FALSE, col.names = FALSE, row.names = FALSE )


		###
		### Affinity Analysis
		###
		cat("counting reads\n")
		# Plot binding affinity heatmap based on read counts within the consensus peak set for each sample
#		mbdSeq <- dba.count(mbdSeq, score=DBA_SCORE_READS, minOverlap=DIFFBIND.MIN.OVERLAPS, insertLength=EXTEND.LENGTH, bCorPlot=TRUE )
#		mbdSeq <- dba.count(mbdSeq, score=DBA_SCORE_RPKM, minOverlap=DIFFBIND.MIN.OVERLAPS, insertLength=EXTEND.LENGTH, bCorPlot=TRUE )
	#	mbdSeq <- dba.count(mbdSeq, score=DBA_SCORE_TMM_MINUS_FULL, minOverlap=DIFFBIND.MIN.OVERLAPS, insertLength=EXTEND.LENGTH, bCorPlot=TRUE )
	print(dim( mbdSeq$vectors ))
		mbdSeq <- dba.count(mbdSeq, score=DBA_SCORE_TMM_READS_EFFECTIVE, minOverlap=DIFFBIND.MIN.OVERLAPS, insertLength=EXTEND.LENGTH, bCorPlot=TRUE )
	#	mbdSeq.counts <- dba.count(mbdSeq, score=DBA_SCORE_TMM_READS_EFFECTIVE, minOverlap=DIFFBIND.MIN.OVERLAPS, insertLength=EXTEND.LENGTH, bCorPlot=TRUE )
	print(dim( mbdSeq$vectors ))
	print(str(mbdSeq))


	#	mbdSeq <- dba.count(mbdSeq, score=DBA_SCORE_TMM_READS_EFFECTIVE, minOverlap=DIFFBIND.MIN.OVERLAPS, insertLength=EXTEND.LENGTH, bCorPlot=FALSE )
	#	mbdSeq <- dba.count(mbdSeq, score=DBA_SCORE_READS, minOverlap=DIFFBIND.MIN.OVERLAPS, insertLength=450, bCorPlot=FALSE )
	#	mbdSeq <- dba.count(mbdSeq, score=2, minOverlap=DIFFBIND.MIN.OVERLAPS, insertLength=EXTEND.LENGTH, bCorPlot=TRUE )

	#	mbdSeq <- dba.count(mbdSeq, minOverlap=DIFFBIND.MIN.OVERLAPS, score=DBA_SCORE_READS, insertLength=EXTEND.LENGTH, bCorPlot=TRUE )
	if (debug) print("DIFFBIND:05")

		#mbdSeq <- dba.contrast(mbdSeq, categories=DBA_CONDITION)
	#	mbdSeq <- dba.contrast(mbdSeq, categories=DBA_CONDITION, minMembers=3)


#		dba.plotHeatmap(mbdSeq, score=DBA_SCORE_READS)
#		dba.plotHeatmap(mbdSeq, score=DBA_SCORE_RPKM)
#		dba.plotHeatmap(mbdSeq, score=DBA_SCORE_TMM_MINUS_FULL)  # default
#		dba.plotHeatmap(mbdSeq, score=DBA_SCORE_TMM_MINUS_EFFECTIVE)  # default


	if ( DIFFBIND.PEAK.STATS ) {
		# Plot:
		#	x axis: chromosome
		#	y axis: point for each peak
		#		peak size prop to A: #tags  B: tag/density (RPKM)
		#

		# I do this first for the consensus peaks

		# Get chromosome lengths file
		if ( !file.exists("chrLengths") ) {
			#system(paste("samtools view -H", aln.file.bam, "| grep '^@SQ' | sed 's/.*SN:\\(chr.*\\).LN:\\(.*\\)$/\\1\\t\\2/' > chrLengths", sep=" "))
			system(paste(
				"mysql --user=genome --host=genome-mysql.cse.ucsc.edu -A -D",
				GENOME.BUILD,
				"-e \"select chrom, size from chromInfo\" | grep -w \"chr[0-9]*[XYM]*\" > chrLengths",
				sep=" "))
		}
		chrLengths <- read.table("chrLengths", sep="\t", stringsAsFactors=FALSE)


		## consPeak length distribution
		width <- mbdSeq$peaks[[1]]$End - mbdSeq$peaks[[1]]$Start
		hist(width, breaks=100, col="blue", )

		## Inter-consPeak distance distribution
		interConsPeakDist <- c()
		for ( i in seq_along(levels(mbdSeq$peaks[[1]]$Chr)) ) {  # for each chrom
			chrom <- levels(mbdSeq$peaks[[1]]$Chr)[i]
			cat("chr #", i," = ", chrom, "\n")

			peak.bed <- subset( mbdSeq$peaks[[1]], Chr==chrom, select=c("Chr","Start","End") )
			interConsPeakDist <- c(interConsPeakDist, peak.bed$Start[2:nrow(peak.bed)] - peak.bed$End[1:(nrow(peak.bed)-1)])
		}
		hist(interConsPeakDist, breaks=200, col="red")
		hist(interConsPeakDist, breaks=10000, col="red", xlim=c(0,100000))
		hist(interConsPeakDist, breaks=50000, col="red", xlim=c(0,10000))



		## merge peaks within max.peak.dist nts
#		max.peak.dist <- 5000
#		for ( i in seq_along(levels(mbdSeq$peaks[[1]]$Chr)) ) {  # for each chrom
#			chrom <- levels(mbdSeq$peaks[[1]]$Chr)[i]
#			cat("chr #", i," = ", chrom, "\n")
#
#			peak.bed <- subset( mbdSeq$peaks[[1]], Chr==chrom, select=c("Chr","Start","End","Reads") )
#
#			interConsPeakDist <- peak.bed$Start[2:nrow(peak.bed)] - peak.bed$End[1:(nrow(peak.bed)-1)]
#
#			for ( j in 1:(nrow(peak.bed)-1) ) {
#				if ( interConsPeakDist[j] < max.peak.dist ) {
#					peak.bed$merge[j] <- TRUE
#				} else {
#					peak.bed$merge[j] <- FALSE
#				}
#			}
#
#		}
#		new.peak.bed <- peak.bed[peak.bed$]



		# common consensus peaks that hv at least one read in all samples:
		common.peaks.mask <- apply(sapply( mbdSeq$peaks, function(x) x$Reads > 1 ),1,all)
		#cond.peaks.mask <- apply(sapply( mbdSeq$peaks, function(x) x$Score > 0 ),1,all)
		#common.peaks.mask <- apply(sapply( mbdSeq$peaks, function(x) {x$Score > 0 & x$Chr == "chr1"} ),1,all)

		# annotate them
		if ( DIFFBIND.ANNOTATE.COMMON.PEAKS && sum(common.peaks.mask) > 0) {
			#data.pts.common <- subset( mbdSeq$vector, common.peaks.mask, select=c("CHR","START","END") )
			data.pts.common <- subset( mbdSeq$vector, common.peaks.mask )
			annotname <- "common.peaks"
			annotate.regions( data.pts.common, annotname,  meta.data=4:ncol(data.pts.common) )
		}
		# annotate all consensus peaks
		if ( DIFFBIND.ANNOTATE.ALL.CONSENSUS.PEAKS ) {
			#data.pts.common <- subset( mbdSeq$vector, select=c("CHR","START","END") )
			annotname <- "all.consensus.peaks"
			#annotate.regions( data.pts.common, annotname )
			annotate.regions( mbdSeq$vector, annotname, meta.data=4:ncol(mbdSeq$vector) )
		}


	pdf(file.path(OUT.DIR, "diffbind.peakStats1.pdf"))
		### TBD
		## wt does the score that I plot as cex signify in rel. to # tags/peak
		## Y can't I add chr names to x axis
		## legend that tells sth abt point size and score


		for ( smpl in names(mbdSeq$vectors)[4:ncol(mbdSeq$vectors)] ) {  # for each sample
			cat("Processing sample: ", smpl, "\n")

			s.id <- which(mbdSeq$samples$SampleID == smpl)  # they should come in the same order as the columns in $vectors


			if ( T ) {
				# here I visualize the peak score (e.g. if I set score=DBA_SCORE_TMM_READS_EFFECTIVE then the score will b the effective # rds in that peak after TMM norm)
				max.tags <- max(mbdSeq$vector[ ,smpl])
				min.tags <- min(mbdSeq$vector[ ,smpl])
				plot(1:nrow(chrLengths), rep(1,nrow(chrLengths)), type="n", xlim=c(1,nrow(chrLengths)), ylim=c(0,1.1), xlab="", ylab="pos along chr norm to chr len",
					main=smpl,
					sub=paste( sum(mbdSeq$peaks[[s.id]]$Score > 0), "called peaks and", nrow(mbdSeq$vector), "consensus peaks w.", min.tags, "-", max.tags, "peak score"), xaxt="n")
				label.y <- rep(1,nrow(chrLengths))
				label.y[seq(1,nrow(chrLengths),by=2)] <- 2
				mtext(chrLengths[ ,1], 1, at=1:nrow(chrLengths), line=label.y, cex=0.5)
				#axis(1, at=1:nrow(chrLengths), labels=chrLengths[ ,1])

				for ( i in seq_along(levels(mbdSeq$vectors$CHR)) ) {  # for each chrom
					chrom <- levels(mbdSeq$vectors$CHR)[i]
					cat("chr #", i," = ", chrom, "\n")

					abline(v=i,col="gray")  # draw grey vertical line at chromosome i

					data.pts <- subset( mbdSeq$vectors, select=c("CHR","START",smpl) )  # only keep current sample peaks
					data.pts$START <- data.pts$START / chrLengths[chrLengths[ ,1]==chrom,2]  # peak start positions normalized to chr len
					data.pts.common <- subset( data.pts, CHR==chrom & common.peaks.mask )
					data.pts <- subset( data.pts, CHR==chrom )  # only keep peaks that r on current chromosome

					points(rep(i,nrow(data.pts)),(data.pts$START), pch=3, col="gray")
					points(rep(i,nrow(data.pts)),(data.pts$START), pch=19, cex=data.pts[ ,smpl]/max.tags*5, col="black")
					points(rep(i,nrow(data.pts.common)),(data.pts.common$START), pch=3, col="orange")
					points(rep(i,nrow(data.pts.common)),(data.pts.common$START), pch=19, cex=data.pts.common[ ,smpl]/max.tags*5, col="red")
				}


			}



			if ( T ) {
				# same but w peak point sizes reflecting # tags in the peak
				max.tags <- max(mbdSeq$peaks[[s.id]]$Reads) - 1
				min.tags <- min(mbdSeq$peaks[[s.id]]$Reads) - 1
				plot(1:nrow(chrLengths), rep(1,nrow(chrLengths)), type="n", xlim=c(1,nrow(chrLengths)), ylim=c(0,1.1), xlab="", ylab="pos along chr norm to chr len",
					main=smpl,
					sub=paste( sum(mbdSeq$peaks[[s.id]]$Score > 0), "called peaks and", nrow(mbdSeq$vector), "consensus peaks w.", min.tags, "-", max.tags, "tags/peak"), xaxt="n")
				label.y <- rep(1,nrow(chrLengths))
				label.y[seq(1,nrow(chrLengths),by=2)] <- 2
				mtext(chrLengths[ ,1], 1, at=1:nrow(chrLengths), line=label.y, cex=0.5)
				#axis(1, at=1:nrow(chrLengths), labels=chrLengths[ ,1])

				for ( i in seq_along(levels(mbdSeq$vectors$CHR)) ) {  # for each chrom
					chrom <- levels(mbdSeq$vectors$CHR)[i]
					cat("chr #", i," = ", chrom, "\n")

					abline(v=i,col="gray")  # draw grey vertical line at chromosome i

					data.pts <- subset( mbdSeq$peaks[[s.id]], select=c("Chr","Start","Reads") )  # only keep current sample peaks
					data.pts$Start <- data.pts$Start / chrLengths[chrLengths[ ,1]==chrom,2]  # peak start positions normalized to chr len
					data.pts.common <- subset( data.pts, Chr==chrom & common.peaks.mask )
					data.pts <- subset( data.pts, Chr==chrom )  # only keep peaks that r on current chromosome

					points(rep(i,nrow(data.pts)),(data.pts$Start), pch=3, col="gray")
					points(rep(i,nrow(data.pts)),(data.pts$Start), pch=19, cex=data.pts$Reads/max.tags*5, col="black")
					points(rep(i,nrow(data.pts.common)),(data.pts.common$Start), pch=3, col="orange")
					points(rep(i,nrow(data.pts.common)),(data.pts.common$Start), pch=19, cex=data.pts.common$Reads/max.tags*5, col="red")
				}
			}

		}
dev.off()


pdf(file.path(OUT.DIR, "diffbind.peakStats2.pdf"))
		for ( i in seq_along(levels(mbdSeq$vectors$CHR)) ) {  # for each chrom
				chrom <- levels(mbdSeq$vectors$CHR)[i]
				smpl.1 <- T

				for ( smpl in names(mbdSeq$vectors)[4:ncol(mbdSeq$vectors)] ) {  # for each sample
					if ( smpl.1 ) {
					#	cat("00\n")
						max.tags <- max(as.vector(as.matrix(mbdSeq$vector[ ,4:ncol(mbdSeq$vectors)])))
						#min.tags <- min(mbdSeq$vector[ ,smpl])
					#	cat("01\n")
					#	print(c(1,chrLengths[chrLengths[ ,1]==chrom,2]))
					#	print( c(0,max.tags) )
						plot(c(1,chrLengths[chrLengths[ ,1]==chrom,2]), c(0,max.tags), type="n", xlim=c(1,chrLengths[chrLengths[ ,1]==chrom,2]), ylim=c(0,max.tags+1), xlab=chrom, ylab="reads",
							main=chrom
						)
					#	cat("02\n")
						#label.y <- rep(1,nrow(chrLengths))
						#label.y[seq(1,nrow(chrLengths),by=2)] <- 2
						#mtext(chrLengths[ ,1], 1, at=1:nrow(chrLengths), line=label.y, cex=0.5)
						#axis(1, at=1:nrow(chrLengths), labels=chrLengths[ ,1])
					}
					data.pts <- subset( mbdSeq$vectors, select=c("CHR","START",smpl) )  # only keep current sample peaks
					data.pts.common <- subset( data.pts, CHR==chrom & common.peaks.mask )
					data.pts <- subset( data.pts, CHR==chrom )  # only keep peaks that r on current chromosome
			#		cat("03\n")
					s.id <- which(mbdSeq$samples$SampleID == smpl)  # they should come in the same order as the columns in $vectors
					s.cond <- mbdSeq$samples$Condition[s.id]
					s.color <- which(levels(factor(exp.spec$condition)) == s.cond)
			#		cat("04\n")
					points(data.pts$START, data.pts[ ,smpl], pch=19, cex=.15, col=s.color)
			#		cat("05\n")
					# create a two column data frame
						# col 1: concatenate all sample scores
						# col 2: sample # factor

						#sample.scores <- c(sample.scores, subset( mbdSeq$vectors, select=c("CHR","START",smpl) ) )
						#sample.factor <- c(sample.factor, )


					smpl.1 <- F
				}
		}
dev.off()


		# TBD: Would it b different using the peaks in the _peks.xls file and not the consensus peaks?
		#	peak color (heat): p-value

	# Plot:
	# boxplot for each sample:
	#	box width prop to #

	}


	if ( DIFFBIND.NORM.FILTER.MX ) {
		peak.counts <- mbdSeq$vectors
		colnames(peak.counts) <- tolower(colnames(peak.counts))
		cat( nrow( peak.counts ), "consensus peaks for\n" )
		print(colnames(peak.counts))

		# normalize to reads per million using # aln rds
		cat("Nomalizing peak counts to no. of aligned reads.\n")
		for (bam in seq_along(mbdSeq$samples$bamReads)) {
			n.aligned <- as.numeric(system(paste("wc -l <", mbdSeq$samples$bamReads[bam]),intern=TRUE))
			peak.counts[ ,3+bam] <- peak.counts[ ,3+bam] / n.aligned * 1e6  # first 3 cols r 'chr start end', followed by the samples
		}

		# filter: keep only those peaks P where: exists sample S . S(P_norm_tags) >= DIFFBIND.MIN.RD.PEAK
		cat("Filtering peak counts.\n")
		cat("Dim b4 filtering:\n")
		print(dim(peak.counts))
		#peak.counts[ ,3:(ncol(peak.counts))] <- peak.counts[any(x > DIFFBIND.MIN.RD.PEAK), ]
		peak.mask <- apply( peak.counts[ ,4:ncol(peak.counts)], 1, function(x) any(x >= DIFFBIND.MIN.RD.PEAK) )
		peak.counts <- peak.counts[peak.mask, ]
		cat("Dim after filtering:\n")
		print(dim(peak.counts))
	}


	if (debug) print("DIFFBIND:06")

		# edgeR analysis - default FDR <= 0.1:

		#mbdSeq = dba.analyze(mbdSeq, method=DBA_EDGER, bSubControl=FALSE, bFullLibrarySize=TRUE, bTagwise=FALSE)
		#mbdSeq = dba.analyze(mbdSeq, method=DBA_EDGER, bSubControl=FALSE, bFullLibrarySize=FALSE, bTagwise=FALSE)
		#mbdSeq = dba.analyze(mbdSeq, method=DBA_EDGER, bSubControl=FALSE, bFullLibrarySize=TRUE, bTagwise=TRUE)
		#mbdSeq = dba.analyze(mbdSeq, method=DBA_EDGER, bSubControl=FALSE, bFullLibrarySize=FALSE, bTagwise=TRUE)

		#mbdSeq = dba.analyze(mbdSeq, method=DBA_EDGER_CLASSIC, bSubControl=FALSE, bFullLibrarySize=TRUE, bTagwise=FALSE)
		#mbdSeq = dba.analyze(mbdSeq, method=DBA_EDGER_CLASSIC, bSubControl=FALSE, bFullLibrarySize=FALSE, bTagwise=FALSE)
		#mbdSeq = dba.analyze(mbdSeq, method=DBA_EDGER_CLASSIC, bSubControl=FALSE, bFullLibrarySize=TRUE, bTagwise=TRUE)  # *
		#mbdSeq = dba.analyze(mbdSeq, method=DBA_EDGER_CLASSIC, bSubControl=FALSE, bFullLibrarySize=FALSE, bTagwise=TRUE)  # **

		#mbdSeq = dba.analyze(mbdSeq, method=DBA_EDGER_GLM, bSubControl=FALSE, bFullLibrarySize=FALSE, bTagwise=FALSE)
		#mbdSeq = dba.analyze(mbdSeq, method=DBA_DESEQ, bSubControl=FALSE, bFullLibrarySize=TRUE, bTagwise=FALSE)
		#mbdSeq = dba.analyze(mbdSeq, method=DBA_DESEQ_CLASSIC, bSubControl=FALSE, bFullLibrarySize=TRUE, bTagwise=FALSE)
		#mbdSeq = dba.analyze(mbdSeq, method=DBA_DESEQ_GLM, bSubControl=FALSE, bFullLibrarySize=TRUE, bTagwise=FALSE)

		# default max FDR = .1
	#	mbdSeq = dba.analyze(mbdSeq,
	#		method=DBA_EDGER_GLM,
	#		bSubControl=FALSE, bFullLibrarySize=TRUE, bTagwise=FALSE)

	#	mbdSeq = dba.analyze(mbdSeq,
	#		method=DBA_EDGER_GLM,
	#		bSubControl=FALSE, bFullLibrarySize=TRUE, bTagwise=TRUE)

		if(F) {
			mbdSeq = dba.analyze(mbdSeq)

			# need 2 run edgeR b4 box-plots
			pvals = dba.plotBox(mbdSeq)
			dba.plotMA( mbdSeq )
			dba.plotPCA(mbdSeq)

			# Generate Differential Binding report if there were any DB sites (DMRs in the case of methylation data).
			# NB: If there were'n any DB sites, this code will give an error.
			mbdSeq.DB <- dba.report(mbdSeq)
			mbdSeq.DB
		}


	if ( DIFFBIND.DMR ) {
		cat("setting contrast\n")
		# Now we pick the contrast we wanna use in our test.
		# See examples in ?dba.contrast for various ways to express the contrasts.
		# Either specify group 1 and group 2 masks:
		# 	dba.contrast(DBA, group1Mask, group2Mask (=!group1Mask), name1="group1", name2="group2", minMembers=3, blockMask )
		# or use one of the predefined ones:
		# 	dba.contrast(DBA, categories = c(DBA_TISSUE,DBA_FACTOR,DBA_CONDITION,DBA_TREATMENT), minMembers=3, blockMask )
        #
        # If there r 2 groups only then this is enough:
		mbdSeq <- dba.contrast(mbdSeq, categories=DBA_CONDITION, minMembers=2, block=DBA_REPLICATE) # let replicate b a second factor in addition to the condition, since a replicate indicates the same person Before and After GBP.
		#mbdSeq <- dba.contrast(mbdSeq, categories=DBA_CONDITION, minMembers=2)
		# otherwise we form all pairwise contrasts or use custom contrast specified by the user:
		if (DIFFBIND.CONTRASTS.ALL) {


			# the peak must be pressent in at least 2/3 ~ 0.66 of the samples for it to be counted to the consensus peakset
			mbdSeqB <- dba.peakset(mbdSeq, consensus=mbdSeq$masks$basal, sampID="basal_ConsPeakset", bRemoveM=FALSE, bMerge=FALSE, minOverlap=0.50)
			mbdSeqM60 <- dba.peakset(mbdSeq, consensus=mbdSeq$masks$min60, sampID="min60_ConsPeakset", bRemoveM=FALSE, bMerge=FALSE, minOverlap=0.50)
			mbdSeq <- dba(mbdSeq, mask = mbdSeq$masks$Consensus)
			dba.plotVenn(mbdSeq)
			#	mbdSeqCond <- dba.peakset(mbdSeq, consensus=DBA_CONDITION, bRemoveM=FALSE, bMerge=FALSE, minOverlap=0.50)
			mbdSeq <- dba.peakset(mbdSeq, consensus=T, peaks=mbdSeq$masks$ctrl, bRemoveM=FALSE, bMerge=FALSE, minOverlap=.5)
			mbdSeq <- dba(mbdSeq)
			mbdSeq <- dba(mbdSeq, mask=(mbdSeq$masks$Consensus | mbdSeq$masks$gbp))
			dba.plotVenn(mbdSeq, mask=c(T,T,T))


			mbdSeqCond <- dba.peakset(mbdSeq, consensus=T, peaks=mbdSeq$masks$gbp, bRemoveM=FALSE, bMerge=FALSE, minOverlap=.5)

			mbdSeqCond <- dba.peakset(mbdSeq, consensus=T, peaks=mbdSeq$masks$basal, bRemoveM=FALSE, bMerge=FALSE, minOverlap=.5)
			mbdSeqCond <- dba.peakset(mbdSeq, consensus=T, peaks=mbdSeq$masks$min10, bRemoveM=FALSE, bMerge=FALSE, minOverlap=.5)
			mbdSeqCond <- dba.peakset(mbdSeq, consensus=T, peaks=mbdSeq$masks$min30, bRemoveM=FALSE, bMerge=FALSE, minOverlap=.5)
			mbdSeqCond <- dba.peakset(mbdSeq, consensus=T, peaks=mbdSeq$masks$min60, bRemoveM=FALSE, bMerge=FALSE, minOverlap=.5)

			for ( cond in unique(exp.spec$condition) ) {
				mbdSeq <- dba.peakset(mbdSeq, consensus=mbdSeq$masks[[cond]], sampID=paste0(cond,"_ConsPeakset"), bRemoveM=FALSE, bMerge=FALSE, minOverlap=0.66)
				mbdSeq <- dba(mbdSeq)  # merge global binding matrix (peaksets)

				mbdSeq <- dba(mbdSeq, mask = mbdSeq$masks$Consensus)

				cond.cons.counts <- dba.count(mbdSeq, peaks=mbdSeq$masks$Consensus)
			}


		}

		print(mbdSeq$contrasts)  #

		dge <- tryCatch( {
				cat("Running differential analysis.\n")

				#if ( DE.EDGER.TAGWISE.DISPERSION ) {
					mbdSeq = dba.analyze(mbdSeq)
				#}

				cat("generating dba report\n")
				mbdSeq.DB <- dba.report(mbdSeq, th=1)   # th = .1 is the default: return all DMRs with FDR <= th
				o <- order(values(mbdSeq.DB)$p.value)
				mbdSeq.DB[o][1:50]
				print(mbdSeq.DB)
				write.table( as.data.frame(mbdSeq.DB), file = file.path(OUT.DIR, "mbdSeq.DB"), sep = "\t", quote = FALSE, col.names = FALSE, row.names = FALSE )

				# Load annotation function
				source(paste(file.path( scriptDir, "annotateRegions.r" )))
				annotated.DB <- annotate.regions( as.data.frame(mbdSeq.DB), "diffbind", 4:11 )
				print(annotated.DB)

			},	error=function(e) {
				message("!!Differential analysis failed.")
				message("!!Here's the original error message:")
				message(e)
				# Choose a return value in case of error
				return(NA)
			},	finally={
				# NOTE:
				# Here goes everything that should be executed at the end,
				# regardless of success or error.
				# If you want more than one expression to be executed, then you
				# need to wrap them in curly brackets ({...}); otherwise you could
				# just have written 'finally=<expression>'
				    message("...")
			}
		)


		#	pvals = dba.plotBox(mbdSeq)
		#	dba.plotMA( mbdSeq )
		#	dba.plotPCA(mbdSeq)

			if (F) {
				mbdSeq = dba.analyze(mbdSeq, bSubControl=FALSE, bFullLibrarySize=TRUE, bTagwise=TRUE)
				pvals = dba.plotBox(mbdSeq)
				dba.plotMA( mbdSeq )
				dba.plotPCA(mbdSeq)
				mbdSeq.DB <- dba.report(mbdSeq)
				mbdSeq.DB
			}
			#mbdSeq = dba.analyze(mbdSeq, bSubControl=FALSE, bFullLibrarySize=FALSE, bTagwise=FALSE)
			#mbdSeq = dba.analyze(mbdSeq, bSubControl=FALSE, bFullLibrarySize=FALSE, bTagwise=TRUE)

		#	corvals = dba.plotHeatmap( mbdSeq )
		#	corvals = dba.plotHeatmap(mbdSeq, contrast=1, correlations=FALSE)
		#	corvals = dba.plotHeatmap(mbdSeq, contrast=1, correlations=FALSE, scale=row)
		#	dba.plotMA( mbdSeq )
		#	dba.plotMA( mbdSeq, bXY=TRUE )
		#	dba.plotPCA(mbdSeq)
		#	dba.plotPCA(mbdSeq, contrast=1,th=.05)  # FDR=.05


			## here u can see what's accessible in the DBA object mbdSeq:
	#		attributes(mbdSeq)



		#	sum(mbdSeq.DB$Fold<0)
		#	sum(mbdSeq.DB$Fold<0)
			#mbdSeq.DB[values(mbdSeq.DB)$Fold > 0]
			#mbdSeq.DB[values(mbdSeq.DB)$Fold < 0]
	}

	dev.off()
}




###########
### MAIN
###########

# Generate DiffBind COMPLETE experiment spec csv-file with peaks from MACS (must b run b4 this step)
cat("Generating DiffBind sampleSheet file:\n")


if ( DIFFBIND.PEAKS == "MACS" ) {
	peak.file.suffix <- "_peaks.xls"
	peak.caller <- "macs"
} else if ( DIFFBIND.PEAKS == "QESEQ" ) {
	peak.file.suffix <- ".txt.qeseq.raw"
	peak.caller <- "raw"     #    for f in *.txt; do cut -f1-3,8 $f > $f.qeseq.raw; done
}  else {
	cat("Unknown peak caller", DIFFBIND.PEAKS)
	q()
}

# there's only one "pair" after alignment

treat.mask <- exp.spec$pair==1 & tolower(exp.spec$condition)==DIFFBIND.TREAT_CONDITION
ctrl.mask <- exp.spec$pair==1 & tolower(exp.spec$condition)==CTRL_CONDITION

if ( DIFFBIND.CUSTOM ) {
	cat("Selecting custom replicates:")
	print(DIFFBIND.REPLICATES)
	print(exp.spec$sampleid[exp.spec$replicate %in% DIFFBIND.REPLICATES])

	treat.mask <- treat.mask & (exp.spec$replicate %in% DIFFBIND.REPLICATES)
	ctrl.mask <- ctrl.mask & (exp.spec$replicate %in% DIFFBIND.REPLICATES)
}


if ( DIFFBIND.RELATIVE ) {  # assume only two conditions
	cat( "DiffBind RELATIVE\n" )
	replicates <- intersect(
		exp.spec$replicate[treat.mask],
		exp.spec$replicate[ctrl.mask])
	treat <- exp.spec$condition[treat.mask][1]

	ctrl.repl <- replicates
	ctrl.id <- paste0(CTRL_CONDITION, "-", treat, "-", replicates)
	ctrl.bam <- paste0(CTRL_CONDITION, "-", replicates, "_aligned.bed")
	ctrl.cond <- rep(CTRL_CONDITION,length(ctrl.repl))

	treat.repl <- replicates
	treat.id <- paste0( treat, "-", CTRL_CONDITION, "-", replicates )
	treat.bam <- paste0(treat, "-", replicates, "_aligned.bed" )
	treat.cond <- rep(treat,length(ctrl.repl))

	csv.file <- file.path( OUT.DIR, paste("diffBind.relative.", DIFFBIND.PEAKS, ".csv", sep="" ))
#} else if ( DIFFBIND.MULTI ) {
#	....
} else {
	ctrl.repl <- exp.spec$replicate[ctrl.mask]
	ctrl.id <- paste(CTRL_CONDITION, ctrl.repl, sep="-")
	ctrl.bam <- paste0(CTRL_CONDITION, "-", ctrl.repl, ".mapped.bam")
	ctrl.cond <- rep(CTRL_CONDITION,length(ctrl.repl))

	treat.repl <- exp.spec$replicate[treat.mask]
	treat.id <- paste( exp.spec$condition[treat.mask], treat.repl, sep="-")
	treat.bam <- paste0( exp.spec$condition[treat.mask], "-", treat.repl, ".mapped.bam")
	treat.cond <- exp.spec$condition[treat.mask]

	csv.file <- file.path( OUT.DIR, paste0("diffBind.single.", DIFFBIND.TREAT_CONDITION, ".csv") )
}

## TBD: Right now only MACS alone is implemented properly
ctrl.peak <- file.path( PPL.ACTIONS$PEAKS, paste( ctrl.id, peak.file.suffix, sep="" ))
treat.peak <- file.path( PPL.ACTIONS$PEAKS, paste( treat.id, peak.file.suffix, sep="" ))

# TBD under construction
if (DIFFBIND.PEAKS == "BOTH" ) {
	ctrl.peak.macs <- file.path( PPL.ACTIONS$PEAKS, paste( ctrl.id, "_peaks.xls", sep="" ))
	treat.peak.macs <- file.path( PPL.ACTIONS$PEAKS, paste( treat.id, "_peaks.xls", sep="" ))
	ctrl.peak.qeseq <- file.path( PPL.ACTIONS$PEAKS, paste( ctrl.id, ".txt", sep="" ))
	treat.peak.qeseq <- file.path( PPL.ACTIONS$PEAKS, paste( treat.id, ".txt", sep="" ))
}

print(ctrl.id)
print(treat.id)
print(ctrl.peak)
print(treat.peak)
print(ctrl.bam)
print(treat.bam)
print(ctrl.cond)
print(treat.cond)
print(ctrl.repl)
print(treat.repl)

diffBind.csv <- data.frame(
	SampleID = c(ctrl.id, treat.id),
	Tissue = exp.spec$tissue[1],
	Factor = "meth",
	Condition = c(ctrl.cond,treat.cond),
	Replicate = c(ctrl.repl,treat.repl),
	bamReads = c(ctrl.bam, treat.bam),
	Peaks = c(ctrl.peak, treat.peak),
	PeakCaller = peak.caller )

# Remove conditions with 0 peaks:
cat("Removing conditions with 0 peaks:\n")
peak.files <- c(ctrl.peak, treat.peak)
for ( i in 1:length(peak.files) ) {
	n.peaks <- as.numeric(system(paste("wc -l <", peak.files[i], sep=" "), intern=TRUE))

	if (n.peaks == 0) {
		cat("File ", peak.files[i], "contains 0 peaks and will not be in the sampleSheet.\n")
		diffBind.csv <- diffBind.csv[-i, ]
	}
}

for ( cond in unique(diffBind.csv$condition) ) {
	l <- length(diffBind.csv[diffBind.csv$condition == cond, ]$replicate)
	diffBind.csv[diffBind.csv$condition == cond, ]$replicate <- 1:l
}

write.csv( diffBind.csv, file=csv.file, quote=FALSE, row.names=FALSE )

diffbind.analysis( csv.file )


